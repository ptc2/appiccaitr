<?php

Route::get('/ping', function () { return response('', 200); }); //ping

Route::post('/user', 'UserController@store'); //register

Route::post('/login', 'AuthController@login'); //login

Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail'); //reset password

Route::post('/devices', 'DeviceController@store'); //devices 

Route::get('/profile/files-user-data/{id}/preview', 'FileUserDataController@previewUserDataFile')->name('api.files_user_data.preview'); //file user data

//parentesto
Route::group(['prefix'=> 'kinship'], function () {
    Route::get('/', 'KinshipController@index');
});

//estudios
Route::group(['prefix'=> 'studies'], function () {
    Route::get('/', 'StudiesController@index');
});

Route::group(['middleware' => ['auth:sanctum', 'actived-user']], function () {
    Route::get('/profile/user', 'UserController@showAuthenticatedUser'); //user
    Route::put('/profile/user', 'UserController@updateAuthenticatedUser');

    Route::get('/profile/files-user-data/{userId}', 'FileUserDataController@getUserDataFiles'); //file user data
    Route::get('/profile/files-user-data/', 'FileUserDataController@getUserDataFilesAuthenticatedUser');
    Route::post('/profile/files-user-data', 'FileUserDataController@storeUserDataFile');
    Route::put('/profile/files-user-data/{id}', 'FileUserDataController@updateUserDataFile');
    //Route::get('/profile/files-user-data/{id}/download', 'FileUserDataController@downloadUserDataFile');
    Route::delete('/profile/files-user-data/{id}', 'FileUserDataController@deleteUserDataFile');
    Route::get('/profile/events', 'EventController@getEventsAuthenticateUser');

    Route::put('/devices/{uuid}', 'DeviceController@update'); //devides

    Route::get('/logout', 'AuthController@logout'); //logout

    /*************************************************************/
    /****************       ICCAIRT API         ******************/
    /*************************************************************/

    //user 
    Route::group(['prefix'=> 'user'], function () {
        Route::get('/', 'UserController@showAuthenticatedUser');
        //Route::post('/', 'UserController@store');
        Route::put('/', 'UserController@updateAuthenticatedUser');
    });

    //survey
    Route::group(['prefix'=> 'survey'], function () {
        Route::get('/', 'SurveyController@index');
        Route::post('/', 'SurveyController@store'); //revisar
    });

    //statistic
    Route::group(['prefix'=> 'statistic'], function () {
        Route::get('/', 'StatusticController@show'); //revisar show 
    });

});




