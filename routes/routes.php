<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$root_url = url();
$domain   = Config::get('app.domain');
$currentDomain = $domain;

require_once base_path('routes/cms.php');
require_once base_path('routes/web.php');

if (env('HAS_WEB')) {
    Route::get('/', 'Web\\SectionsController@index')->name('home');
} else {
    Route::get('/', function () {
        return redirect('/cms');
    });
}
Route::get('/cms', 'Cms\\DashboardController@index')->name('home')->middleware('auth:web');
Route::get('/home', function () {
    return redirect()->route('home');
});
