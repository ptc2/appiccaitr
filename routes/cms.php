<?php

Route::group(['domain' => $currentDomain], function () use ($root_url, $domain, $currentDomain) {
    $prefix = 'cms';
    if ($currentDomain == 'cms.' . $domain) {
        $prefix = '';
    }

    Route::group(['prefix' => $prefix], function () use ($root_url, $domain, $prefix) {
        // Modules compeltes
        $modules = require(base_path('routes/modules.php'));

        foreach ($modules as $module) {
            Route::group(['prefix' => $module, 'middleware' => ['auth:web']], function () use ($module) {
                Route::get('/export', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@export')->name($module . '.export');
                Route::get('/pdf', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@listToPdf')->name($module . '.listToPdf');
                Route::get('/', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@index')->name($module . '.index');
                Route::post('/grid', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@grid')->name($module . '.grid');
                Route::get('/create', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@create')->name($module . '.create');
                Route::post('/store', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@store')->name($module . '.store');
                Route::get('/{id}/edit', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@edit')->name($module . '.edit');
                Route::get('/{id}/cancel', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@cancel')->name($module . '.cancel');
                Route::delete('/{id}/cancel', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@cancel')->name($module . '.cancel');
                Route::get('/{id}', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@show')->name($module . '.show');
                Route::put('/{id}/inactivate', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@inactivate')->name($module . '.inactivate');
                Route::put('/{id}/activate', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@activate')->name($module . '.activate');
                if ($module != 'image') {
                    Route::put('/{id}', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@store')->name($module . '.update');
                } else {
                    Route::post('/{id}', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@update')->name($module . '.update');
                }
                Route::delete('/{id}', 'Cms\\' . ucfirst(Str::camel($module)) . 'Controller@destroy')->name($module . '.destroy');
            });
        }
		
		// Route send email user created
        Route::post('/user/{id}/sendEmailConfirmation', 'Cms\\UserController@sendEmailConfirmation');
        Route::get('/statistic/{id}/pdfStatisticDownload', 'Cms\\StatisticController@pdfStatisticDownload');
		Route::get('/statistic/{id}/pdfEstablishmentsStatisticDownload', 'Cms\\StatisticController@pdfEstablishmentsStatisticDownload');
		Route::post('/statistic/{id}/refreshStatisticTable', 'Cms\\StatisticController@refreshStatisticTable');
		Route::post('/statistic/{id}/deleteAllSurveysAnswered', 'Cms\\StatisticController@deleteAllSurveysAnswered');
        Route::get('/statistic/{id}/pdfStatisticQrSimpleDownload', 'Cms\\StatisticController@pdfStatisticQrSimpleDownload');
        Route::get('/statistic/{id}/pdfStatisticQrMultipleDownload', 'Cms\\StatisticController@pdfStatisticQrMultipleDownload');
    });
});
/**
 * Unauthenticated group
 */
Route::group(['middleware' => 'guest'], function () use ($root_url, $domain, $currentDomain) {
    Route::group(['domain' => $currentDomain], function () use ($root_url, $domain, $currentDomain) {
        $prefix = 'cms';
        if ($currentDomain == 'cms.' . $domain) {
            $prefix = '';
        }
        Route::group(['prefix' => $prefix], function () use ($root_url, $domain, $prefix) {
            Route::group(array(), function () {
                Route::post('login', 'Cms\\SessionController@store')->name('login');
            });

            /**
             * Account create form (GET)
             */
            Route::get('account/create', 'Cms\\AccountController@getCreate')->name('account.create');
            /**
             * Account create form (GET)
             */
            //Route::get('account/activate/{activation_code}', 'Cms\\AccountController@getActivate')->name('account.activate');
            /**
             * Login form (GET)
             */
            Route::get('login', 'Cms\\SessionController@create')->name('session.create');
            /**
             * Forgot password (GET)
             */
            //Route::get('account/forgot', 'Cms\\AccountController@getForgotPassword')->name('account.forgot-password');
            /**
             * Reset password (GET)
             */
            //Route::get('account/password/reset/{token}', 'Cms\\AccountController@getPasswordReset')->name('account.password-reset');
            Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
            Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
            Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
            Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.request');
        });
    });
});
/**
 * Authenticated group
 */
Route::group(['middleware' => ['auth:web']], function () use ($root_url, $domain, $currentDomain) {
    Route::group(['domain' => $currentDomain], function () use ($root_url, $domain, $currentDomain) {
        $prefix = 'cms';
        if ($currentDomain == 'cms.' . $domain) {
            $prefix = '';
        }
        Route::group(['prefix' => $prefix], function () use ($root_url, $domain, $prefix) {
            /**
             * Logout (GET)
             */
            Route::get('logout', 'Cms\\SessionController@destroy')->name('session.destroy');
            Route::get('dashboard', 'Cms\\DashboardController@index')->name('dashboard');
        });
    });
});

Route::group(['prefix' => 'cms'], function () {
    /**
     * Reset password from API
     */
    Route::get('user/password/reset/{token}', 'Auth\UserResetPasswordController@showResetForm')->name('user.password.reset');
    Route::post('user/password/reset', 'Auth\UserResetPasswordController@reset')->name('user.password.request');
    /**
     * Set account
     */
    Route::get('user/account/set/{token}', 'Auth\UserAccountSetController@showSetForm')->name('user.account.set');
    Route::post('user/account/set', 'Auth\UserAccountSetController@set')->name('user.account.request');
});
