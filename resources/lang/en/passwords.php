<?php

return [

    'password' => 'La contraseña debe tener al menos 6 caracteres y cooincidir',
    'user' => 'Usuario no encontrado',
    'token' => 'El identificador de cambio de contraseña no es válido',
    'sent' => 'Se ha enviado un email para recuperar la contraseña',
    'reset' => 'La contraseña ha sido actualizada',

];
