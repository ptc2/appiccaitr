<?php
return [
    'name' => 'Nombre',
    'surname' => 'Apellidos',
    'email' => 'Correo Electrónico',
    'manager' => 'Persona responsable',
    'members' => 'Número de familias asociadas al Centro',
    'address' => 'Dirección',
    'province' => 'Provincia',
    'city' => 'Localidad',
	'phone' => 'Teléfono'
];
