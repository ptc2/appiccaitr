<?php

return [    
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Información Estadística',
	'pdftitle' => 'informacion-estadistica',
	'pdfcentertitle' => 'informacion-estadistica-centro',
    'pdfqrsimpletitle' => 'qr-simple',
    'pdfqrmultipletitle' => 'qr-multiple',
    'user' => 'Datos del Centro',
    'user_name' => 'Nombre del Centro',
    'statistic_data' => 'Tabla de Datos',
    'question' => 'Item',
    'average' => 'Media',
    'standard_deviation' => 'Desv. Típica',
    'mode' => 'Moda',
    'min' => 'Mínimo',
    'max' => 'Máximo',
    'members' => 'Nº Familias Centro',
    'percentage_survey_answered' => '% Cuestionarios Realizados',
    'code' => 'Cod. Centro',
    'created_at' => 'Fecha',
    'genre_table' => 'Género',
    'age_table' => 'Edad',
    'studies_table' => 'Estudios',
    'kinship_table' => 'Parentesco',
    'time_coming_center_table' => 'Tiempo Acudiendo al Centro'
];