<?php

return [    
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Cuestionarios',
	'user' => 'Datos del Centro',
	'member' => 'Datos SocioDemográficos',
    'created_at' => 'Fecha de creación',
	'user_name' => 'Nombre del Centro',
	'genre' => 'Género',
	'age' => 'Edad',
	'studies' => 'Estudios',
	'other_studies' => 'Otros estudios',
	'kinship' => 'Parentesco',
	'other_kinship' => 'Otro parentesco',
	'time_going' => 'Tiempo acudiendo centro',
	'other_center' => 'Acude a otros Centros',
	'ratings' => 'Listado de Preguntas',
	'rating_question' => 'Pregunta',
	'rating_value' => 'Respuesta',
	'code' => 'Cod. Centro'
];