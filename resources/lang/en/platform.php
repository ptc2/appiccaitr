<?php
return [    
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Plataformas',
    'platform' => 'Plataforma',
    'not_found' => 'No existe la plataforma'
];