<?php
return array(

    /**
     * |--------------------------------------------------------------------------
     * | Validation Language Lines
     * |--------------------------------------------------------------------------
     * |
     * | The following language lines contain the default error messages used by
     * | the validator class. Some of these rules have multiple versions such
     * | as the size rules. Feel free to tweak each of these messages here.
     * |
     */

    "accepted" => "Debe aceptar :attribute.",
    "active_url" => "El campo :attribute no es una p&aacute;gina web v&aacute;lida.",
    "after" => ":attribute debe ser posterior a :date.",
    "alpha" => "El campo :attribute s&oacute;lo puede contener letras.",
    "alpha_dash" => "El campo :attribute s&oacute;lo puede contener letras, n&uacute;meros, y guiones.",
    "alpha_num" => "El campo :attribute s&oacute;lo puede contener letras y n&uacute;meros.",
    "array" => "El campo :attribute debe ser un array.",
    "before" => "El campo :attribute debe ser anterior a :date.",
    "between" => array(
        "numeric" => "El campo :attribute debe estar entre :min y :max.",
        "file" => ":attribute debe estar entre :min y :max kb.",
        "string" => "El campo :attribute debe estar entre :min y :max caracteres.",
        "array" => "El campo :attribute debe estar entre :min y :max elementos."
    ),
    "confirmed" => "El campo :attribute de confirmación no coincide.",
    "date" => "El campo :attribute no es una fecha v&aacute;lida.",
    "date_format" => "El campo :attribute no coincide con el formato :format.",
    "different" => "Los campos :attribute y :other deben ser diferentes.",
    "digits" => "The :attribute must be :digits digits.",
    "digits_between" => "The :attribute must be between :min and :max digits.",
    "email" => "El formato del campo :attribute no es v&aacute;lido.",
    "exists" => "El campo seleccionado :attribute no es v&aacute;lido.",
    "image" => "El fichero :attribute debe ser una imagen.",
    "in" => "El campo seleccionado :attribute no es v&aacute;lido.",
    "integer" => "El campo :attribute debe ser un entero.",
    "ip" => "El campo :attribute debe ser una IP v&aacute;lida.",
    "max" => array(
        "numeric" => "The :attribute may not be greater than :max.",
        "file" => "The :attribute may not be greater than :max kilobytes.",
        "string" => "The :attribute may not be greater than :max characters.",
        "array" => "The :attribute may not have more than :max items."
    ),
    "mimes" => "The :attribute must be a file of type: :values.",
    "min" => array(
        "numeric" => "El campo :attribute es obligatorio.",
        "file" => "The :attribute must be at least :min kilobytes.",
        "string" => "The :attribute must be at least :min characters.",
        "array" => "The :attribute must have at least :min items."
    ),
    "not_in" => "The selected :attribute is invalid.",
    "numeric" => "El campo :attribute debe ser un num&eacute;rico.",
    "regex" => "The :attribute format is invalid.",
    "required" => "El campo :attribute es obligatorio.",
    "required_unless" => "El campo :attribute es obligatorio.",
    "required_if" => "The :attribute field is required when :other is :value.",
    "required_with" => "The :attribute field is required when :values is present.",
    "required_without" => "The :attribute field is required when :values is not present.",
    "same" => "The :attribute and :other must match.",
    "size" => array(
        "numeric" => "The :attribute must be :size.",
        "file" => "The :attribute must be :size kilobytes.",
        "string" => "The :attribute must be :size characters.",
        "array" => "The :attribute must contain :size items."
    ),
    "unique" => "El :attribute ya existe.",
    "url" => "The :attribute format is invalid.",
    'nif' => "Inv&aacute;lido",
    'swift' => 'SWIFT introducido inválido',
    'iban' => 'IBAN introducido inválido',
    'cif' => 'CIF introducido inválido',
    'partida_order' => 'El orden ya está en uso',
    'invoice_number' => 'Número de factura no válido',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'username' => [
            'required' => 'El nombre de usuario es obligatorio',
            'unique' => 'Ya existe un usuario con el mismo correo electrónico',
        ],
        'password' => [
            'required' => 'La contraseña de usuario es obligatoria',
            'min' => 'La contraseña debe tener al menos 6 caracteres',
            'regex' => 'La contraseña debe contener al menos una mayúscula'
        ],
    ],

    /**
     * |--------------------------------------------------------------------------
     * | Custom Validation Attributes
     * |--------------------------------------------------------------------------
     * |
     * | The following language lines are used to swap attribute place-holders
     * | with something more reader friendly such as E-Mail Address instead
     * | of "email". This simply helps us make messages a little cleaner.
     * |
     */

    'attributes' => array(
        'name' => 'nombre',
        'surname' => 'apellidos',
        'phone_number' => 'número de teléfono móvil',
        'telephone_number' => 'número de teléfono',
        'email' => 'correo electrónico',
        'position' => 'cargo en la empresa',
        'reason_registration' => 'motivos del registro',
        'contribution' => 'aportación',
        'partner' => 'socio',
        'sector' => 'sector',
        'address' => 'dirección',
        'web' => 'página web',
        'activity' => 'actividad',
        'number_centers' => 'número de centros',
        'number_employees' => 'número de empleados',
        'company_name' => 'nombre de empresa'
    )
);
