<?php

return [
    'unauthorized' => 'Código de acceso incorrecto',
    'no_api_key' => 'La clave para la API no es válida',
    'unauthenticated' => 'No autenticado',
    'inactive' => 'Usuario inactivado',
    'not_permission' => 'No tienes permisos para realizar la acción',
    'no_verified' => 'El usuario no está verificado'
];
