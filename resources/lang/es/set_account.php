<?php

return [
    'message_error_set' => 'El enlace para configurar su cuenta ha expirado',
    'message_success_set' => 'La cuenta se ha configurado correctamente. Ya puedes acceder a la aplicación con tu usuario',
    'set_account' => 'Configurar cuenta',
    'password' => 'Contraseña',
    'confirm_password' => 'Confirma contraseña',
    'no_user' => 'No se ha localizado el usuario.'
];
