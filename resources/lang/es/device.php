<?php

return [
    
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Dispositivos',
    'push_token' => 'Push Token',
    'uuid' => 'UUID',
    'language' => 'Lenguaje',
    'model_device' => 'Modelo',
    'os_version' => 'Versión SO',
    'app_version' => 'Versión APP',
    'last_activity' => 'Última Actividad',
    'platform' => 'Plataforma',
    'device' => 'Dispositivo',
    'unsubscribe' => 'Suscrito',
    'not_found' => 'No existe el dispositivo'
];