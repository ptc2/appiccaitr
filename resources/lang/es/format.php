<?php

return [
    'date' => 'd/m/Y',
    'dateTime' => 'd/m/Y H:i:s',
    'timezone' => 'Europe/Madrid',
    'distance' => ' km',
    'decimal' => ',',
    'thousands' => '.'
];