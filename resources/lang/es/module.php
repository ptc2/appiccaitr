<?php

return [
    
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Modulos',
    'is_menu' => 'Mostrar en menú',
    'is_dashboard' => 'Mostrar en dashboard',
    'table_name' => 'Nombre de la tabla',
    'slug' => 'URL',
    'icon' => 'Icono',
    'type_icon' => 'Tipo de icono'
];