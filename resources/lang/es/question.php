<?php

return [    
    /**
     * --------------------------------------------------------------------------
     * Messages Language Lines
     * --------------------------------------------------------------------------
     *
     * The following language lines are used by the app.
     */
    'mtitle' => 'Preguntas',
	'label' => 'Etiqueta',
    'text' => 'Pregunta'
];