@extends('cms.layouts.default')

@section('content')
<div class="container">
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center">
                            <img class="img-password-reset" src="{{ res('/assets/images/logo_lateral.png') }}" alt="logo">
                        </div>
                        <div class="col-md-8 col-md-offset-2">
                            @if ($message)
                                <div class="text-center content-group text-theme-secondary">
                                    <h1>{{ $message }}</h1>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>            
</div>
@endsection
@section('footer')
@stop
