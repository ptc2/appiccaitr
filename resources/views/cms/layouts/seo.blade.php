                    <div class="panel panel-flat panel-collapsed">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <i class="fa fa-table module-{!! $module !!}"></i> SEO<a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    @foreach (App\Models\Language::getAvailable() as $key=>$lang)
                                    <li><button type="button" data-lang="{!! $lang->code !!}" data-action="changelang" class="btn bg-slate  @if ($key==0)active @endif">{!! strtoupper($lang->code) !!}</button></li>
                                    @endforeach                
                                    <li><a class="btn bg-slate" data-action="collapse"></a></li>
                                </ul>                
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::myCheckBox($item, 'meta_noindex_follow', $module, $errors, array( 'cols' => 1 ) ) !!}
                                {!! Form::myTextField($item, 'seo_title', $module, $errors, array( 'cols' => 4, 'maxlength' => 100, 'langs'=>true )) !!}        
                                {!! Form::myTextField($item, 'friendly_url', $module, $errors, array( 'cols' => 3, 'langs'=>true )) !!}
                            </div>
                            <div class="form-group">                                
                                {!! Form::myTextArea($item, 'seo_keywords', $module, $errors, array( 'cols' => 4, 'langs'=>true )) !!}
                                {!! Form::myTextArea($item, 'seo_description', $module, $errors, array( 'cols' => 4, 'langs'=>true )) !!}
                                {!! Form::myTextArea($item, 'canonical', $module, $errors, array( 'cols' => 4, 'langs'=>true )) !!}
                            </div>
                        </div>
                    </div>
     