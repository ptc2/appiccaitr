<fieldset class="content-group">
    <legend class="bordered-legend text-theme-secondary ">@lang('global.file_user_data')</legend>
    @if($files->toArray())
    <div class="table-responsive">
        <table class="table table-columned">
            <tbody>
                @foreach($files as $file)
                <tr>
                    <td>
                        @php
                            $url = route('files_user_data.preview', ['id' => $file->id]);
                        @endphp
                        <img src="{{$url}}" alt="{{$file->original_name}}" width="50px"/>
                    </td>
                    <td>{{$file->original_name}}</td>
                    <td>{{$file->fileUserDataType->label}}</td>
                    <td>
                        <a href="{{route('files_user_data.download', ['id' => $file->id])}}" class="btn btn-icon btn-sm bg-theme-primary" title="{{trans('buttons.download')}}">
                            <i class="fa fa-download"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @else
        <span class="label label-danger">No existen ficheros</span>
    @endif
</fieldset>