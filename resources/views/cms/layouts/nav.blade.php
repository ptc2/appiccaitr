<!-- Main navigation -->
{{--
<div class="sidebar-category sidebar-category-visible">
	<div class="category-content no-padding">
		<ul class="navigation navigation-main navigation-accordion">

			<!-- Main -->
			@if($menu)
			@foreach($menu as $item)
				<li data-module="{{$item->name}}"><a href="{{ url(env('APP_PREFIX_CMS').'/'.$item->slug) }}"><i class="{{$item->type_icon}} {{$item->icon}}"></i><span>{{$item->description}}</span></a></li>
			@endforeach
			@endif		
		</ul>
	</div>
</div>
--}}
<!-- /main navigation -->

<!-- Main navigation -->
@php
	$userRole = Auth::user()->user_cms_profile_id;
	$menu = [];
	
	if(IntVal($userRole) != 2) {
		$menuItems = App\Models\Module::where([
			['active', '=', 1],
			['is_menu', '=', 1],
		])
		->whereNull('module_group_id')
		->orderBy('data_order', 'asc')
		->get();

		foreach ($menuItems as $m) {
			$menu[] = $m;
		}

		$groups = DB::table('module_group')
		->where('active', 1)
		->whereNull('deleted_at')
		->get();
			
		foreach($groups as $key => $group) {
			$modules = App\Models\Module::where([
				['active', '=', 1],
				['is_menu', '=', 1],
				['module_group_id', $group->id],
			])
			->orderBy('data_order', 'asc')
			->get();

			if($modules->count() > 0) {
				$group->modules = $modules;
				$menu[] = $group;
			} else {
				unset($groups[$key]);
			}
		}		
	}
@endphp
<div class="sidebar-category sidebar-category-visible">
	<div class="category-content no-padding">
		<ul class="navigation navigation-main navigation-accordion">
			<!-- Main -->
			<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
				@if($menu)
					@foreach($menu as $item)
						<li data-module="{{$item->name}}">
							<a href="@if(!empty($item->slug)){{ url(env('APP_PREFIX_CMS').'/'.$item->slug) }}@endif">
								<i class="{{$item->type_icon}} {{$item->icon}}"></i>
								<span>{{!empty($item->description)?$item->description:$item->name}}</span>
							</a>
							@if($item->modules)
								<ul>
									@foreach($item->modules as $module)
									<li>
										<a href="{{ url(env('APP_PREFIX_CMS').'/'.$module->slug) }}">
											<i class="{{$module->type_icon}} {{$module->icon}}"></i>
											<span>{{$module->description}}</span>
										</a>
									</li>
									@endforeach
								</ul>
							@endif
						</li>
					@endforeach
				@endif
		</ul>
	</div>
</div>
<!-- /main navigation -->