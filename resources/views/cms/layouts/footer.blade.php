<div class="footer text-theme-secondary">
	<div class="container" style="width: 100%;">
		<div class="row">
			<div class="footer-copyright text-center col-sm-6">
				{{env('APP_NAME')}} {{env('APP_VERSION')}}
			</div>
			{{--<div class="footer-copyright text-center col-sm-4">
				<img src="{{ res('/assets/images/footer.png') }}" alt="{{env('APP_NAME')}}" class="img-footer">
			</div>--}}
			<div class="footer-copyright text-center col-sm-6">
				<img src="{{ res('/assets/images/solbyte.png') }}" alt="Solbyte" class="img-footer">
			</div>
		</div>
	</div>
</div>
