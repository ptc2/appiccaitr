@if (count($results) > 0)
    <div class="row">
        <div class="col-md-12">
            <fieldset class="content-group">
                <legend class="bordered-legend text-theme-secondary ">@lang($module . '.statistic_data')</legend>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>@lang($module . '.question')</th>
							<th>@lang($module . '.average')</th>
							<th>@lang($module . '.standard_deviation')</th>
							<th>@lang($module . '.mode')</th>
							<th>@lang($module . '.min')</th>
							<th>@lang($module . '.max')</th>
						</tr>
					</thead>
					<tbody>
						@foreach($results as $question => $value)
							<?php $object = (object) $value; ?>
							<tr>
								<td>{{ $question }} - {{ $object->label }}</td>
								<td>{{ $object->average }}</td>
								<td>{{ $object->standard_deviation }}</td>
								<td>{{ $object->mode }}</td>
								<td>{{ $object->min }}</td>
								<td>{{ $object->max }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
            </fieldset>
        </div>
    </div>
	<div class="row">
		<div class="col-md-12">
            <fieldset class="content-group">
                <legend class="bordered-legend text-theme-secondary ">@lang($module . '.genre_table')</legend>
                <table width="100%" class="table">
					<tr>
						<td>&nbsp;</td>
						<td align="center">Total</td>
					</tr>
					<tr>
						<td>Mujer</td>
						<td align="center">{{ $totals['totalWomen'] }}</td>
					</tr>
					<tr>
						<td>Hombre</td>
						<td align="center">{{ $totals['totalMen'] }}</td>
					</tr>
				</table>
            </fieldset>
        </div>
	</div>
	<div class="row">
		<div class="col-md-12">
            <fieldset class="content-group">
                <legend class="bordered-legend text-theme-secondary ">@lang($module . '.age_table')</legend>
                <table width="100%" class="table">
					<tr>
						<td>&nbsp;</td>
						<td align="center">Frecuencia</td>
						<td align="center">Porcentaje</td>
					</tr>
					<tr>
						<td>menor 20</td>
						<td align="center">{{ $totals['totalUnder20Years'] }}</td>
						<td align="center">{{ $percentages['percentageSurveyUnder20YearsAnswered'] }}</td>
					</tr>
					<tr>
						<td>20 - 29</td>
						<td align="center">{{ $totals['totalBetween20And29Years'] }}</td>
						<td align="center">{{ $percentages['percentageSurveyBetween20And29YearsAnswered'] }}</td>
					</tr>
					<tr>
						<td>30 - 39</td>
						<td align="center">{{ $totals['totalBetween30And39Years'] }}</td>
						<td align="center">{{ $percentages['percentageSurveyBetween30And39YearsAnswered'] }}</td>
					</tr>
					<tr>
						<td>40 - 49</td>
						<td align="center">{{ $totals['totalBetween40And49Years'] }}</td>
						<td align="center">{{ $percentages['percentageSurveyBetween40And49YearsAnswered'] }}</td>
					</tr>
					<tr>
						<td>50 - 59</td>
						<td align="center">{{ $totals['totalBetween50And59Years'] }}</td>
						<td align="center">{{ $percentages['percentageSurveyBetween50And59YearsAnswered'] }}</td>
					</tr>
					<tr>
						<td>mayor 59</td>
						<td align="center">{{ $totals['totalOverEqual60Years'] }}</td>
						<td align="center">{{ $percentages['percentageSurveyOverEqual60YearsAnswered'] }}</td>
					</tr>
				</table>
            </fieldset>
        </div>
	</div>
	<div class="row">
		<div class="col-md-12">
            <fieldset class="content-group">
                <legend class="bordered-legend text-theme-secondary ">@lang($module . '.studies_table')</legend>
				<table width="100%" class="table right-content-wrapper-table">
					<tr>
						<td>&nbsp;</td>
						<td align="center">Hombre</td>
						<td align="center">Mujer</td>
					</tr>
					<tr>
						<td>Sin Estudios</td>
						<td align="center">{{ $totals['totalMenUneducatedStudies'] }}</td>
						<td align="center">{{ $totals['totalWomenUneducatedStudies'] }}</td>
					</tr>
					<tr>
						<td>Primarios</td>
						<td align="center">{{ $totals['totalMenPrimaryStudies'] }}</td>
						<td align="center">{{ $totals['totalWomenPrimaryStudies'] }}</td>
					</tr>
					<tr>
						<td>Medios</td>
						<td align="center">{{ $totals['totalMenHighSchoolEducationStudies'] }}</td>
						<td align="center">{{ $totals['totalWomenHighSchoolEducationStudies'] }}</td>
					</tr>
					<tr>
						<td>Superiores</td>
						<td align="center">{{ $totals['totalMenHigherEducation'] }}</td>
						<td align="center">{{ $totals['totalWomenHigherEducation'] }}</td>
					</tr>
					<tr>
						<td>Postgrado</td>
						<td align="center">{{ $totals['totalMenPostgraduateStudies'] }}</td>
						<td align="center">{{ $totals['totalWomenPostgraduateStudies'] }}</td>
					</tr>
					<tr>
						<td>Otros Estudios</td>
						<td align="center">{{ $totals['totalMenOtherStudies'] }}</td>
						<td align="center">{{ $totals['totalWomenOtherStudies'] }}</td>
					</tr>
					<tr>
						<td>TOTAL</td>
						<td align="center">{{ $totals['totalMenStudies'] }}</td>
						<td align="center">{{ $totals['totalWomenStudies'] }}</td>
					</tr>
				</table>
            </fieldset>
        </div>
	</div>
	<div class="row">
		<div class="col-md-12">
            <fieldset class="content-group">
                <legend class="bordered-legend text-theme-secondary ">@lang($module . '.kinship_table')</legend>
                <table width="100%" class="table">
					<tr>
						<td>&nbsp;</td>
						<td align="center">Mujer</td>
						<td align="center">Hombre</td>
					</tr>
					<tr>
						<td>Madre/Padre</td>
						<td align="center">{{ $totals['totalMother'] }}</td>
						<td align="center">{{ $totals['totalFather'] }}</td>
					</tr>
					<tr>
						<td>Tía/Tío</td>
						<td align="center">{{ $totals['totalAunt'] }}</td>
						<td align="center">{{ $totals['totalUncle'] }}</td>
					</tr>
					<tr>
						<td>Abuela/Abuelo</td>
						<td align="center">{{ $totals['totalGrandma'] }}</td>
						<td align="center">{{ $totals['totalGrandfather'] }}</td>
					</tr>
					<tr>
						<td>Tutora/Tutor</td>
						<td align="center">{{ $totals['totalGuardian'] }}</td>
						<td align="center">{{ $totals['totalTutor'] }}</td>
					</tr>
					<tr>
						<td>Cuidadora/Cuidador</td>
						<td align="center">{{ $totals['totalNanny'] }}</td>
						<td align="center">{{ $totals['totalCaregiver'] }}</td>
					</tr>
					<tr>
						<td>Otro</td>
						<td align="center" colspan="2">{{ $totals['totalOther'] }}</td>
					</tr>
				</table>
            </fieldset>
        </div>
	</div>
	<div class="row">
		<div class="col-md-12">
            <fieldset class="content-group">
                <legend class="bordered-legend text-theme-secondary ">@lang($module . '.time_coming_center_table')</legend>
				<table width="100%" class="table right-content-wrapper-table">
					<tr>
						<td>&nbsp;</td>
						<td align="center">Total</td>
					</tr>
					<tr>
						<td>1 a 12 meses</td>
						<td align="center">{{ $totals['totalBetween1And12MonthsGoing'] }}</td>
					</tr>
					<tr>
						<td>13 a 24 meses</td>
						<td align="center">{{ $totals['totalBetween13And24MonthsGoing'] }}</td>
					</tr>
					<tr>
						<td>25 a 36 meses</td>
						<td align="center">{{ $totals['totalBetween25And36MonthsGoing'] }}</td>
					</tr>
					<tr>
						<td>37 a 48 meses</td>
						<td align="center">{{ $totals['totalBetween37And48MonthsGoing'] }}</td>
					</tr>
					<tr>
						<td>49 a 60 meses</td>
						<td align="center">{{ $totals['totalBetween49And60MonthsGoing'] }}</td>
					</tr>
					<tr>
						<td>61 a 72 meses</td>
						<td align="center">{{ $totals['totalBetween61And72MonthsGoing'] }}</td>
					</tr>
				</table>
            </fieldset>
        </div>
	</div>
@endif