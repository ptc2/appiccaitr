@extends('edit') 
<!-- Page content -->
@section('edit-master')
<div class="row">
    <div class="col-md-12">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang($module . '.user')</legend>
            <div class="form-group">
				<div class="col-xs-4">
					<div class="col-sm-12">
						<label>@lang($module . '.user_name')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $item->user->userData->name }}</label>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="col-sm-12">
						<label>@lang($module . '.members')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $item->user->userData->members }}</label>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="col-sm-12">
						<label>@lang($module . '.percentage_survey_answered')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $percentageSurveyAnswered }} % ({{$totalSurveys}})</label>
					</div>
				</div>
            </div>
        </fieldset>
    </div>
</div>
<div class="table-filter">
	<?php
		$operators = [
			'equal' => [
				'title' => 'Igual a',
				'sign' => '='
			], 
			'nequal' => [
				'title' => 'Desigual a',
				'sign' => '≠'
			], 
			'high' => [
				'title' => 'Mayor que',
				'sign' => '>'
			],
			'less' => [
				'title' => 'Menor que',
				'sign' => '<'
			],
			'eqhigh' => [
				'title' => 'Mayor o igual que',
				'sign' => '≥'
			],
			'eqless' => [
				'title' => 'Menor o igual que',
				'sign' => '≤'
			],
			'between' => [
				'title' => 'Entre',
				'sign' => '≶'
			]
		];
	?>
	{!! Form::dateFilter($module, 'created_at', $operators) !!}
</div>
<div class="results-table">
	@include('statistic.table')
</div>
@stop
@section('optional-buttons')
    <div class="panel-empty">
        <a id="btnPdfStatisticDownload" name="pdf_statistic_download" type="button" class="btn bg-teal-400 btnPdfStatisticDownload"> @lang('buttons.pdf_statistic_download')</a>
		<a id="btnPdfEstablishmentsStatisticDownload" name="pdf_establishments_statistic_download" type="button" class="btn bg-teal-400 btnPdfEstablishmentsStatisticDownload"> @lang('buttons.pdf_establishments_statistic_download')</a>
    </div>
@stop
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var module = js_module;
	    var id = $('input[name=id]').val();
	    var dataUrlPdfStatisticDownload =  BASE_URL + '/' + module + '/' + id + '/pdfStatisticDownload';
		var dataUrlPdfEstablishmentsStatisticDownload =  BASE_URL + '/' + module + '/' + id + '/pdfEstablishmentsStatisticDownload';

	    $('#btnPdfStatisticDownload').attr('href', dataUrlPdfStatisticDownload);
	    $('#btnPdfEstablishmentsStatisticDownload').attr('href', dataUrlPdfEstablishmentsStatisticDownload);
		
		$(document).on('change', '#created_at', function() {
            refreshStatisticTable();
        });
		$(document).on('change', '#created_at_max', function() {
            refreshStatisticTable();
        });
    });
	
	function refreshStatisticTable() {
		var module = js_module;
		var id = $('input[name=id]').val();
		var dataUrlRefreshStatisticTable =  BASE_URL + '/' + module + '/' + id + '/refreshStatisticTable';
		$.ajax({
			url: dataUrlRefreshStatisticTable,
			method: 'post',
			dataType: 'json',
			data: { 'created_at': $('#created_at').val(), 'created_at_max': $('#created_at_max').val(), 'operator': $('input[name=created_at_operator]').val() },
			success: function( data ) {
				$('.results-table').html(data.view);
			}
		});
	}
</script>
@stop