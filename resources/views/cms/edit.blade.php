@extends('layouts.default')

@section('content')
  <!-- Page content -->
           
                <?php
                // Set the Form params
                $blade_form_params = array(
                    'method' => (isset($form['method'])?$form['method']:'POST'),
                    'class'  => 'form-horizontal',
          'novalidate'
                );

        if(isset($form['id'])) {
          $blade_form_params['id'] = $form['id'];
        }

                if( array_key_exists( 'files', $form) )
                    $blade_form_params[ 'files' ] = $form[ 'files' ];

                if (!empty($item->id)){
                    $form_route = array($form['route'], 'id' => $item->id);
                }else{
                    $form_route = array($form['route']);
                }
                $blade_form_params['route'] = $form_route;
                ?>
                {!! Form::model($item, $blade_form_params) !!}
                @if (!empty($item->id))
                    {!! Form::hidden('id', $item->id) !!}
                    {!! Form::hidden('related_id', $item->id) !!}
                    {!! Form::hidden('related_type', ucfirst(Str::camel($module))) !!}
                @endif
                @if ($item->isPrivateReal())
                    {!! Form::hidden('user_id', Auth::user()->getScopeKey(), array('id' => 'user_id')) !!}
                @endif
                <div class="row">
                  <div class="col-md-<?php echo empty($relations)?'12':'12'; ?>">
                    <div class="panel panel-flat panel-visible">
                        <div class="panel-heading">
                          <!--h5 class="panel-title"><i class="fa fa-table module-{!! $module !!}"></i>
                          @if (!empty($item->id) && !request()->has('deleteOnCancel'))
                            @if (!empty($item->name))
                                @lang($module.'.edit', array('name' => $item->name))
                            @else
                                @lang($module.'.edit')
                            @endif
                          @else
                            @lang($module.'.new')
                          @endif
                          </h5-->
                          @if((!isset($hideFormLanguages) || !$hideFormLanguages) && !empty($item->fieldsLangs))
                            <div class="heading-elements">
                              <ul class="icons-list">
                                @foreach (App\Models\Language::getAvailable() as $key=>$lang)
                                    <li><button type="button" data-lang="{!! $lang->code !!}" data-action="changelang" class="btn bg-slate  @if ($key==0)active @endif">{!! strtoupper($lang->code) !!}</button></li>
                                @endforeach
                                </ul>                
                            </div>
                          @endif
                        </div>
                        <div class="panel-body">
                            @yield('edit-master')
                            @if ($item->numImages)
                            <div class="form-group">
                              @if($item->typeGroupsImages)
                                @for ($i = 1; $i <= $item->numImages; $i++)                                  
                                {!! Form::myImgGroup($item, 'image'.$i, $module, $i, $errors, array( 'cols' => 6 )) !!} 
                                @endfor
                              @else
                                @for ($i = 1; $i <= $item->numImages; $i++)                                  
                                {!! Form::myImgPreField($item, 'image'.$i, $module, $errors, array( 'cols' => 4, 'URI' => $item->getURI() ) ) !!} 
                                @endfor
                              @endif        
                            </div>  
                            @endif
                            @if ($item->numFiles)
                            <div class="form-group">
                              @for ($i = 1; $i <= $item->numFiles; $i++)                                  
                              {!! Form::myFile($item, 'file'.$i, $module, $errors, array( 'cols' => 3, 'URI' => $item->getURI() ) ) !!} 
                              @endfor        
                            </div>  
                            @endif
                            <div class="form-group">
                            @if ($item->numTextsGroups)
                              @for ($i = 1; $i <= $item->numTextsGroups; $i++)                                  
                              {!! Form::myTextGroup($item, 'text'.$i, $module, $errors, array( 'cols' => 4)) !!} 
                              @endfor        
                            @endif
                            @if ($item->numButtonsGroups)
                              @for ($i = 1; $i <= $item->numButtonsGroups; $i++)                                  
                              {!! Form::myButtonGroup($item, 'button'.$i, $module, $errors, array( 'cols' => 4)) !!} 
                              @endfor        
                            @endif
                            </div>  
                        </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <!-- <div id="accordion2" class="panel-group accordion" style="margin: 10px 0 20px 0">
                        @yield('edit-related-large')
                    </div> -->
                    @if (Route::has($module.'.index'))
                    
                    <?php
                        $defaultButtonsActions = array(
                            'cancel' => array(
                                'name' => $module.'.index',
                                'route' => route($module.'.index')
                               )
                        );

                        if(isset($buttonsActions)) {
                            $buttonsActions = array_merge($defaultButtonsActions, $buttonsActions);
                        } else {
                            $buttonsActions = $defaultButtonsActions;
                        }
                    ?>
                    @endif
                    <div class="panel-empty">
                      @if($item->options['can_edit'] || $item->options['can_create'])
                        <button id="btnGuardar" name="save" type="submit" class="btn bg-green-400"> @lang('buttons.save')</button>
                        @if(!$item->id)
                        <button id="btnGuardarNew" name="save_new" type="submit" class="btn bg-teal-400"> @lang('buttons.save_new')</button>
                        @endif
                        <button id="btnApply" name="apply" type="submit" class="btn bg-blue-400"> @lang('buttons.apply')</button>
                      @elseif($item->options['can_send'])
                        <button id="btnGuardar" name="send" type="submit" class="btn bg-green-400"> @lang('buttons.send')</button>
                        @if(!$item->id)
                        <button id="btnGuardarNew" name="send_new" type="submit" class="btn bg-teal-400"> @lang('buttons.send_new')</button>
                        @endif
                      @endif
                      @if (Route::has($module.'.index'))
                          @if($item->options['can_cancel'])
                          <a id="btnCancelar" class="btn bg-danger-400" href="{!! Route::getRoutes()->hasNamedRoute( $buttonsActions['cancel']['name'] )? $buttonsActions['cancel']['route'] :'/' !!}"> @lang('buttons.cancel')</a>
                          @elseif($item->options['can_ok'])
                          <a id="btnOk" class="btn bg-theme-primary" href="{!! Route::getRoutes()->hasNamedRoute( $buttonsActions['cancel']['name'] )? $buttonsActions['cancel']['route'] :'/' !!}"> @lang('buttons.ok')</a>
                          @endif
                      @endif
                      @if(isset($item->id) && $item->options['can_inactive'])
                      @php
                      @endphp
                          <button id="btnInactivate" name="inactivate" type="submit" class="btn bg-grey-300" style="{{ !$item->active?'display:none' : '' }}"> @lang('buttons.inactivate')</button>
                          <button id="btnActivate" name="activate" type="submit" class="btn bg-grey-300" style="{{ $item->active?'display:none' : '' }}"> @lang('buttons.activate')</button>
                      @endif
                    </div>
                    @yield( 'optional-buttons' )
                  </div>
                </div>
                @if ($item->options['seo'])

                <div class="row">
                  <div class="col-md-12">
                      <div id="accordionSeo" class="panel-group accordion">
                     <?php echo view()->make('seo_tags.edit')
                                ->with('module', $module)
                                ->with('item', $item)
                                ->render();?>
                      @yield('edit-seo','fail')
                      </div>
                  </div>
                </div>
                @endif
                @if (!request()->ajax())
                @if (!empty($relations))

                <div class="row">
                  <div class="col-md-12">
                      <div id="accordion1">
                     <?php echo view()->make('edit-related')
                                ->with('module', $module)
                                ->with('item', $item)
                                ->with('relations', $relations)                                
                                ->with('related_field', $related_field)
                                ->render();?>
                      @yield('edit-related','fail')
                      </div>
                  </div>
                </div>
                @endif
                @endif

            {!! Form::close() !!}

                </div>

@if (!request()->ajax())
    <!-- Modal delete confirmation -->
    <div aria-hidden="true" role="dialog" tabindex="-1" id="alertModal" class="modal fade" style="display: none;">
      <div class="modal-dialog modal-dialog-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title text-center">@lang('buttons.delete-title')</h4>
          </div>
          <div class="modal-body dialog-center">
            <p class="margin-bottom-lg"> @lang('buttons.delete-confirmation') </p>
            <div class="form-group text-center">
              <button data-dismiss="modal" class="btn btn-success margin-right-sm delRow" action="{{ route($module.'.destroy', 'i') }}" type="button"><i class="fa fa-check"></i> @lang('buttons.accept')</button>
              <button data-dismiss="modal" class="btn btn-danger" type="button"><i class="fa fa-warning"></i> @lang('buttons.cancel')</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal delete confirmation contact -->
    <div aria-hidden="true" role="dialog" tabindex="-1" id="alertModalContact" class="modal fade" style="display: none;">
      <div class="modal-dialog modal-dialog-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title text-center">@lang('buttons.delete-title')</h4>
          </div>
          <div class="modal-body dialog-center">
            <p class="margin-bottom-lg">¿Está seguro de que desea eliminar este contacto? También se eliminarán sus conversaciones y próxima acción</p>
            <div class="form-group text-center">
              <button data-dismiss="modal" class="btn btn-success margin-right-sm delRow" action="{!! route($module.'.destroy', 'i') !!}" type="button"><i class="fa fa-check"></i> @lang('buttons.accept')</button>
              <button data-dismiss="modal" class="btn btn-danger" type="button"><i class="fa fa-warning"></i> @lang('buttons.cancel')</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal add form -->
    <div aria-hidden="true" role="dialog" tabindex="-1" id="formModal" class="modal fade" style="display: none;">
    </div>
@endif
<script type="text/javascript" src="{{ res('/js/lib/input-img-manager.js') }}"></script>
@stop