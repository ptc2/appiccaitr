@section('header-edit-ajax')
    	<div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    		<h4 class="modal-title">
    			@if (empty($item->id))
    			<i class="fa fa-plus"></i> @lang($module.'.new')
                @else
                <i class="fa fa-pencil"></i> @lang($module.'.edit')
                @endif
    		</h4>
            <div class="heading-elements">
                <ul class="icons-list">
                  @foreach (App\Models\Language::getAvailable() as $key=>$lang)
                      <li><button type="button" data-lang="{!! $lang->code !!}" data-action="changelang" class="btn bg-slate  @if ($key==0)active @endif">{!! strtoupper($lang->code) !!}</button></li>
                  @endforeach
                  </ul>                
              </div>
    	</div>
@stop