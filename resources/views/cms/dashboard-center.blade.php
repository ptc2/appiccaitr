@extends('layouts.default')
<!-- Page content -->
@section('content')
@if (session('flash_message_title'))
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">
            {{ session('flash_message_title') }}
        </div>
    </div>
</div>
@endif
<div class="row">
	<div class="col-md-12">
		<fieldset class="content-group">
			<legend class="bordered-legend text-theme-secondary ">@lang($module . '.user')</legend>
			<div class="form-group">
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.code')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $user->username }}</label>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.user_name')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $user->userData->name }}</label>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.members')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $user->userData->members }}</label>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.percentage_survey_answered')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold" class="percentage-survey-answered">{{ $percentageSurveyAnswered }} % ({{$totalSurveys}})</label>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="row">
    <div class="col-md-2 dashboard-panel">
        <a class="qr-simple" href="{{ url(env('APP_PREFIX_CMS').'/'.$module.'/'.$centerId.'/pdfStatisticQrSimpleDownload') }}">
            <div class="text-center text-theme-secondary dashboard-body">
                <h1 class="dashboard-body-title">QR Simple</h1>
                <i class="fa fa-qrcode"></i>
                <p class="text-theme-primary dashboard-body-number">PDF</p>
            </div>
        </a>
    </div>
	<div class="col-md-2 dashboard-panel">
        <a class="qr-multiple" href="{{ url(env('APP_PREFIX_CMS').'/'.$module.'/'.$centerId.'/pdfStatisticQrMultipleDownload') }}">
            <div class="text-center text-theme-secondary dashboard-body">
                <h1 class="dashboard-body-title">QR Múltiple</h1>
                <i class="fa fa-qrcode"></i>
                <p class="text-theme-primary dashboard-body-number">PDF</p>
            </div>
        </a>
    </div>
	<div class="col-md-2 dashboard-panel @if($totalSurveys == 0) dashboard-panel-disabled @endif">
        <a class="statistic-report" @if($totalSurveys > 0) href="{{ url(env('APP_PREFIX_CMS').'/'.$module.'/'.$centerId.'/pdfEstablishmentsStatisticDownload') }}" @else href="javascript:void(0);" @endif>
            <div class="text-center text-theme-secondary dashboard-body">
                <h1 class="dashboard-body-title">Informe Estadístico</h1>
                <i class="fa fa-file-pdf-o"></i>
                <p class="text-theme-primary dashboard-body-number">PDF</p>
            </div>
        </a>
    </div>
	<div class="col-md-2 dashboard-panel">
        <a class="remove-statistic" href="javascript:void(0);" data-url="{{ url(env('APP_PREFIX_CMS').'/'.$module.'/'.$centerId.'/deleteAllSurveysAnswered') }}">
            <div class="text-center text-theme-secondary dashboard-body">
                <h1 class="dashboard-body-title">Limpiar Encuestas</h1>
                <i class="fa fa-trash-o"></i>
                <p class="text-theme-primary dashboard-body-number">&nbsp;</p>
            </div>
        </a>
    </div>
</div>

<div class="results-table">
	@include('statistic.table')
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
		/*$(document).on('change', '#created_at', function() {
            refreshStatisticTable();
        });
		$(document).on('change', '#created_at_max', function() {
            refreshStatisticTable();
        });*/
		
		$(document).on('click', '.remove-statistic', function() {
			var dataUrlDeleteAllSurveysAnswered =  $(this).data('url');
            $.ajax({
				url: dataUrlDeleteAllSurveysAnswered,
				method: 'post',
				dataType: 'json',
				data: {},
				success: function( data ) {
					$('.percentage-survey-answered').html('0 %');
					$('.results-table').html(data.view);
				}
			});
        });
    });
	
	/*function refreshStatisticTable() {
		var module = js_module;
		var id = $('input[name=id]').val();
		var dataUrlRefreshStatisticTable =  BASE_URL + '/' + module + '/' + id + '/refreshStatisticTable';
		$.ajax({
			url: dataUrlRefreshStatisticTable,
			method: 'post',
			dataType: 'json',
			data: { 'created_at': $('#created_at').val(), 'created_at_max': $('#created_at_max').val(), 'operator': $('input[name=created_at_operator]').val() },
			success: function( data ) {
				$('.results-table').html(data.view);
			}
		});
	}*/
</script>
@stop
