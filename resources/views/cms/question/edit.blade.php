@extends('edit') 
<!-- Page content -->
@section('edit-master')
    <div class="form-group">
        {!! Form::myTextField($item, 'id', $module, $errors, array( 'cols' => 4)) !!}
        {!! Form::myTextField($item, 'text', $module, $errors, array( 'cols' => 8)) !!}
    </div>
@stop