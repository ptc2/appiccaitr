@extends('layouts.default')
<!-- Page content -->
@section('content')
@if (session('flash_message_title'))
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">
            {{ session('flash_message_title') }}
        </div>
    </div>
</div>
@endif
<div class="row">
@foreach($items as $item)
    <div class="col-md-2 dashboard-panel">
        <a href="{{ url(env('APP_PREFIX_CMS').'/'.$item['slug']) }}">
            <div class="text-center text-theme-secondary dashboard-body">
                <h1 class="dashboard-body-title">{{$item['title']}}</h1>
                <i class="{{$item['icon']}}"></i>
                <p class="text-theme-primary dashboard-body-number">{{$item['count']}}</p>
            </div>
        </a>
    </div>
@endforeach
</div>
@endsection
