@extends('layouts.default')

@section('styles')
    @if ($lock)
    <link rel="stylesheet" type="text/css" href="{!! res('/vendor/plugins/countdown/jquery.countdown.css') !!}">
    @endif
@stop

@section('content')
<div class="container">

    <!-- Page container -->
  <div class="page-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">

          <!-- Advanced login -->
          @if (!$lock)
          {!! Form::open(array( 'route' => 'login', 'id' => "altForm") ) !!}
          @endif
            <div class="panel panel-body login-form">
            @if (!$lock)
              <div>
                <!--img src="{{ res('/assets/images/logo_dark.png') }}" alt="loginLogo" id="loginLogo"-->
                <img src="{{ res('/assets/images/logo.png') }}" alt="{{env('APP_NAME')}}" id="loginLogo">
                <h5 class="content-group-lg title-login-form text-theme-secondary">@lang('session.login')<small class="display-block"></small></h5>
              </div>

              <div class="form-group has-feedback has-feedback-left">
                <input type="text" id="username" name="username" value="{{ old('username') }}" class="form-control" placeholder="@lang('session.username')">
                <div class="form-control-feedback">
                  <i class="icon-user text-muted"></i>
                </div>
                {!! $errors->first('username', '<span class="help-block">:message</span>') !!}
              </div>

              <div class="form-group has-feedback has-feedback-left">
                <input type="password" id="password" name="password" class="form-control" placeholder="@lang('session.password')">
                <div class="form-control-feedback">
                  <i class="icon-lock2 text-muted"></i>
                </div>
                {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
              </div>

              <div class="form-group login-options">
                <div class="row">
                  <!--div class="col-sm-5">
                    <label class="checkbox-inline">
                      <input type="checkbox" class="styled" checked="checked">
                      @lang('session.remember')
                    </label>
                  </div-->

                  <div class="col-sm-12 text-right ">
                    <a href="{{ route('password.request') }}" class="text-theme-primary">@lang('session.forgotten')</a>
                  </div>
                </div>
              </div>
              @else
            <!--  Too much login attemps -->
              <div class="alert alert-warning alert-styled-left">@lang('session.too_much_attemps')</div>
              <div class="panel-body">
                  <div id="countdown"></div>
              </div>
             @endif
              @if (!$lock)
              <div class="form-group">
                <button type="submit" class="btn bg-theme-primary btn-block">@lang('session.signin') <!--i class="icon-circle-right2 position-right"></i--></button>
              </div>
              @endif
              
            </div>
          @if (!$lock)
          {!! Form::close() !!}
          @endif
          <!-- /advanced login -->

        </div>
        <!-- /content area -->

      </div>
      <!-- /main content -->

    </div>
    <!-- /page content -->

  </div>
  <!-- /page container -->
  </div>
  <link rel="stylesheet" type="text/css" href="{!! res('/vendor/plugins/gritter/css/jquery.gritter.css') !!}">
  <script type="text/javascript" src="{!! res('/vendor/plugins/gritter/js/jquery.gritter.min.js') !!}"></script>
@stop

@section('javascripts')
    @if ($lock)
    <link rel="stylesheet" type="text/css" href="{!! res('/vendor/plugins/countdown/jquery.countdown.css') !!}">
    <script type="text/javascript" src="{!! res('/vendor/plugins/countdown/jquery.plugin.min.js') !!}"></script>
    <script type="text/javascript" src="{!! res('/vendor/plugins/countdown/jquery.countdown.min.js') !!}"></script>
    <script type="text/javascript" src="{!! res('/vendor/plugins/countdown/jquery.countdown-'. App::getLocale() .'.js') !!}"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
          var liftoffTime = new Date( (new Date()).valueOf() + {!! $time_to_wait * 1000 !!} ) ;
        	$('#countdown').countdown({
        		until : liftoffTime,
            format: 'mS',
            expiryUrl: document.location
          });
        });
    </script>
    @endif
@stop
