@extends('layouts/default')

@section('content')
<div class="page-header">
    <h2 class="sub-header">@lang('sessions.reminder')</h2>
</div>

<div class="row">
{{ Form::open(array( 'action' => 'password.reset', 'class' => 'form-horizontal') ) }}
    {{ Form::hidden('token', $token) }}
    <div class="control-group{{ $errors->first('email', ' error') }}">
        {{ Form::label('email', trans('sessions.email'), array('class' => 'control-label')) }}
        <div class="controls">
    	   {{ Form::text('email') }}
    	   {{ $errors->first('email', '<span class="help-block">:message</span>') }}
    	</div>
    </div>
    <div class="control-group{{ $errors->first('password', ' error') }}">
        {{ Form::label('email', trans('sessions.password'), array('class' => 'control-label')) }}
        <div class="controls">
    	   {{ Form::password('password') }}
    	   {{ $errors->first('password', '<span class="help-block">:message</span>') }}
    	</div>
    </div>    
    <div class="control-group{{ $errors->first('password_confirmation', ' error') }}">
        {{ Form::label('email', trans('sessions.password_confirmation'), array('class' => 'control-label')) }}
        <div class="controls">
    	   {{ Form::password('password_confirmation') }}
    	   {{ $errors->first('password_confirmation', '<span class="help-block">:message</span>') }}
    	</div>
    </div> 
	<hr/>
	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn">@lang('sessions.reset')</button>
		</div>
	</div>
{{ Form::close() }}
</div>
@stop