@extends('edit') 
<!-- Page content -->
@section('edit-master')
<div class="row">
	@if (!empty($item->id))
		<div class="col-md-4">
			<fieldset class="content-group">
				<legend class="bordered-legend text-theme-secondary ">@lang('global.user')</legend>
				<div class="form-group">
					<div class="col-xs-6">
						<div class="col-sm-12">
							<label>@lang('global.code')</label>
						</div>
						<div class="col-sm-12">
							<label style="font-weight:bold">{{ $item->username }}</label>
						</div>					
					</div>
				</div>
			</fieldset>
		</div>
	@endif
    <div class="col-md-8">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang('global.user_data')</legend>
            <div class="form-group">
                {!! Form::myTextField($item->userData, 'name', isset($item->userData) ? $item->userData->getTable() : 'user_data', $errors, array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item->userData, 'email', isset($item->userData) ? $item->userData->getTable() : 'user_data', $errors, array( 'cols' => 6 )) !!}
            </div>
			<div class="form-group">
				{!! Form::myTextField($item->userData, 'manager', isset($item->userData) ? $item->userData->getTable() : 'user_data', $errors, array( 'cols' => 6 )) !!}
				{!! Form::myNumberField($item->userData, 'members', isset($item->userData) ? $item->userData->getTable() : 'user_data', $errors, array( 'cols' => 3 )) !!}
				{!! Form::myNumberField($item->userData, 'phone', isset($item->userData) ? $item->userData->getTable() : 'user_data', $errors, array( 'cols' => 3 )) !!}
			</div>
            <div class="form-group">
                {!! Form::myTextField($item->userData, 'address', isset($item->userData) ? $item->userData->getTable() : 'user_data', $errors, array( 'cols' => 6 )) !!}
                {!! Form::myTextField($item->userData, 'province', isset($item->userData) ? $item->userData->getTable() : 'user_data', $errors, array( 'cols' => 3 )) !!}
                {!! Form::myTextField($item->userData, 'city', isset($item->userData) ? $item->userData->getTable() : 'user_data', $errors, array( 'cols' => 3 )) !!}
            </div>
        </fieldset>
    </div>
</div>
@stop
@section('optional-buttons')
    @if($item->id && !$item->company_id)
        <div class="panel-empty">
            <button id="btnSendUserCreatedEmail" name="send_user_created_email" type="submit" class="btn bg-teal-400"> @lang('buttons.send_user_created_email')</button>
        </div>
    @endif
@stop
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.pickadate').pickadate({
            format: 'dd/mm/yyyy',
            selectYears: true,
            selectMonths: true,
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Cerrar',
        });
        $(document).on('click', '.showpass', function() {
            var type = $('#password').attr('type');
            var newType;

            if(type == 'password') {
                newType = 'text';
                $(this).removeClass('fa-eye').addClass('fa-eye-slash');
            }else{
                newType = 'password';
                $(this).removeClass('fa-eye-slash').addClass('fa-eye');
            }

            $('#password').attr('type', newType);
        });
    });
</script>
@stop