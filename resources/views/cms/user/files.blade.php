<div class="row">
    <div class="col-md-12">
    	<fieldset class="content-group">
    		<legend class="bordered-legend text-theme-secondary ">@lang('global.images')</legend>
    		<div class="images-content">
	            @foreach($files as $file)
	                <div class="image-item">
	                    <img src="{{ route('files_user_data.preview', ['id' => $file->id]) }}" alt="{{$file->userData->name}}" width="200" loading="lazy"/>
	                </div>
	            @endforeach
	        </div>
    	</fieldset>
    </div>
</div>