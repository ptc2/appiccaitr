<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
    <head>
        <title>Estadisticas</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style type="text/css">
            table {
                background-color:#444;
                font-size: 14px;
            }
            table th {
                background-color: #444;
                color: #fff;
                font-weight: normal;
            }
            table tr {
                background-color: #fff;
            }
            .text-center {
                text-align: center;
            }
            .left-content-wrapper {
                position: absolute;
                top: 0;
                left: 0;
                width: 50%;
            }
            .right-content-wrapper {
                position: absolute;
                top: 3%;
                left: 60%;
                width: 50%;
            }
            .center-content-title {
                position: absolute;
                top: 20%;
            }
            .center-content-name {
                position: absolute;
                top: 30%;
            }
			.center-content-qr {
                position: absolute;
                top: 40%;
				left: 28%;
            }
			.center-content-code-title {
                position: absolute;
                top: 80%;
            }
			.center-content-code {
                position: absolute;
                top: 85%;
				border: 1px solid;
				padding: 1%;
            }
        </style>
    </head>
    <body bgcolor="#ffffff" vlink="blue" link="blue">
        <div class="left-content-wrapper">
            <img src="{{ res('/assets/images/logo_lateral.png') }}" alt="{{env('APP_NAME')}}" id="loginLogo" style="width: 80%;">
        </div>
        <div class="right-content-wrapper text-right">
            <img src="{{ res('/assets/images/logo_uma.jpg') }}" alt="{{env('APP_NAME')}}" id="loginLogo" style="width: 80%;">
        </div>
        <div class="text-justify">
            <div class="col-xs-12 center-content-title text-center">
                <h3>INVENTARIO DE CALIDAD EN LOS CENTROS DE <br/> ATENCION INFANTIL TEMPRANA</h3>
            </div>
			<div class="col-xs-12 center-content-name text-center">
                <h1>{{ $user_name }}</h1>
            </div>
			<div class="col-xs-12 center-content-qr text-center">
                <img src="{{ res('/assets/images/QR-code.png') }}" alt="{{env('APP_NAME')}}" id="loginLogo" style="width: 65%;">
            </div>
			<div class="col-xs-12 center-content-code-title text-center">
                <h3>Código Del Centro</h3>
            </div>
			<div class="col-xs-12 center-content-code text-center">
                <h1>{{ $code }}</h1>
            </div>
        </div>
    </body>
</html>
