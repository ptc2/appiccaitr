<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
    <head>
        <title>Estadisticas</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style type="text/css">
            table {
                background-color:#444;
                font-size: 14px;
            }
            table th {
                background-color: #444;
                color: #fff;
                font-weight: normal;
            }
            table tr {
                background-color: #fff;
            }
            .text-right {
                text-align: right;
            }
        </style>
    </head>
    <body bgcolor="#ffffff" vlink="blue" link="blue">
        <h1>{{{ $title }}}</h1>
        <div class="text-right">
            <img src="{{ res('/assets/images/logo_lateral.png') }}" alt="{{env('APP_NAME')}}" id="loginLogo" style="width: 20%;">
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <fieldset class="content-group">
                    <legend class="bordered-legend text-theme-secondary ">@lang('statistic.user')</legend>
                    <div class="form-group">
                        <div class="col-xs-4">
                            <div class="col-sm-12">
                                <label>@lang('statistic.user_name')</label>
                            </div>
                            <div class="col-sm-12">
                                <label style="font-weight:bold">{{ $user_name }}</label>
                            </div>
                        </div>
						<div class="col-xs-4">
							<div class="col-sm-12">
								<label>@lang('statistic.members')</label>
							</div>
							<div class="col-sm-12">
								<label style="font-weight:bold">{{ $members }}</label>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="col-sm-12">
								<label>@lang('statistic.percentage_survey_answered')</label>
							</div>
							<div class="col-sm-12">
								<label style="font-weight:bold">{{ $percentageSurveyAnswered }} %</label>
							</div>
						</div>
                    </div>
                </fieldset>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <table class="table" cellpadding="4" width="100%">
                    <thead>
                        <tr>
                            <th>@lang('statistic.question')</th>
                            <th>@lang('statistic.average')</th>
                            <th>@lang('statistic.standard_deviation')</th>
                            <th>@lang('statistic.mode')</th>
                            <th>@lang('statistic.min')</th>
                            <th>@lang('statistic.max')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $question => $value)
                            <?php $object = (object) $value; ?>
                            <tr>
                                <td>{{ $question }} - {{ $object->label }}</td>
                                <td>{{ $object->average }}</td>
                                <td>{{ $object->standard_deviation }}</td>
                                <td>{{ $object->mode }}</td>
                                <td>{{ $object->min }}</td>
                                <td>{{ $object->max }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
