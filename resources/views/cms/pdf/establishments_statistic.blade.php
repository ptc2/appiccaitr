<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
    <head>
        <title>Estadisticas</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style type="text/css">
            table {
                background-color:#444;
                font-size: 14px;
            }
            table th {
                background-color: #444;
                color: #fff;
                font-weight: normal;
            }
            table tr {
                background-color: #fff;
            }
            .text-right {
                text-align: right;
            }
			.center-content-vertically {
				position: absolute;
				top: 40%;
			}
			.right-content-horizontally {
				position: absolute;
				left: 40%;
			}
			.center-text-content {
				position: absolute;
				top: 25%;
				font-size: 20px;
			}
			.left-content-wrapper {
				position: absolute;
				top: 0;
				left: 0;
				width: 70%;
				font-size: 20px;
			}
			.right-content-wrapper {
				position: absolute;
				top: 0;
				left: 75%;
				width: 25%;
				font-size: 20px;
			}
			.left-content-wrapper-genre,
			.right-content-wrapper-genre {
				top: 40%;
			}			
			.left-content-wrapper-kinship {
				top: 55%;
			}
			.right-content-wrapper-kinship {
				top: 50%;
			}			
			.left-content-wrapper-age {
				top: 80%;
			}
			.right-content-wrapper-age {
				top: 75%;
			}
			.right-content-wrapper-table {
				top: 10%;
			}
			.half-content-wrapper {
				top: 25%;
				width: 50%;
			}
			.right-half-content-wrapper {
				left: 50%;
			}
			.half-content-wrapper > h4 {
				text-align: center;
			}
			.page-break {
				page-break-after: always;
			}
			.text-justify {
				text-align: justify;
			}
			.dimensions-list > li {
				margin: 0 0 10px 0;
			}
			.references-list > li {
				margin: 0 0 5% 0;
			}
        </style>
    </head>
    <body bgcolor="#ffffff" vlink="blue" link="blue">
		<div class="text-justify">
			<div class="col-xs-12 center-content-vertically right-content-horizontally">
				<h1>Estudio sobre la calidad de servicio percibida por los usuarios del Centro de Atención Infantil Temprana:</h1>
				<h1 style="color: red;">{{ $user_name }}</h1>
			</div>
			<div class="page-break"></div>
			<div class="col-xs-12">
				<h1>Introducción</h1>
				<div class="center-text-content">
					<p>Presentamos en este documento un informe que recoge los aspectos más relevantes del estudio realizado en el Centro de Atención Infantil Temprana (CAIT) <b>{{ $user_name }}</b>, basado en la calidad del 
					servicio percibida por los usuarios del mismo.</p>
					
					<p>La Atención Temprana supone el conjunto de intervenciones, dirigidas a la población infantil de cero a seis años, a la familia y al entorno, que tiene por objetivo dar respuesta lo más pronto posible a 
					las necesidades transitorias o permanentes que presentan los niños con trastornos en su desarrollo o que tienen riesgo de padecerlos. Se trata de un concepto amplio que incluye a la población infantil que 
					presenta diferentes discapacidades y también a los niños que presentan trastornos o dificultades de distinto grado en cualquier ámbito del desarrollo: motricidad, comunicación, lenguaje, cognición, 
					conducta o emocional.</p>
					
					<p>Conscientes de la importancia de la Atención Temprana y la necesidad de lograr un servicio de la máxima calidad, el Centro de Atención Infantil Temprana de <b>{{ $user_name }}</b> realiza un esfuerzo por 
					obtener toda la información posible que le permita la adecuada toma de decisiones para la mejora del servicio.</p>
				</div>
			</div>
			<div class="page-break"></div>
			<div class="col-xs-12">
				<h1>Cuestionario</h1>
				<div class="center-text-content">
					<p>ICCAIT-R: 33 ítems y 5 dimensiones:</p>
					<ul class="dimensions-list">
						<li>Instalaciones</li>
						<li>Salas de tratamiento y material</li>
						<li>Personal especializado</li>
						<li>Información técnica</li>
						<li>Satisfacción e intención futura</li>
					</ul>
				</div>
			</div>
			<div class="page-break"></div>
			<div class="col-xs-12">
				<h1>Procedimiento</h1>
				<div class="center-text-content">
					<p>La recogida de datos se realizó entre el <b>{{ $minDay }}</b> y el <b>{{ $maxDay }}</b>, en el CAIT <b>{{ $user_name }}</b>,  donde se informó a los usuarios del objetivo de la investigación y se les 
					comunicó que la participación era completamente voluntaria y anónima, enfatizando en que no existían respuestas correctas o incorrectas, y solicitando la mayor honestidad en las respuestas.</p>
					
					<p>Se aplicó el mismo protocolo con todos los participantes, que respondieron de manera individualizada en las instalaciones del CAIT. Previamente, los usuarios fueron informados sobre cómo debían 
					cumplimentar el cuestionario, haciendo referencia a las instrucciones que se encontraban disponibles en el encabezado del mismo e indicándoles que firmaran el modelo de consentimiento informado 
					proporcionado.</p>
				</div>
			</div>
			<div class="page-break"></div>
			<div class="col-xs-12 center-content-vertically right-content-horizontally">
				<h1>Resultados</h1>
			</div>
			<div class="page-break"></div>
			<div class="col-xs-12">
				<h1>Factores Sociodemográficos</h1>
				<h2>Participantes</h2>
				<div class="center-text-content">
					<p>Han participado <b>{{ $totals['totalSurveys'] }}</b> familias usuarias del <b>{{ $user_name }}</b>, lo que supone un <b>{{ $percentages['percentageSurveyAnswered'] }}</b>% del 
					total (<b>{{ $members }}</b> familias)</p>
				</div>				
				<div class="left-content-wrapper left-content-wrapper-genre">						
					<p>Respecto al género, el <b>{{ $percentages['percentageSurveyWomenAnswered'] }}</b>% fueron mujeres y el <b>{{ $percentages['percentageSurveyMenAnswered'] }}</b>% hombres.</p>
				</div>
				<div class="left-content-wrapper left-content-wrapper-kinship">
					<p>En cuanto a parentesco: el <b>{{ $percentages['percentageSurveyMotherAnswered'] }}</b>% fueron madres, el <b>{{ $percentages['percentageSurveyFatherAnswered'] }}</b>% padres, 
					el <b>{{ $percentages['percentageSurveyAuntAnswered'] }}</b>% tias, el <b>{{ $percentages['percentageSurveyUncleAnswered'] }}</b>% tios, el <b>{{ $percentages['percentageSurveyGrandmaAnswered'] }}</b>% 
					abuelas, el <b>{{ $percentages['percentageSurveyGrandfatherAnswered'] }}</b>% abuelos, el <b>{{ $percentages['percentageSurveyGuardianAnswered'] }}</b>% tutoras, 
					el <b>{{ $percentages['percentageSurveyTutorAnswered'] }}</b>% tutores, el <b>{{ $percentages['percentageSurveyNannyAnswered'] }}</b>% cuidadoras, 
					el <b>{{ $percentages['percentageSurveyCaregiverAnswered'] }}</b>% cuidadores, el <b>{{ $percentages['percentageSurveyOtherAnswered'] }}</b>% otros.</p>						
				</div>
				<div class="left-content-wrapper left-content-wrapper-age">
					<p>Estudiando la edad de los participantes: el <b>{{ $percentages['percentageSurveyUnder20YearsAnswered'] }}</b>% tenían menos de 20 años, 
					el <b>{{ $percentages['percentageSurveyBetween20And29YearsAnswered'] }}</b>% tenían entre 20 y 29 años, el <b>{{ $percentages['percentageSurveyBetween30And39YearsAnswered'] }}</b>% tenían entre 30 y 
					39 años, el <b>{{ $percentages['percentageSurveyBetween40And49YearsAnswered'] }}</b>% tenían entre 40 y 49 años, el <b>{{ $percentages['percentageSurveyBetween50And59YearsAnswered'] }}</b>% tenían 
					entre 50 y 59 años, el <b>{{ $percentages['percentageSurveyOverEqual60YearsAnswered'] }}</b>% tenían más 60 años.</p>
				</div>
				<div class="right-content-wrapper right-content-wrapper-genre">
					<table width="100%" class="table">
						<tr>
							<td colspan="2" align="center">GENERO</td>
						</tr>
						<tr>
							<td>Mujer</td>
							<td align="center">{{ $totals['totalWomen'] }}</td>
						</tr>
						<tr>
							<td>Hombre</td>
							<td align="center">{{ $totals['totalMen'] }}</td>
						</tr>
					</table>
				</div>
				<div class="right-content-wrapper right-content-wrapper-kinship">
					<table width="100%" class="table">
						<tr>
							<td colspan="3" align="center">PARENTESCO</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td align="center">Mujer</td>
							<td align="center">Hombre</td>
						</tr>
						<tr>
							<td>Madre/Padre</td>
							<td align="center">{{ $totals['totalMother'] }}</td>
							<td align="center">{{ $totals['totalFather'] }}</td>
						</tr>
						<tr>
							<td>Tía/Tío</td>
							<td align="center">{{ $totals['totalAunt'] }}</td>
							<td align="center">{{ $totals['totalUncle'] }}</td>
						</tr>
						<tr>
							<td>Abuela/Abuelo</td>
							<td align="center">{{ $totals['totalGrandma'] }}</td>
							<td align="center">{{ $totals['totalGrandfather'] }}</td>
						</tr>
						<tr>
							<td>Tutora/Tutor</td>
							<td align="center">{{ $totals['totalGuardian'] }}</td>
							<td align="center">{{ $totals['totalTutor'] }}</td>
						</tr>
						<tr>
							<td>Cuidadora/Cuidador</td>
							<td align="center">{{ $totals['totalNanny'] }}</td>
							<td align="center">{{ $totals['totalCaregiver'] }}</td>
						</tr>
						<tr>
							<td>Otro</td>
							<td align="center" colspan="2">{{ $totals['totalOther'] }}</td>
						</tr>
					</table>
				</div>
				<div class="right-content-wrapper right-content-wrapper-age">
					<table width="100%" class="table">
						<tr>
							<td colspan="3" align="center">EDAD (años)</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td align="center">Frecuencia</td>
							<td align="center">Porcentaje</td>
						</tr>
						<tr>
							<td>menor 20</td>
							<td align="center">{{ $totals['totalUnder20Years'] }}</td>
							<td align="center">{{ $percentages['percentageSurveyUnder20YearsAnswered'] }}</td>
						</tr>
						<tr>
							<td>20 - 29</td>
							<td align="center">{{ $totals['totalBetween20And29Years'] }}</td>
							<td align="center">{{ $percentages['percentageSurveyBetween20And29YearsAnswered'] }}</td>
						</tr>
						<tr>
							<td>30 - 39</td>
							<td align="center">{{ $totals['totalBetween30And39Years'] }}</td>
							<td align="center">{{ $percentages['percentageSurveyBetween30And39YearsAnswered'] }}</td>
						</tr>
						<tr>
							<td>40 - 49</td>
							<td align="center">{{ $totals['totalBetween40And49Years'] }}</td>
							<td align="center">{{ $percentages['percentageSurveyBetween40And49YearsAnswered'] }}</td>
						</tr>
						<tr>
							<td>50 - 59</td>
							<td align="center">{{ $totals['totalBetween50And59Years'] }}</td>
							<td align="center">{{ $percentages['percentageSurveyBetween50And59YearsAnswered'] }}</td>
						</tr>
						<tr>
							<td>mayor 59</td>
							<td align="center">{{ $totals['totalOverEqual60Years'] }}</td>
							<td align="center">{{ $percentages['percentageSurveyOverEqual60YearsAnswered'] }}</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="page-break"></div>
			<div class="col-xs-12">
				<div class="left-content-wrapper">
					<h1>Factores Sociodemográficos</h1>
					<h2>Asistencia al Centro</h2>
					<div class="center-text-content">
						<p>El rango de respuestas obtenido ha sido muy amplio. No obstante, se observa un <b>{{ $percentages['percentageSurveyBetween1And12MonthsGoing'] }}</b>% personas llevan asistiendo al CAIT entre 1 y 12 
						meses; un <b>{{ $percentages['percentageSurveyBetween13And24MonthsGoing'] }}</b>% personas llevan asistiendo al CAIT entre 13 y 24 meses; un 
						<b>{{ $percentages['percentageSurveyBetween25And36MonthsGoing'] }}</b>% personas llevan asistiendo al CAIT entre 25 y 36 meses; un <b>{{ $percentages['percentageSurveyBetween37And48MonthsGoing'] }}</b>% 
						personas llevan asistiendo al CAIT entre 37 y 48 meses; un <b>{{ $percentages['percentageSurveyBetween49And60MonthsGoing'] }}</b>% personas llevan asistiendo al CAIT entre 49 y 60 meses; un 
						<b>{{ $percentages['percentageSurveyBetween61And72MonthsGoing'] }}</b>% personas llevan asistiendo al CAIT entre 61 y 72 meses;</p>
					</div>
				</div>
				<div class="right-content-wrapper">
					<div class="center-text-content">
						<table width="100%" class="table right-content-wrapper-table">
							<tr>
								<td colspan="2" align="center">Asistencia al centro</td>
							</tr>
							<tr>
								<td>1 a 12 meses</td>
								<td align="center">{{ $totals['totalBetween1And12MonthsGoing'] }}</td>
							</tr>
							<tr>
								<td>13 a 24 meses</td>
								<td align="center">{{ $totals['totalBetween13And24MonthsGoing'] }}</td>
							</tr>
							<tr>
								<td>25 a 36 meses</td>
								<td align="center">{{ $totals['totalBetween25And36MonthsGoing'] }}</td>
							</tr>
							<tr>
								<td>37 a 48 meses</td>
								<td align="center">{{ $totals['totalBetween37And48MonthsGoing'] }}</td>
							</tr>
							<tr>
								<td>49 a 60 meses</td>
								<td align="center">{{ $totals['totalBetween49And60MonthsGoing'] }}</td>
							</tr>
							<tr>
								<td>61 a 72 meses</td>
								<td align="center">{{ $totals['totalBetween61And72MonthsGoing'] }}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="page-break"></div>
			<div class="col-xs-12">
				<div class="left-content-wrapper">
					<h1>Factores Sociodemográficos</h1>
					<h2>Formación Académica</h2>
					<div class="center-text-content">
						<p>Los resultados obtenidos sobre el nivel de estudios de los participantes reflejan: porcentaje de población sin estudios (<b>{{ $percentages['percentageSurveyUneducatedStudiesAnswered'] }}</b>%), 
						porcentaje de población con estudios primarios (<b>{{ $percentages['percentageSurveyPrimaryStudiesAnswered'] }}</b>%), porcentaje de población con estudios medios 
						(<b>{{ $percentages['percentageSurveyHighSchoolEducationStudiesAnswered'] }}</b>%), porcentaje de población con estudios superiores (<b>{{ $percentages['percentageSurveyHigherEducationAnswered'] }}</b>%), 
						porcentaje de población con estudios postgrado (<b>{{ $percentages['percentageSurveyPostgraduateStudiesAnswered'] }}</b>%) y porcentaje de población con otros estudios 
						(<b>{{ $percentages['percentageSurveyOtherStudiesAnswered'] }}</b>%).</p>
						
						<p>Respecto al género (mujeres): porcentaje de población sin estudios (<b>{{ $percentages['percentageSurveyWomenUneducatedStudiesAnswered'] }}</b>%), 
						porcentaje de población con estudios primarios (<b>{{ $percentages['percentageSurveyWomenPrimaryStudiesAnswered'] }}</b>%), porcentaje de población con estudios medios 
						(<b>{{ $percentages['percentageSurveyWomenHighSchoolEducationStudiesAnswered'] }}</b>%), porcentaje de población con estudios superiores (<b>{{ $percentages['percentageSurveyWomenHigherEducationAnswered'] }}</b>%), 
						porcentaje de población con estudios postgrado (<b>{{ $percentages['percentageSurveyWomenPostgraduateStudiesAnswered'] }}</b>%) y porcentaje de población con otros estudios 
						(<b>{{ $percentages['percentageSurveyWomenOtherStudiesAnswered'] }}</b>%)</p>
						
						<p>Respecto al género (hombres): porcentaje de población sin estudios (<b>{{ $percentages['percentageSurveyMenUneducatedStudiesAnswered'] }}</b>%), 
						porcentaje de población con estudios primarios (<b>{{ $percentages['percentageSurveyMenPrimaryStudiesAnswered'] }}</b>%), porcentaje de población con estudios medios 
						(<b>{{ $percentages['percentageSurveyMenHighSchoolEducationStudiesAnswered'] }}</b>%), porcentaje de población con estudios superiores (<b>{{ $percentages['percentageSurveyMenHigherEducationAnswered'] }}</b>%), 
						porcentaje de población con estudios postgrado (<b>{{ $percentages['percentageSurveyMenPostgraduateStudiesAnswered'] }}</b>%) y porcentaje de población con otros estudios 
						(<b>{{ $percentages['percentageSurveyMenOtherStudiesAnswered'] }}</b>%).</p>
					</div>
				</div>
				<div class="right-content-wrapper">
					<div class="center-text-content">
						<table width="100%" class="table right-content-wrapper-table">
							<tr>
								<td>&nbsp;</td>
								<td colspan="2" align="center">Genero</td>
							</tr>
							<tr>
								<td>ESTUDIOS</td>
								<td align="center">Hombre</td>
								<td align="center">Mujer</td>
							</tr>
							<tr>
								<td>Sin Estudios</td>
								<td align="center">{{ $totals['totalMenUneducatedStudies'] }}</td>
								<td align="center">{{ $totals['totalWomenUneducatedStudies'] }}</td>
							</tr>
							<tr>
								<td>Primarios</td>
								<td align="center">{{ $totals['totalMenPrimaryStudies'] }}</td>
								<td align="center">{{ $totals['totalWomenPrimaryStudies'] }}</td>
							</tr>
							<tr>
								<td>Medios</td>
								<td align="center">{{ $totals['totalMenHighSchoolEducationStudies'] }}</td>
								<td align="center">{{ $totals['totalWomenHighSchoolEducationStudies'] }}</td>
							</tr>
							<tr>
								<td>Superiores</td>
								<td align="center">{{ $totals['totalMenHigherEducation'] }}</td>
								<td align="center">{{ $totals['totalWomenHigherEducation'] }}</td>
							</tr>
							<tr>
								<td>Postgrado</td>
								<td align="center">{{ $totals['totalMenPostgraduateStudies'] }}</td>
								<td align="center">{{ $totals['totalWomenPostgraduateStudies'] }}</td>
							</tr>
							<tr>
								<td>Otros Estudios</td>
								<td align="center">{{ $totals['totalMenOtherStudies'] }}</td>
								<td align="center">{{ $totals['totalWomenOtherStudies'] }}</td>
							</tr>
							<tr>
								<td>TOTAL</td>
								<td align="center">{{ $totals['totalMenStudies'] }}</td>
								<td align="center">{{ $totals['totalWomenStudies'] }}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="page-break"></div>
			<?php
				$facilitiesItems = array_filter($data, function($key) {
					return preg_match("/^I_/i", trim($key)) > 0;
				}, ARRAY_FILTER_USE_KEY);
				$min = min(array_column($facilitiesItems, 'min'));
				$max = max(array_column($facilitiesItems, 'max'));
				
				$minAverage = min(array_column($facilitiesItems, 'average'));
				$maxAverage = max(array_column($facilitiesItems, 'average'));
				
				$minValKeys = array_filter($facilitiesItems, function($item) use($minAverage) {
					return $item['average'] == $minAverage;
				});
				$maxValKeys = array_filter($facilitiesItems, function($item) use($maxAverage) {
					return $item['average'] == $maxAverage;
				});
			?>
			<div class="col-xs-12">
				<h1>Resultados.ICCAIT-R</h1>
				<h2>Instalaciones</h2>
				<div class="center-text-content">
					<div class="row">
						<p>Respecto a las instalaciones del CAIT, la media de las puntuaciones de los participantes ha oscilado entre <b>{{ $min }}</b> y <b>{{ $max }}</b>.</p>
						
						<p>
							@if(count($maxValKeys) > 1)
								Como mejores Items puntuados tenemos:
								<ul>
									@foreach($maxValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($maxValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> ha obtenido una puntuación superior (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
						
						<p>
							@if(count($minValKeys) > 1)
								Como peores Items puntuados tenemos:
								<ul>
									@foreach($minValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($minValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> es el peor puntuado (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
					</div>
					<div class="row">
						<table width="100%" class="table">
							<thead>
								<tr>
									<th>@lang('statistic.question')</th>
									<th>@lang('statistic.average')</th>
									<th>@lang('statistic.standard_deviation')</th>
									<th>@lang('statistic.mode')</th>
									<th>@lang('statistic.min')</th>
									<th>@lang('statistic.max')</th>
								</tr>
							</thead>
							<tbody>
								@foreach($facilitiesItems as $question => $value)
									<?php $object = (object) $value; ?>
									<tr>
										<td align="center">{{ $question }}: {{ $object->label }}</td>
										<td align="center">{{ $object->average }}</td>
										<td align="center">{{ $object->standard_deviation }}</td>
										<td align="center">{{ $object->mode }}</td>
										<td align="center">{{ $object->min }}</td>
										<td align="center">{{ $object->max }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="page-break"></div>
			<?php
				$facilitiesItems = array_filter($data, function($key) {
					return preg_match("/^STM_/i", trim($key)) > 0;
				}, ARRAY_FILTER_USE_KEY);
				$min = min(array_column($facilitiesItems, 'min'));
				$max = max(array_column($facilitiesItems, 'max'));
				
				$minAverage = min(array_column($facilitiesItems, 'average'));
				$maxAverage = max(array_column($facilitiesItems, 'average'));
				
				$minValKeys = array_filter($facilitiesItems, function($item) use($minAverage) {
					return $item['average'] == $minAverage;
				});
				$maxValKeys = array_filter($facilitiesItems, function($item) use($maxAverage) {
					return $item['average'] == $maxAverage;
				});
			?>
			<div class="col-xs-12">
				<h1>Resultados.ICCAIT-R</h1>
				<h2>Salas de Tratamiento y Material</h2>
				<div class="center-text-content">
					<div class="row">
						<p>En esta dimensión, la media de las puntuaciones de los participantes ha oscilado entre <b>{{ $min }}</b> y <b>{{ $max }}</b>.</p>
						
						<p>
							@if(count($maxValKeys) > 1)
								Como mejores Items puntuados tenemos:
								<ul>
									@foreach($maxValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($maxValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> ha obtenido una puntuación superior (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
						
						<p>
							@if(count($minValKeys) > 1)
								Como peores Items puntuados tenemos:
								<ul>
									@foreach($minValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($minValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> es el peor puntuado (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
					</div>
					<div class="row">
						<table width="100%" class="table">
							<thead>
								<tr>
									<th>@lang('statistic.question')</th>
									<th>@lang('statistic.average')</th>
									<th>@lang('statistic.standard_deviation')</th>
									<th>@lang('statistic.mode')</th>
									<th>@lang('statistic.min')</th>
									<th>@lang('statistic.max')</th>
								</tr>
							</thead>
							<tbody>
								@foreach($facilitiesItems as $question => $value)
									<?php $object = (object) $value; ?>
									<tr>
										<td align="center">{{ $question }}: {{ $object->label }}</td>
										<td align="center">{{ $object->average }}</td>
										<td align="center">{{ $object->standard_deviation }}</td>
										<td align="center">{{ $object->mode }}</td>
										<td align="center">{{ $object->min }}</td>
										<td align="center">{{ $object->max }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="page-break"></div>
			<?php
				$facilitiesItems = array_filter($data, function($key) {
					return preg_match("/^AU_/i", trim($key)) > 0 || preg_match("/^PE_/i", trim($key)) > 0;
				}, ARRAY_FILTER_USE_KEY);
				$min = min(array_column($facilitiesItems, 'min'));
				$max = max(array_column($facilitiesItems, 'max'));
				
				$minAverage = min(array_column($facilitiesItems, 'average'));
				$maxAverage = max(array_column($facilitiesItems, 'average'));
				
				$minValKeys = array_filter($facilitiesItems, function($item) use($minAverage) {
					return $item['average'] == $minAverage;
				});
				$maxValKeys = array_filter($facilitiesItems, function($item) use($maxAverage) {
					return $item['average'] == $maxAverage;
				});
			?>
			<div class="col-xs-12">
				<h1>Resultados.ICCAIT-R</h1>
				<h2>Personal Especializado</h2>
				<div class="center-text-content">
					<div class="row">
						<p>La media de las puntuaciones que los usuarios participantes han valorado de esta dimensión ha oscilado entre <b>{{ $min }}</b> y <b>{{ $max }}</b>.</p>
						
						<p>
							@if(count($maxValKeys) > 1)
								Como mejores Items puntuados tenemos:
								<ul>
									@foreach($maxValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($maxValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> ha obtenido una puntuación superior (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
						
						<p>
							@if(count($minValKeys) > 1)
								Como peores Items puntuados tenemos:
								<ul>
									@foreach($minValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($minValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> es el peor puntuado (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
					</div>
					<div class="row">
						<table width="100%" class="table">
							<thead>
								<tr>
									<th>@lang('statistic.question')</th>
									<th>@lang('statistic.average')</th>
									<th>@lang('statistic.standard_deviation')</th>
									<th>@lang('statistic.mode')</th>
									<th>@lang('statistic.min')</th>
									<th>@lang('statistic.max')</th>
								</tr>
							</thead>
							<tbody>
								@foreach($facilitiesItems as $question => $value)
									<?php $object = (object) $value; ?>
									<tr>
										<td align="center">{{ $question }}: {{ $object->label }}</td>
										<td align="center">{{ $object->average }}</td>
										<td align="center">{{ $object->standard_deviation }}</td>
										<td align="center">{{ $object->mode }}</td>
										<td align="center">{{ $object->min }}</td>
										<td align="center">{{ $object->max }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="page-break"></div>
			<?php
				$facilitiesItems = array_filter($data, function($key) {
					return preg_match("/^IT_/i", trim($key)) > 0;
				}, ARRAY_FILTER_USE_KEY);
				$min = min(array_column($facilitiesItems, 'min'));
				$max = max(array_column($facilitiesItems, 'max'));
				
				$minAverage = min(array_column($facilitiesItems, 'average'));
				$maxAverage = max(array_column($facilitiesItems, 'average'));
				
				$minValKeys = array_filter($facilitiesItems, function($item) use($minAverage) {
					return $item['average'] == $minAverage;
				});
				$maxValKeys = array_filter($facilitiesItems, function($item) use($maxAverage) {
					return $item['average'] == $maxAverage;
				});
			?>
			<div class="col-xs-12">
				<h1>Resultados.ICCAIT-R</h1>
				<h2>Información Técnica</h2>
				<div class="center-text-content">
					<div class="row">
						<p>En cuanto a la información técnica que los profesionales del CAIT proporcionan a las familias atendidas, la media de las puntuaciones de los participantes, en esta dimsensión, ha oscilado entre 
						<b>{{ $min }}</b> y <b>{{ $max }}</b>.</p>
						
						<p>
							@if(count($maxValKeys) > 1)
								Como mejores Items puntuados tenemos:
								<ul>
									@foreach($maxValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($maxValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> ha obtenido una puntuación superior (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
						
						<p>
							@if(count($minValKeys) > 1)
								Como peores Items puntuados tenemos:
								<ul>
									@foreach($minValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($minValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> es el peor puntuado (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
					</div>
					<div class="row">
						<table width="100%" class="table">
							<thead>
								<tr>
									<th>@lang('statistic.question')</th>
									<th>@lang('statistic.average')</th>
									<th>@lang('statistic.standard_deviation')</th>
									<th>@lang('statistic.mode')</th>
									<th>@lang('statistic.min')</th>
									<th>@lang('statistic.max')</th>
								</tr>
							</thead>
							<tbody>
								@foreach($facilitiesItems as $question => $value)
									<?php $object = (object) $value; ?>
									<tr>
										<td align="center">{{ $question }}: {{ $object->label }}</td>
										<td align="center">{{ $object->average }}</td>
										<td align="center">{{ $object->standard_deviation }}</td>
										<td align="center">{{ $object->mode }}</td>
										<td align="center">{{ $object->min }}</td>
										<td align="center">{{ $object->max }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="page-break"></div>
			<?php
				$facilitiesItems = array_filter($data, function($key) {
					return preg_match("/^SAT_/i", trim($key)) > 0 || preg_match("/^IF_/i", trim($key)) > 0;
				}, ARRAY_FILTER_USE_KEY);
				$min = min(array_column($facilitiesItems, 'min'));
				$max = max(array_column($facilitiesItems, 'max'));
				
				$minAverage = min(array_column($facilitiesItems, 'average'));
				$maxAverage = max(array_column($facilitiesItems, 'average'));
				
				$minValKeys = array_filter($facilitiesItems, function($item) use($minAverage) {
					return $item['average'] == $minAverage;
				});
				$maxValKeys = array_filter($facilitiesItems, function($item) use($maxAverage) {
					return $item['average'] == $maxAverage;
				});
			?>
			<div class="col-xs-12">
				<h1>Resultados.ICCAIT-R</h1>
				<h2>Satisfacción e Intención Futura</h2>
				<div class="center-text-content">
					<div class="row">
						<p>Según los resultados obtenidos, con respecto a la decisión de acudir al CAIT y la intención futura de los usuarios participantes, la media de las puntuaciones de los participantes, en esta dimsensión, 
						ha oscilado entre <b>{{ $min }}</b> y <b>{{ $max }}</b>.</p>
						
						<p>
							@if(count($maxValKeys) > 1)
								Como mejores Items puntuados tenemos:
								<ul>
									@foreach($maxValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($maxValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> ha obtenido una puntuación superior (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
						
						<p>
							@if(count($minValKeys) > 1)
								Como peores Items puntuados tenemos:
								<ul>
									@foreach($minValKeys as $key => $value)
										<?php $object = (object) $value; ?>
										<li>Item <b>{{ $key }}</b>: <b>{{ $object->label }}</b> que ha obtenido una puntuación de <b>{{ $object->average }}</b></li>
									@endforeach
								</ul>
							@else
								@foreach($minValKeys as $key => $value)
									<?php $object = (object) $value; ?>
									El ítem <b>{{ $key }}</b>: <b>{{ $object->label }}</b> es el peor puntuado (<b>{{ $object->average }}</b>)
								@endforeach
							@endif
						</p>
					</div>
					<div class="row">
						<table width="100%" class="table">
							<thead>
								<tr>
									<th>@lang('statistic.question')</th>
									<th>@lang('statistic.average')</th>
									<th>@lang('statistic.standard_deviation')</th>
									<th>@lang('statistic.mode')</th>
									<th>@lang('statistic.min')</th>
									<th>@lang('statistic.max')</th>
								</tr>
							</thead>
							<tbody>
								@foreach($facilitiesItems as $question => $value)
									<?php $object = (object) $value; ?>
									<tr>
										<td align="center">{{ $question }}: {{ $object->label }}</td>
										<td align="center">{{ $object->average }}</td>
										<td align="center">{{ $object->standard_deviation }}</td>
										<td align="center">{{ $object->mode }}</td>
										<td align="center">{{ $object->min }}</td>
										<td align="center">{{ $object->max }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="page-break"></div>
			<div class="col-xs-12">
				<h1>El Inventario de Calidad para Centros de Atención Temprana en su versión reducida (ICCAIT-R) es una herramienta válida y fiable para medir la calidad percibida, la satisfacción y la intención futura de las 
				familias que acuden a un Centro de Atención Temprana:</h1>
				<div class="center-text-content">
					<ul class="references-list">
						<li>Romero-Galisteo RP, Gálvez Ruiz P, Blanco Villaseñor A, Rodríguez-Bailón M, González-Sánchez M. 2020. What families really think about the quality of early intervention centers: a 
						perspective from mixed methods. PeerJ 8:e10193 http://doi.org/10.7717/peerj.10193</li>
						<li>Romero-Galisteo, RP, Blanco-Villaseñor, Á, Moreno-Morales, N, Gálvez-Ruiz, P. Early intervention and perceived quality, Medicine: April 2019 - Volume 98 - Issue 15 - p 
						e15173 http://doi.org/10.1097/MD.0000000000015173</li>
						<li>Romero-Galisteo, RP, Morales-Sánchez, V, Hernández-Mendo, A. Desarrollo de una herramienta para la evaluación de la calidad percibida en los centros de atención infantil temprana, Anales de 
						psicología, 2015, vol. 31, no 1 (enero), 127-136 http://dx.doi.org/10.6018/analesps.31.1.158191</li>
					</ul>
				</div>
			</div>
			<!--<div class="page-break"></div>
			<div class="col-xs-12">
				<h1>Conclusiones</h1>
				<div class="center-text-content">
					<p>A partir de los resultados obtenidos y mostrados a lo largo de este informe, consideramos que el servicio evaluado cuenta con una percepción de la calidad muy satisfactoria, si bien existen algunos 
					aspectos de los que cabe la posibilidad de mejora.</p>
					<div class="row">
						<div class="left-content-wrapper half-content-wrapper">
							<h4>Fortalezas</h4>
							<ul>
								<li>Limpieza del centro adecuada</li>
								<li>Material utilizado en las terapias</li>
								<li>Coordinación del personal especializado</li>
								<li>Cercanía en el trato</li>
								<li>Informes sobre la progresión del usuario</li>
							</ul>
						</div>
						<div class="right-content-wrapper half-content-wrapper right-half-content-wrapper">
							<h4>Debilidades</h4>
							<ul>
								<li>No es fácil llegar al centro en transporte público</li>
								<li>Espacio de las salas de tratamiento insuficiente</li>
								<li>Recibe indicaciones por parte de los profesionales para trabajar con el usuario</li>
							</ul>
						</div>
					</div>
				</div>
			</div>-->
		</div>
    </body>
</html>
