<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
    <head>
        <title>Estadisticas</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style type="text/css">
            table {
                background-color:#444;
                font-size: 14px;
            }
            table th {
                background-color: #444;
                color: #fff;
                font-weight: normal;
            }
            table tr {
                background-color: #fff;
            }
            .text-center {
                text-align: center;
            }
            .left-content-wrapper {
                position: absolute;
                left: 0;
                width: 30%;
            }
            .center-content-wrapper {
                position: absolute;
                left: 12%;
                width: 35%;
            }
            .right-content-wrapper {
                position: absolute;
                left: 40%;
                width: 50%;
            }
			.center-content-name {
                font-size: 25px;
				font-weight: bold;
            }
			.center-content-code-title {
                font-size: 20px;
            }
			.center-content-code {
				font-size: 25px;
				font-weight: bold;
				border: 1px solid;
				padding: 1%;
            }
        </style>
    </head>
    <body bgcolor="#ffffff" vlink="blue" link="blue">
		<?php $top = 0; ?>
		@for($i = 1; $i <= 5; $i++)
			<div class="left-content-wrapper" style="top: {{$top}}%;">
				<div class="row"><img src="{{ res('/assets/images/logo_lateral.png') }}" alt="{{env('APP_NAME')}}" id="loginLogo" style="width: 50%;"></div>
				<div class="row"><img src="{{ res('/assets/images/logo_uma.jpg') }}" alt="{{env('APP_NAME')}}" id="loginLogo" style="width: 50%;"></div>
			</div>
			<div class="center-content-wrapper text-center" style="top: {{$top}}%;">
				<div class="row"><img src="{{ res('/assets/images/QR-code.png') }}" alt="{{env('APP_NAME')}}" id="loginLogo" style="width: 50%;"></div>
			</div>
			<div class="right-content-wrapper" style="top: {{$top}}%;">
				<div class="text-justify">
					<div class="row center-content-name text-center">
						<span>{{ $user_name }}</span>
					</div>
					<br/>
					<div class="row center-content-code-title text-center">
						<span>Código Del Centro</span>
					</div>
					<br/>
					<div class="row center-content-code text-center">
						<span>{{ $code }}</span>
					</div>
				</div>
			</div>
			<?php $top += 20; ?>
			@if($i < 5)
				<div class="row" style="position: absolute; top: {{$top-5}}%; width=100%;">
					<hr>
				</div>
			@endif
		@endfor
    </body>
</html>
