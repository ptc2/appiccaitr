@extends('edit') 
<!-- Page content -->
@section('edit-master')
<div class="row">
    <div class="col-md-4">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang($module . '.user')</legend>
            <div class="form-group">
				<div class="col-xs-6">
					<div class="col-sm-12">
						<label>@lang($module . '.user_name')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $item->user->userData->name}}</label>
					</div>
				</div>
            </div>
        </fieldset>
    </div>
	<div class="col-md-8">
        <fieldset class="content-group">
            <legend class="bordered-legend text-theme-secondary ">@lang($module . '.member')</legend>
            <div class="form-group">
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.genre')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">@if ($item->socioData->genre == 1) <span>Masculino</span> @else <span>Femenino</span> @endif </label>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.age')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $item->socioData->age}}</label>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.studies')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $item->socioData->studies->name }}</label>
					</div>
				</div>
				@if (!empty($item->socioData->other_studies))
					<div class="col-xs-3">
						<div class="col-sm-12">
							<label>@lang($module . '.other_studies')</label>
						</div>
						<div class="col-sm-12">
							<label style="font-weight:bold">{{ $item->socioData->other_studies }}</label>
						</div>
					</div>
				@endif
            </div>
			<div class="form-group">
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.kinship')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $item->socioData->kinship->name }}</label>
					</div>
				</div>
				@if (!empty($item->socioData->other_kinship))
					<div class="col-xs-3">
						<div class="col-sm-12">
							<label>@lang($module . '.other_kinship')</label>
						</div>
						<div class="col-sm-12">
							<label style="font-weight:bold">{{ $item->socioData->other_kinship }}</label>
						</div>
					</div>
				@endif
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.time_going')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $item->socioData->time_going }}</label>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="col-sm-12">
						<label>@lang($module . '.other_center')</label>
					</div>
					<div class="col-sm-12">
						<label style="font-weight:bold">{{ $item->socioData->other_center}}</label>
					</div>
				</div>
            </div>
        </fieldset>
    </div>
</div>
@if (count($item->ratings) > 0)
	<div class="row">
		<div class="col-md-12">
			<fieldset class="content-group">
				<legend class="bordered-legend text-theme-secondary ">@lang($module . '.ratings')</legend>
				<?php $current_table = 1; $total_tables = ceil(count($item->ratings) / 10); ?>
				@while( $current_table <= $total_tables )
					<?php $firstItemToShowIndex = ($current_table - 1) * 10; $ratings_table = array_slice($item->ratings->toArray(), $firstItemToShowIndex, 10);  ?>
					<table class="table table-bordered">
						<tr>
							<td>@lang($module . '.rating_question')</td>
							@foreach($ratings_table as $rating)
								<?php $ratingObj = (object) $rating; ?>
								<td>{{ $ratingObj->question }}</td>
							@endforeach
						</tr>
						<tr>
							<td>@lang($module . '.rating_value')</td>
							@foreach($ratings_table as $rating)						
								<?php $ratingObj = (object) $rating; ?>
								<td>{{ $ratingObj->value }}</td>
							@endforeach
						</tr>					
					</table>
					<?php $current_table++; ?>
					<br/>
				@endwhile
			</fieldset>
		</div>
	</div>
@endif
@stop