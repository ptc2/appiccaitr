<?php

use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Models\Countries;
use Stringy\StaticStringy as Stringy ;
use Illuminate\Http\Request;

/**
 * Custom macros
*/

Form::macro('mySelectFieldCountries', function($item, $field, $module, $errors = null, $rows = 6)
{
    $countries = Countries::lists('nombre', 'id')->put(0, trans('global.select-none'))->toArray();
    ksort($countries);

    $params = [
        'id'    => $field,
        'class' => 'form-control error country chosen',
        'cols'  => $rows,
    ];

    // por defecto españa
    if (is_null($item->$field)) {
        $item->$field = Countries::SPAIN;
    }

    return Form::mySelectField($item, $field, $module, $countries, $errors, $params);
});

/**
 * Input text field colum 6
 */
Form::macro('myTextField', function ( $item, $field, $module, $errors = null, $params = array(), $isFloat = false)
{
    $relatedField = !empty($params['related_id']) ? $params['related_id'] : $field;
    if( Helper::isRequired( $item, $field ) || Helper::isRequiredLang( $item, $field ))
        $required = 'required';
    else
        $required = '';

    if(!empty($params['required'])) 
    {
        $required .= ' ' . $params['required'];
    }
    
    $out = '';

    if(!empty($params['label']))
    {
        $out .= '   <div class="col-sm-1">';
        $out .= '       <label for="' . $relatedField . '">' . trans($module . '.' . $field) . '</label>';
        $out .= '   </div>';
        $out .= '   <div class="col-sm-5">';
        $out .= '       <label style="font-weight:bold">'.$item->{$field}.'</label>';
        $out .= '       <input type="hidden" id="'. $relatedField .'" name="'. (!empty($params['name']) ? $params['name'] : $field) .'" value="'.$item->{$field}.'"></input>';
        $out .= '   </div>';
    }
    else
    {
        $out .= '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '"';

        if(!empty($params['style']))
            $out .= 'style="color:red;'. (empty($params['style']) ? '' : $params['style']) . '"';

        $out .= '>';

        $out .= '<label class="' . $required . '" for="' . $relatedField . '">' . trans($module . '.' . $field) . '</label>';

        if (!empty($params['langs']) && $params['langs']==true && sizeof(Config::get('app.available_languages'))>1){
            foreach (Config::get('app.available_languages') as $key=>$lang) {
                $fieldLang=$field.'_'.$lang;
                $params['addon']=strtoupper($lang);    
        
                if (! empty($params['icon'])) {
                    $out .= '<div class="input-group">';
                    $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
                }
                if (! empty($params['addon'])) {
                    $out .= '<div class="input-group haslang" data-locale="'. $lang .'" '.($key!=0?'style="display:none"':'').' >';
                }
                $old = request()->old($fieldLang);

                $placeholder = (trans($module . '.' . $field . '.placeholder') == $module . '.' . $field . '.placeholder')?trans($module . '.' . $field) : trans($module . '.' . $field . '.placeholder');
                $out .= '<input ' . $required . ' ';

                $value = '';
                if (empty($old)) {
                    if (!empty($item->{$fieldLang})) {
                        $value = $item->{$fieldLang};
                        if ($isFloat) {
                            $value = float_format($value);
                        }
                    }
                } else {
                    $value = $old;
                }

                $inputName = $fieldLang;
                if(!empty($params['name'])) {
                    $inputName = $params['name'] . '_' . $lang;
                }

                $inputId = $fieldLang;
                if(!empty($params['id'])) {
                    $inputId = $params['id'] . '_' . $lang;
                }

                // type
                $out .= 'type="'. (empty($params['type']) ? 'text' : $params['type'] ) .'" ';
                $out .= 'value="' . $value . '" id="' . $inputId . '" name="' . $inputName . '" placeholder="' . $placeholder . '"';
                $out .= (!empty($params['readonly']) && $params['readonly'] ? ' readonly ' : '');
                // class
                $out .= ' class="' . (empty($params['class']) ? 'form-control error' : $params['class']) . '"';
                // maxlength
                $out .= ! empty($params['maxlength']) ? ' maxlength="' . $params['maxlength'] . '" ' : '';
                $out .= ! empty($params['attributes']) ? $params['attributes'] : '';
                $out .= '>';

                if (! empty($params['addon'])) {
                    $out .= '<span class="input-group-addon">' . $params['addon'] . '</span>';
                }
                if (! empty($params['icon'])||! empty($params['addon'])) {
                    $out .= '</div>';
                }

                $out .= ($errors) ? $errors->first($fieldLang, '<label for="' . $inputId . '" data-locale="'. $lang .'" class="error">:message</label><br/>') : '';
            }
        }
        else
        {   
        
            if (! empty($params['icon'])) {
                $out .= '<div class="input-group">';
                $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
            }
            if (! empty($params['addon'])) {
                $out .= '<div class="input-group">';
            }
            $old = request()->old($field);

            $placeholder = (trans($module . '.' . $field . '.placeholder') == $module . '.' . $field . '.placeholder')?trans($module . '.' . $field) : trans($module . '.' . $field . '.placeholder');
            $out .= '<input ' . $required . ' ';

            $value = '';
            if (empty($old)) {
                if (!empty($item->{$field})) {
                    $value = $item->{$field};
                    if ($isFloat) {
                        $value = float_format($value);
                    }
                }
            } else {
                $value = $old;
            }

            // type
            $out .= 'type="'. (empty($params['type']) ? 'text' : $params['type'] ) .'" ';
            $out .= 'value="' . $value . '" id="' . (empty($params['id']) ? $field : $params['id']) . '" name="' . (empty($params['name']) ? $field : $params['name']) . '" placeholder="' . $placeholder . '"';
            $out .= (!empty($params['readonly']) && $params['readonly'] ? ' readonly ' : '');
            // class
            $out .= ' class="' . (empty($params['class']) ? 'form-control error' : $params['class']) . '"';
            // maxlength
            $out .= !empty($params['maxlength']) ? ' maxlength="' . $params['maxlength'] . '" ' : '';
            $out .= !empty($params['attributes']) ? $params['attributes'] : '';
            $out .= '>';

            if (! empty($params['addon'])) {
                $out .= '<span class="input-group-addon">' . $params['addon'] . '</span>';
            }
            if (! empty($params['icon'])||! empty($params['addon'])) {
                $out .= '</div>';
            }

            $out .= ($errors) ? $errors->first($relatedField, '<label for="' . $relatedField . '" class="error">:message</label>') : '';
        }

        $out .= '</div>';

    }

    return $out;
});

/**
 * Input text field colum 12
 */
Form::macro('myTextField12', function ($item, $field, $module, $errors = null, $params = array())
{

    $params['cols'] = '12';
    return Form::myTextField($item, $field, $module, $errors, $params);
});

/**
 * Input currency field colum 6
 */
Form::macro('myCurrencyField', function ($item, $field, $module, $errors = null, $params = array())
{
    return Form::myTextField($item, $field, $module, $errors, $params);
});



Form::macro('myPasswordField', function ($item, $field, $module, $errors = null, $params = array())
{

    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    $out = '';
    $out .= '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= '<label class="' . $required . '" for="' . $field . '">' . trans($module . '.' . $field) . '</label>';

    if (! empty($params['icon'])) {
        $out .= '<div class="input-group">';
        $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
    }

    $placeholder = (trans($module . '.' . $field . '.placeholder') == $module . '.' . $field . '.placeholder')?trans($module . '.' . $field) : trans($module . '.' . $field . '.placeholder');
    $out .= '<input type="password" id="' . $field . '" name="' . $field . '" placeholder="' . $placeholder . '" ' . $required . ' ';
    // class
    $out .= ' class="' . (empty($params['class']) ? 'form-control error' : $params['class']) . '"';
    // maxlength
    $out .= ! empty($params['maxlength']) ? ' maxlength="' . $params['maxlength'] . '" ' : '';

    // security
    $out .= !empty($params['security']) ? ' data-password-security="true"' : '';

    // end
    $out .= '>';

    if($field == 'password' || $field == 'password_confirmation') 
        $out .= '<span style="position: absolute; top: 38px; right: 15px;" class="showpass fa fa-eye"></span>';

    if (! empty($params['icon'])) {
        $out .= '</div>';
    }
    $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label>') : '';
    $out .= '</div>';

    return $out;
});

/**
 * Input select field
 */
Form::macro('mySelectField', function ($item, $field, $module, $select_values, $errors = null, $params = array())
{
    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    if(!empty($params['required'])){
        $required .= ' ' . $params['required'];
    }

    $value = isset($params['value']) ? $params['value'] : (isset($item->$field) ? $item->$field : null);

    $id =  isset($params['id'])
                ? $params['id']
                : $field ;
    $params['id'] = $id;
    $label =  isset($params['label'])
                ? $params['label']
                : $module . '.' . $field ;

    $out = '';
    $out .= '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';

    if(empty($params['hideLabel'])) {
        $out .= '<label class="' . $required . '" for="' . $id . '">' . trans($label) . '</label>';
    }

    if (! empty($params['icon'])) {
        $out .= '<div class="input-group">';
        $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
    }
        
    // Remove empty option
    if(isset($params['remove_entry_empty'])) {
        unset($select_values[""]);
        unset($select_values["0"]);
    }

    $out .= Form::select($id, $select_values, $value, $params);

    if (! empty($params['icon'])) {
        $out .= '</div>';
    }

    $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label>') : '';
    $out .= '</div>';

    return $out;
});

/**
 * Related input select field
 */
Form::macro('myRelatedSelectField', function ($item, $field, $module, $relatedModel, $select_values, $errors = null, $params = array())
{
    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    $value = isset($params['value'])
                ? $params['value']
                : (isset($item->$field) ? $item->$field : null) ;
    $id =  isset($params['id'])
                ? $params['id']
                : $field ;
    $params['id'] = $id;
    $label =  isset($params['label'])
                ? $params['label']
                : $module.'.'.$field ;

    $out = '';
    $out .= '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '"';

    if(!empty($params['style']))
        $out .= 'style="'. (empty($params['style']) ? '' : $params['style']) . '"';

    $out .= '>';

    $out .= '<label class="'.$required.'" for="'.$id.'">'.trans($label).'</label>';

    if (! empty($params['icon'])) {
        $out .= '<div class="input-group">';
        $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
    }

    $params['class'] = empty($params['class']) ? 'form-control' : $params['class'];
    $params['autocomplete'] = 'off';
    $params[] = $required;

    unset($params['cols']);
    unset($params['value']);

    $out .= Form::select('relation_'.$relatedModel.'_'.$id, $select_values, $value, $params);

    if (! empty($params['icon'])) {
        $out .= '</div>';
    }

    $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label>') : '';
    $out .= '</div>';

    return $out;
});

/**
 * Input date field
 */
Form::macro('myDateTime', function ($item, $field, $module, $errors = null, $params = array()) {

    $params['type'] = 'datetimepicker';
    return Form::myDate($item, $field, $module, $errors, $params);

});

/**
 * Input date field
 */
Form::macro('myDate', function ($item, $field, $module, $errors = null, $params = array()) {

    

    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    $defaultsParams = array(
        'readonly' => false
    );

    $params = array_merge($defaultsParams, $params);

    $out = '';
    $out .= '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= '<label class="' . $required . '" for="' . $field . '">' . trans($module . '.' . $field) . '</label>';
    $out .= '<div class="input-group">';
    $icon = 'fa fa-calendar';
    if (! empty($params['icon'])) {
        $out .= $params['icon'];
    }
    $out .= '<span class="input-group-addon"><i class="'.$icon.'"></i> </span>';

    // Get old value
    $old = request()->old($field);
    // Get the placeholder
    $placeholder = (trans($module . '.' . $field . '.placeholder') == $module . '.' . $field . '.placeholder') ? trans($module . '.' . $field) : trans($module . '.' . $field . '.placeholder');
    $out .= '<input type="text" value="'.(empty($item->{$field}) ? request()->old($field) : $item->{$field}).'" placeholder="'.$placeholder.'" ' . $required . ($params['readonly'] ? ' readonly ' : '').' class="form-control '.(empty( $params[ 'type' ] ) ? 'pickadate' : $params[ 'type' ] ).' margin-top-none" id="'.$field.'" name="'.$field.'">';
    $out .= '</div>';

    $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label>') : '';

    $out .= '</div>';
    return $out;
});

Form::macro('myAutocomplete', function ($item, $field, $module, $errors = null, $params = array())
{
    

    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    $out = '';
    $out .= '<div class="col-sm-' . (empty($params['cols']) ? '12' : $params['cols']) . '">';
    $out .= '<label class="' . $required . '" for="' . $field . '">' . trans($module . '.' . $field) . '</label>';

    if (! empty($params['icon'])) {
        $out .= '<div class="input-group">';
        $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
    }

    $out .= '<input ' . $required . ' type="text" value="' . (empty($item->{$field}) ? request()->old($field) : $item->{$field}) . '" id="' . $field . '" name="' . $field . '" placeholder="' . trans($module . '.' . $field) . '"';
    // class
    $out .= ' class="' . (empty($params['class']) ? 'form-control error' : $params['class']) . '"';
    // maxlength
    $out .= ! empty($params['maxlength']) ? ' maxlength="' . $params['maxlength'] . '" ' : '';
    $out .= '>';

    if (! empty($params['icon'])) {
        $out .= '</div>';
    }

    $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label>') : '';
    $out .= '</div>';

    $related = '';
    if (! empty($params['related_fields'])){
        foreach ($params['related_fields'] as $related_field){
            $related .= '"'.$related_field .'": $("#' . $related_field . '").val() ,';
        }
        $related = rtrim($related, ' ,');
    }

    $response_field = $field;
    if (! empty($params['callback'])){
        $tmp_array = explode('/',$params['callback']);
        $response_field = array_pop($tmp_array);
    }

    $out .= '<script type="text/javascript">
                $("#' . $field . '").autocomplete({
                    source: function( request, response ) {
                                $.ajax({
                                    url: "'. ( ! empty($params['callback']) ? $params['callback'] : ('/' . $module . '/autocomplete/' . $field ) ) . '",
                                    method: "get",
                                    dataType: "json",
                                    data: { "search_autocomplete": $("#' . $field . '").val(), '. $related .' },
                                    success: function( data ) {

                                            var that = $("#' . $field . '");
                                            var tip = $( that ).siblings( "span.ui-helper-hidden-accessible:first" )[0];
                                            var newDiv = "<div class=\'col-sm-12\'></div>";

                                            $( tip ).appendTo( $(that).parent().parent() );
                                            if( $( that ).parent( "div.input-group" ).length == 0  )
                                                $( tip ).wrap( newDiv );

                                            response( $.map( data.items, function( item ) {
                                            return {
                                                label: item.'.$response_field.',
                                                value: item.'.$response_field.'
                                            }
                                        }));
                                    }
                                });
                            },
                    minLength: 2,
                    delay: 300,
                    select: function( event, ui ) {
                        if (ui.item){
                            $(this).val(ui.item.value);
                        }
                        return false;
                    }
                });
             </script>';
    return $out;
});

Html::macro('divCommonSearch', function ($module, $related, $params=false)
{
    $camelizeModule = Stringy::upperCamelize($module);  
    $class = '\\App\\Models\\' . $camelizeModule;
    $model = new $class();

    $out  = '<div id="'.$module.'GridSearch" class="gridCommonSearchFilters" rel="'.$module.'Grid"> ';

    /*if( !$related && !empty($model->filters))
        $out .= '<button type="button" class="btn btn-primary btn-clean-search " rel="'.$module.'Grid"><i class="fa fa-eraser"></i> Limpiar</button><button type="button" class="btn btn-primary btn-advanced-search " rel="'.$module.'Grid">'.trans('global.advanced_search').'</button>';*/

    $out .= '<div class="input-group margin-bottom searchGroup"><span class="input-group-addon"><i class="fa fa-search"></i> </span>';
    $out .= '<input id="sSearch" name="sSearch" class="form-control gridSearchFilter" type="search" placeholder="'. trans('global.search') .'" aria-controls="'.$module.'Grid"  rel="'.$module.'Grid" value="'.(isset($params) && isset($params["searchValue"]) ? $params["searchValue"] : '').'" />';
    $out .= '</div>';
    $out .= '</div>';

    return $out;
});

Html::macro('myDataTable', function ($module, $related=null, $params = array(), $columnsOnly = null)
{
    $camelizeModule = Stringy::upperCamelize($module);  
    $relationsColumns = [];

    $out = '';
    $class = '\\App\\Models\\' . $camelizeModule;
    if (!class_exists($class)){
        $out .= 'Class ' . $class . ' doesn\'t exists!';
    } else {

        $model = new $class();

        if ($columnsOnly && count($columnsOnly) > 0) {
            $columnnames = implode(',', $columnsOnly);
        } else {
            if( $related && isset( $model->gridColumnsRelated ) ) {
                $relatedModel = isset($params['relatedModel']) ? $params['relatedModel'] : null;
                $columns = $model->gridColumns;

                if(!empty($relatedModel)) {
                    $columns = isset($model->gridColumnsRelated[$relatedModel]) ? $model->gridColumnsRelated[$relatedModel] : $columns;
                }

                //$columnnames = implode(',', $columns);
            }elseif($model->relationsGridColumns){
                $columns = $model->gridColumns;
                foreach($model->relationsGridColumns as $alias => $field) {
                    if(array_key_exists('field', $field)){
                        $columns[] = $alias;
                        $relationsColumns[] = $alias;
                    }   
                }
                $columnnames = $columns;
            }else {
                $columnnames = $model->gridColumns;
            }
            //$columnnames = implode(',', $columns);
        }

        $out = '';

        if( !$related ){
            // Div search
            $out .= '<div class="datatable-header"><div id="'.$module.'GridSearch" class="gridSearchFilters" rel="'.$module.'Grid"> ';

            // Div common search
            $out .= Html::divCommonSearch( $module, $related, $params );


            // Div advanced search
            $out .= '<div class="customSearchAdvanced" rel="'.$module.'Grid"><div class="row">'; //style="display: none;"
            $advanced_search_template = (empty($params['advanced_search_template']))
                                            ? $module.'.advanced_search'
                                            : $params['advanced_search_template'] ;
            
            if (! view()->exists($advanced_search_template)) {
                $advanced_search_template = 'advanced_search';
            }
            $out .= view()->make($advanced_search_template)
                                ->with('module', $module)
                                ->with('params', $params)
                                ->render();
            $out .= '</div></div>';

            $out .= '</div></div>';

        }

        $out .= '<div rel="'.$module.'Grid">';

        /*$colum = Auth::user()->profile()->ofColumns($module)->get();
        $profile_columns = ($colum->count()? $colum->first()->profile_value : '');

        $columnnames = ($colum->count()?$profile_columns:$columnnames);*/
		
		$columnnames = array_diff($columnnames, $model->gridColumnsToHide);		
		
        $out .= '<table id="'.$module.'Grid" data-module="'.$module.'"' . (($related)?'related="'.$related.'" ':'') .  'data-colums="'.implode(',',$columnnames).'" class="table table-bordered table-hover datatable-show-all dataTable no-footer ' . (isset( $params[ 'mainClass' ] ) ? $params[ 'mainClass' ] : 'dataTable') . '" data-paginate="'. (isset( $params[ 'paginate' ] ) ? $params[ 'paginate' ] : 'true') .'" data-edit="'. ($model->options['can_edit']?: 'false') .'" data-delete="'. ($model->options['can_delete']?: 'false') .'" data-see="'. ($model->options['can_see']?: 'false') .'">';
        $out .= '<thead><tr>';
        foreach ($columnnames as $column){
            if(in_array($column, $relationsColumns)){
                $modName = isset($model->relationsGridColumns[$column]['table'])? $model->relationsGridColumns[$column]['table'] : $module;
            }else{
                $modName = $module;
            }
            $out .= '<th>'.trans($modName . '.' . $column).'</th>';
        }
        $out .= '</tr></thead><tbody></tbody></table>';

        $out .= '</div>';
    }

    return $out;
});

Html::macro('modalsGridManyRelations', function ($module, $related=null, $params = array(), $columnsOnly = [])
{
    $camelizeModule = Stringy::upperCamelize($module);  
    $relationsColumns = [];
    $relationsManyColumns = [];

    $out = '';
    $class = '\\App\\Models\\' . $camelizeModule;

    if (!class_exists($class))
        return 'Class ' . $class . ' doesn\'t exists!';

    $model = new $class();

    if($model->relationsManyGridColumns && count($model->relationsManyGridColumns) > 0) {
        foreach ($model->relationsManyGridColumns as $key => $modal) {
            $out .= '<div class="modal fade" id="' . $modal['table'] . '_modal" tabindex="-1" role="dialog" aria-labelledby="' . $modal['table'] . '_modalLabel" aria-hidden="true" data-modal-relation-many="modal" data-service="' . $modal['service'] . '" data-relation="' . $key . '">
                        <div class="modal-dialog" role="document" style="width: 70%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="' . $modal['table'] . '_modalLabel">' . trans($key . '.' . $modal['text']) . '</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" data-conten-modal-body="' .$modal['table'] . '_modal">
                                    <div>Cargando ...</div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-primary" data-save-many="modal">Guardar todo</button>
                                </div>
                            </div>
                        </div>
                    </div>';
        } 
    }


    return $out;
});

Form::macro('myNumberField', function ( $item, $field, $module, $errors = null, $params = array())
{
    
    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    $out = '';
    $out .= '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= '<label class="' . $required . '" for="' . $field . '">' . trans($module . '.' . $field) . '</label>';

    if (! empty($params['icon'])) {
        $out .= '<div class="input-group">';
        $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
    }
    $old = request()->old($field);

    $placeholder = (trans($module . '.' . $field . '.placeholder') == $module . '.' . $field . '.placeholder')?trans($module . '.' . $field) : trans($module . '.' . $field . '.placeholder');
    $out .= '<input ' . $required . ' ';

	$value = '';
	if (empty($old)) {
		if (!empty($item->{$field})) {
			$value = $item->{$field};
		}
	} else {
		$value = $old;
	}

    // type
    $out .= 'type="'. (empty($params['type']) ? 'number' : $params['type'] ) .'" ';
    $out .= 'value="' . $value . '" id="' . $field . '" name="' . $field . '" placeholder="' . $placeholder . '"';

    //Attributes
    $out .= 'min="'. (empty($params['min']) ? '0' : $params['min'] ) .'" ';
    $out .= 'max="'. (empty($params['max']) ? '100' : $params['max'] ) .'" ';
    $out .= 'step="'. (empty($params['step']) ? '1' : $params['step'] ) .'" ';

    // class
    $out .= ' class="' . (empty($params['class']) ? 'form-control error' : $params['class']) . '"';
    // maxlength
    $out .= ! empty($params['maxlength']) ? ' maxlength="' . $params['maxlength'] . '" ' : '';
    $out .= ! empty($params['attributes']) ? $params['attributes'] : '';
    $out .= '>';

    if (! empty($params['icon'])) {
        $out .= '</div>';
    }

    $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label>') : '';
    $out .= '</div>';

    return $out;
});

Form::macro('myImgField', function ($item, $field, $module, $errors = null, $params = array() ){

    $out = '<div id="' . $field . '-container" class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= Form::label( trans( $module . '.' . $field ) );
    $out .= '<div class="row" style="padding-left: 10px; padding-right: 10px">';

        if( $item->$field ){
            $out .= '<img id="'.$field.'-img" src="' . (empty($params['URI']) ? $item->getTable() . '/' . $item->$field : $params[ 'URI' ]) . '" class="form-control" style="height: 150px"  width="100" height="100">';
            $out .= '<button id="btn-img-' . $field  . '-load" type="button" class="btn btn-default img-load col-sm-12" data-target="#'.$field.'" >' . trans( 'global.change' ) . '</button>';
        } else
            $out .= '<button id="btn-img-' . $field  . '-load" type="button" class="btn btn-default img-load col-sm-12" data-target="#'.$field.'" >' . trans( 'global.load' ) . '</button>';

        //$out .= '<button id="btn-img-' . $field  . '-delete" type="button" class="btn btn-default img-delete col-sm-6" data-target="'.$field.'" data-text-deleted="'.trans( 'global.delete' ).'" data-text-cancel="'.trans( 'global.cancel' ).'" >' . trans( 'global.delete' ) . '</button>';

    $out .= '</div>';

    $out .= Form::hidden( 'input-img-' . $field  . '-delete-status', 0, array('id' => 'input-img-' . $field  . '-delete-status' ));
    $out .= Form::file( $field, array( 'id' => $field, 'class' => 'form-control error', 'style' => 'display: none', 'value' => $item->$field ) );

    $out .= '</div>';

    return $out;

});
Form::macro('myImgPreField', function ($item, $field, $module, $errors = null, $params = array() ){

    $out = '<div id="' . $field . '-container" class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= Form::label( trans( $module . '.' . $field ) );
    /*$out .= '<div class="row" style="padding-left: 10px; padding-right: 10px">';

        if( $item->$field ){
            $out .= '<img id="'.$field.'-img" src="' . (empty($params['URI']) ? $item->$field : $params[ 'URI' ].$item->$field) . '" class="form-control" style="height: 150px"  width="100" height="100">';
            $out .= '<button id="btn-img-' . $field  . '-load" type="button" class="btn btn-default img-load col-sm-12" data-target="#'.$field.'" >' . trans( 'global.change' ) . '</button>';
        }

    $out .= '</div>';
   */
    if( $item->$field ){
        $initialPreview=(empty($params['URI']) ? $item->getTable() . '/' . $item->$field : $params[ 'URI' ] . '/' . $item->getTable() . '/' . $item->$field) ;
        if(file_exists(trim($initialPreview,'/')))
            $size=filesize(trim($initialPreview,'/'));
        else
            $size='';
    }
    else{
        $initialPreview='';
        $size='';
    }
    $out .= Form::hidden( 'input-file-' . $field  . '-delete-status', 0, array('id' => 'input-file-' . $field  . '-delete-status' ));
    $out .= Form::file( $field, array( 'id' => $field, 'class' => 'file-input image', 'value' => '','data-show-caption'=>'false', 'data-show-upload'=>'false', 'data-browse-class'=>'btn btn-primary btn-block','data-show-remove'=>'false','data-initial-preview'=>$initialPreview, 'data-caption'=>$item->$field, 'data-size'=>$size ) );

    $out .= '</div>';

    return $out;

});
Form::macro('myFile', function ($item, $field, $module, $errors = null, $params = array() ){

    $out = '<div id="' . $field . '-container" class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= Form::label( trans( $module . '.' . $field ) );

    if (!empty($params['langs']) && $params['langs']==true && sizeof(Config::get('app.available_languages'))>1){
        $out.= '<label class="langlbl btn btn-default">'.strtoupper(Config::get('app.default_web_lang')).'</label>';
        foreach (Config::get('app.available_languages') as $key=>$lang) {
            $fieldLang=$field.'_'.$lang;

            if( $item->$fieldLang ){
                $initialPreview=(empty($params['URI']) ? $item->getTable() . '/' . $item->$fieldLang : $params[ 'URI' ] . '/' . $item->getTable() . '/' . $item->$fieldLang) ;
                $caption=$item->$fieldLang ;  
                $size='1000';     
            }else{
                $initialPreview='';
                $caption=trans('global.select_file');
                $size='';
            }
            $out .= Form::hidden( 'input-file-' . $fieldLang  . '-delete-status', 0, array('id' => 'input-file-' . $fieldLang  . '-delete-status' ));
            $out .= Form::file( $fieldLang, array( 'id' => $fieldLang, 'class' => 'file-input files','data-initial-preview'=>$initialPreview, 'data-caption'=>$caption, 'value' => $item->$fieldLang,"data-show-preview"=>"false", 'data-show-upload'=>'false', 'data-locale' => $lang ) );
        
            $out .= ($errors) ? $errors->first($fieldLang, '<label for="' . $fieldLang . '" data-locale="'. $lang .'" class="error">:message</label><br/>') : '';
        }
    }else{

        if( $item->$field ){
            $initialPreview=(empty($params['URI']) ? $item->getTable() . '/' . $item->$field : $params[ 'URI' ] . '/' . $item->getTable() . '/' . $item->$field) ;
            $caption=$item->$field ;  
            $size='1000';     
        }else{
            $initialPreview='';
            $caption=trans('global.select_file');
            $size='';
        }
        $out .= Form::hidden( 'input-file-' . $field  . '-delete-status', 0, array('id' => 'input-file-' . $field  . '-delete-status' ));
        $out .= Form::file( $field, array( 'id' => $field, 'class' => 'file-input files','data-initial-preview'=>$initialPreview, 'data-caption'=>$caption, 'value' => $item->$field,"data-show-preview"=>"false", 'data-show-upload'=>'false' ) );
        $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label><br/>') : '';
    }

    $out .= '</div>';

    return $out;

});
/**
 * Input select multiple field
 */
Form::macro('mySelectMultipleField', function ($item, $field, $module, $select_values, $errors = null, $params = array())
{
    
    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    $params[ 'multiple' ] = true;
    
    $values = isset($params['values'])
                ? $params['values']
                : ( isset($item->$field) && !is_null($item->$field) && $item->$field !== false ? str_split($item->$field)  : null) ;

    $id =  isset($params['id'])
                ? $params['id']
                : $field ;
    $params['id'] = $id;
    $name = $id . '[]';
    $label =  isset($params['label'])
                ? $params['label']
                : $module . '.' . $field ;

    $out = '';

    if( empty($params['cols']) ){
        $out .= '<div class="col-sm-6">';
        unset( $params[ 'cols' ] );
    } else
        $out .= '<div class="col-sm-' . $params['cols'] . '">';



    $out .= '<label class="' . $required . '" for="' . $id . '">' . trans($label) . '</label>';

    if (! empty($params['icon'])) {
        $out .= '<div class="input-group">';
        $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
    }

    $params[] = $required;
    //$params['class'] = isset($params['class'])? $params['class'] . 'select' : 'select';

    $out .= Form::select($name, $select_values, $values, $params);

    if (! empty($params['icon'])) {
        $out .= '</div>';
    }

    $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label>') : '';
    $out .= '</div>';

    return $out;
});


/**
 * TextArea
 */
Form::macro('myTextArea', function ($item, $field, $module, $errors = null, $params = array())
{

    if( Helper::isRequired( $item, $field ) || Helper::isRequiredLang( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    $out  = '<div class="col-sm-' . (empty($params['cols']) ? '12' : $params['cols']) . '">';
    $out .= Form::label($field, trans($module.'.'.$field), array('class' => 'control-label ' . $required));
    $params['class']=isset($params['class']) && !empty($params['class']) ? ' '.$class=$params['class']:'';

    $params['resize'] = !isset($params['resize']) && empty($params['resize']) ? 'false' : 'true';
    if (!empty($params['langs']) && $params['langs']==true && sizeof(Config::get('app.available_languages'))>1){
        $out.= '<label class="langlbl btn btn-default">'.strtoupper(Config::get('app.default_web_lang')).'</label>';
        foreach (Config::get('app.available_languages') as $key=>$lang) {
            $fieldLang=$field.'_'.$lang;
            $class=$params['class'].($key>0?' hide':'');

            $out .= Form::textarea($fieldLang, $item->$fieldLang , array( 'id' => (empty($params['id']) ? $field : $params['id']), 'class' => 'form-control '.$class.' '.$required, 'rows' => '5', 'resize'=>$params['resize'],'data-locale'=>$lang) );
        
            $out .= ($errors) ? $errors->first($fieldLang, '<label for="' . $fieldLang . '" data-locale="'. $lang .'" class="error">:message</label><br/>') : '';
        }
    }
    else{
        $out .= Form::textarea($field, $item->$field , array( 'id' => (empty($params['id']) ? $field : $params['id']), 'class' => 'form-control '.$params['class'].' '.$required, 'rows' => '5', 'resize'=>$params['resize']) );

        $out .= $errors->first($field, '<label for="$field" class="error">:message</label>');
    }

    $out .= '</div>';

    return $out;

});


/**
 * Input checkbox colum 6
 */
Form::macro('myCheckBox', function ($item, $field, $module, $errors = null, $params = array())
{
    
    
    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    if(!empty($params['required'])){
        $required .= ' ' . $params['required'];
    }

    if( isset( $params[ 'class' ] ) )
        $params[ 'class' ] .= ' styled error';
    else
        $params[ 'class' ] = 'styled error';

    //$params[] = $required;

    if( request()->old( $field ) === '' )
        $params[] = 'checked';

    if( !isset( $params[ 'id' ] ) )
        $params[ 'id' ] = 'checkbox_'.$field;
	
	$value = 0;
	if (!empty($item->$field)) {
		$value = 1;
	}

    $out  = '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= '<label class="checkbox-inline ' . $required . '">';
    $out .=         Form::checkbox('checkbox_'.$field.'', $value, ($value?true:false), $params );
    $out .=         Form::hidden($field, $value, array('id' => $field));
    $out .=         trans( $module . '.' . $field);
    $out .=     '</label>';
    $out .= '</div>';


    
    return $out;
});
/**
 * Input checkbox colum 6
 */
Form::macro('myCheckBoxMultiple', function ($name, $label, $checked)
{

    $out  = '<div class="col-sm-2">';
    $out .= '   <div>'. $label.'</div>';
    $out .= '   <div style="padding-top:14px"> ';   
    $out .=         Form::hidden( $name, $checked?1:0, array('id' => $name ));
    $out .=         Form::checkbox('checkbox_'.$name, 1, $checked);
    $out .= '   </div> ';
    $out .= '</div> ';
    
    return $out;
});
/**
 * Input select chosen field
 */
Form::macro('myAutocompleteChosen', function ($item, $field, $module, $select_values, $errors = null, $params = array())
{

    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    $value = isset($params['value'])
                ? $params['value']
                : (isset($item->$field) ? $item->$field : null) ;
    $id =  isset($params['id'])
                ? $params['id']
                : $field ;
    $params['id'] = $id;
    $label =  isset($params['label'])
                ? $params['label']
                : $module . '.' . $field ;

    $out = '';
    $out .= '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= '<label class="' . $required . '" for="' . $id . '">' . trans($label) . '</label>';

    if (! empty($params['icon'])) {
        $out .= '<div class="input-group">';
        $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
    }

    $params[] = $required;

    if( isset( $params[ 'class' ] ) )
        $params[ 'class' ] .= ' chosen';
    else
        $params[ 'class' ] = 'chosen';

    $out .= Form::select($id, $select_values, $value, $params);

    if (! empty($params['icon'])) {
        $out .= '</div>';
    }

    $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label>') : '';
    $out .= '</div>';

    return $out;
});

Form::macro('myPhoneField', function ( $item, $extension, $numberField, $module, $errors = null, $params = array())
{
    
    if( Helper::isRequired( $item, $numberField ) )
        $required = 'required';
    else
        $required = '';

    $out = '';
    $out .= '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '"';

    if(!empty($params['style']))
        $out .= 'style="'. (empty($params['style']) ? '' : $params['style']) . '"';

    $out .= '>';

    $out .= '<label class="' . $required . '" for="' . $numberField . '">' . trans($module . '.' . $numberField) . '</label>';

    $old = request()->old($extension);

    if (! empty($params['icon'])) {
        $out .= '<div class="input-group">';

        if(!empty($params['withExtension'])) {
            $out .= '<input
                        type="text"
                        value="' . (empty($old) ? (empty($item->{$extension})?'':$item->{$extension}) : $old ) . '"
                        id="'.(empty($params['id']) ? $extension : $params['id']).'"
                        name="'.$extension.'"
                        class="form-control error"
                        placeholder="'.trans($module . '.' . $extension).'"
                        style="margin-right: -130px;"
                    >';
        }

        $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
    }

    $old = request()->old($numberField);

    $placeholder = (trans($module . '.' . $numberField . '.placeholder') == $module . '.' . $numberField . '.placeholder')?trans($module . '.' . $numberField) : trans($module . '.' . $numberField . '.placeholder');
    $out .= '<input ' . $required . ' ';

    // type
    $out .= 'type="'. (empty($params['type']) ? 'text' : $params['type'] ) .'" ';
    $out .= 'value="' . (empty($old) ? (empty($item->{$numberField})?'':$item->{$numberField}) : $old ) . '" id="' . $numberField . '" name="' . $numberField . '" placeholder="' . $placeholder . '"';

    // class
    $out .= ' class="' . (empty($params['class']) ? 'form-control error' : $params['class']) . '"';
    // maxlength
    $out .= ! empty($params['maxlength']) ? ' maxlength="' . $params['maxlength'] . '" ' : '';
    $out .= ! empty($params['attributes']) ? $params['attributes'] : '';
    $out .= '>';

    if (! empty($params['icon'])) {
        $out .= '</div>';
    }

    $out .= ($errors) ? $errors->first($numberField, '<label for="' . $numberField . '" class="error">:message</label>') : '';
    $out .= '</div>';

    return $out;
});

/**
 * Input select client field that allows create a new client
 */
Form::macro('myClientSelectField', function ($item, $field, $module, $select_values, $errors = null, $params = array())
{

    if( Helper::isRequired( $item, $field ) )
        $required = 'required';
    else
        $required = '';

    $value = isset($params['value'])
                ? $params['value']
                : (isset($item->$field) ? $item->$field : null) ;
    $id =  isset($params['id'])
                ? $params['id']
                : $field ;
    $params['id'] = $id;
    $label =  isset($params['label'])
                ? $params['label']
                : $module . '.' . $field ;

    $out = '';
    $out .= '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= '<div class="select-client-div">';
    $out .= '<label class="' . $required . '" for="' . $id . '">' . trans($label) . '</label>';

    $actionButton = route('customers.createAjax');
    $textButton = '<span class="fa fa-plus"></span>';
    $paramsButton = array('target' => '#formModal', 'action' => $actionButton, 'text' => $textButton);

    $out .= Html::myNewClientButton($paramsButton);

    $out .= '</div>';

    if (! empty($params['icon'])) {
        $out .= '<div class="input-group">';
        $out .= '<span class="input-group-addon"><i class="' . $params['icon'] . '"></i> </span>';
    }

    $params[] = $required;

    $out .= Form::select($id, $select_values, $value, $params);

    if (! empty($params['icon'])) {
        $out .= '</div>';
    }

    $out .= ($errors) ? $errors->first($field, '<label for="' . $field . '" class="error">:message</label>') : '';
    $out .= '</div>';

    return $out;
});

/**
 * Input select client field that allows create a new client
 */
Html::macro('myNewClientButton', function ($params=false)
{
    $out = '<button class="btn btn-default btn-gradient new-client-button addNewClientAjax pull-right" type="button" ';

    if(!empty($params['target'])){
        $out .= 'data-target="' . $params['target'] . '" data-toggle="modal" ';
    }

    $out .= 'module="customers" ';

    if(!empty($params['action'])){
        $out .= 'action = "' . $params['action'] . '" ';
    }

    $out .= '>' . $params['text'];
    $out .= '</button>';

    return $out;
});

/**
 * Input checkbox colum 6
 */
Form::macro('myLabelField', function ($item, $field, $module, $errors = null, $params = array())
{
    $out =  '<div>';

    if(empty($params['hideLabel'])) {
        $out .= '   <div class="col-sm-' . (empty($params['cols']) ? '1' : $params['cols']) . '">';
        $out .= '       <label for="' . $field . '">' . trans($module . '.' . $field) . '</label>';
        $out .= '   </div>';
    }
    
    $out .= '   <div class="col-sm-' . (empty($params['cols']) ? '5' : $params['cols']) . '">';
    $out .= '       <label style="font-weight:bold">'.$item->{$field}.'</label>';
    $out .= '   </div>';
    $out .= '</div>';

    return $out;
});

Form::macro('myButtonGroup', function ($item, $field, $module, $errors = null, $params = array())
{   
    $out = '<div class="col-sm-' . (empty($params['cols']) ? '4' : $params['cols']) . '">';
    $out .= '<fieldset class="content-group">';
    $out .= '<legend class="text-bold">'. trans($module . '.' . $field) .'</legend>';
    $out .= Form::mySelectField($item, $field . '_type', $module, array(
        0=>'Ninguno',
        1=>'Enlace nueva pestaña', 
        2=>'Enlace misma pestaña', 
        3=>'Scroll misma URL', 
        4=>'Scroll otra URL', 
        5=>'Descarga fichero', 
        6=>'Imagen PopUp'
    ), $errors, array('class' => 'form-control error', 'cols' => 12 ) );
    $out .= Form::myTextField12($item, $field . '_text', $module, $errors, array('langs'=>true  ));
    $out .= Form::myTextField12($item, $field . '_url', $module, $errors, array('langs'=>true  ));
    $out .= Form::myFile($item, $field . '_file', $module, $errors, array( 'cols' => 12, 'URI' => $item->getURI() ) );
    $out .= Form::myImgPreField($item, $field . '_image', $module, $errors, array( 'cols' => 12, 'URI' => $item->getURI() ) );
    $out .= '</fieldset>';
    $out .= '</div>';

    return $out;
});

Form::macro('myTextGroup', function ($item, $field, $module, $errors = null, $params = array())
{   
    $out = '<div class="col-sm-' . (empty($params['cols']) ? '4' : $params['cols']) . '">';
    $out .= '<fieldset class="content-group">';
    $out .= '<legend class="text-bold">'. trans($module . '.' . $field) .'</legend>';
    $out .= Form::mySelectField($item, $field . '_vertical', $module, array(1=>'Abajo',2=>'Arriba'), $errors, array('class' => 'form-control error', 'cols' => 12 ) );
    $out .= Form::mySelectField($item, $field . '_horizontal', $module, array(1=>'Izquierda',2=>'Derecha',3=>'Centro'), $errors, array('class' => 'form-control error', 'cols' => 12 ) );
    $out .= Form::mySelectField($item, $field . '_alienation', $module, array(1=>'Izquierda',2=>'Derecha',3=>'Centro',4=>'Justificado'), $errors, array('class' => 'form-control error', 'cols' => 12 ) );
    $out .= '</fieldset>';
    $out .= '</div>';

    return $out;
});

Form::macro('myImgGroup', function ($item, $field, $module, $index, $errors = null, $params = array())
{   
    if ($item->typeGroupsImages) {
        $group = $item->typeGroupsImages[$index];
        $group = 'myImgGroup' . $group;
    
        $out = Form::$group($item, $field, $module, $errors = null, $params = array());
    }else {
        $out = '';
    }

    return $out;

});

Form::macro('myImgGroup1', function ($item, $field, $module, $errors = null, $params = array())
{   
    $out = '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= '<fieldset class="content-group">';
    $out .= '<legend class="text-bold">'. trans($module . '.' . $field) .'</legend>';
    $out .= Form::myImgPreField($item, $field, $module, $errors, array( 'cols' => 12, 'URI' => $item->getURI() ) );
    $out .= Form::myTextField12($item, $field . '_link', $module, $errors, array('langs'=>true  ));
    $out .= Form::myCheckBox($item, $field . '_in_new_tab', $module, $errors, array( 'cols' => 12 ) );
    $out .= '</fieldset>';
    $out .= '</div>';

    return $out;
});

Form::macro('myImgGroup2', function ($item, $field, $module, $errors = null, $params = array())
{   
    $out = '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= '<fieldset class="content-group">';
    $out .= '<legend class="text-bold">'. trans($module . '.' . $field) .'</legend>';
    $out .= Form::myImgPreField($item, $field, $module, $errors, array( 'cols' => 12, 'URI' => $item->getURI() ) );
    $out .= Form::myTextField12($item, $field . '_name', $module, $errors, array('langs'=>true  ));
    $out .= Form::myTextField12($item, $field . '_title', $module, $errors, array('langs'=>true  ));
    $out .= Form::myFile($item, $field . '_long_desc', $module, $errors, array( 'langs'=>true) );
    $out .= Form::myTextField12($item, $field . '_alt', $module, $errors, array('langs'=>true  ));
    $out .= Form::myTextField12($item, $field . '_link', $module, $errors, array('langs'=>true  ));
    $out .= Form::myCheckBox($item, $field . '_in_new_tab', $module, $errors, array( 'cols' => '12' ) );
    $out .= Form::mySelectField($item, $field . '_vertical', $module, array(1=>'Abajo',2=>'Arriba'), $errors, array('class' => 'form-control error', 'cols' => 12 ) );
    $out .= Form::mySelectField($item, $field . '_horizontal', $module, array(1=>'Izquierda',2=>'Derecha',3=>'Centro'), $errors, array('class' => 'form-control error', 'cols' => 12 ) );
    $out .= '</fieldset>';
    $out .= '</div>';

    return $out;
});

Form::macro('myImgGroupGeneral', function ($item, $field, $module, $errors = null, $params = array())
{   
    $out = '<div class="col-sm-' . (empty($params['cols']) ? '6' : $params['cols']) . '">';
    $out .= '<fieldset class="content-group">';
    $out .= '<legend class="text-bold">'. trans($module . '.' . $field) .'</legend>';
    $out .= Form::myImgPreField($item, $field, $module, $errors, array( 'cols' => 12, 'URI' => $item->getURI() ) );
    $out .= Form::myTextField12($item, $field . '_title', $module, $errors, array('langs'=>true  ));
    $out .= Form::myFile($item, $field . '_long_desc', $module, $errors, array( 'langs'=>true) );
    $out .= Form::myTextField12($item, $field . '_long_desc_rewrite', $module, $errors, array('langs' => true  ));
    $out .= Form::myTextField12($item, $field . '_alt', $module, $errors, array('langs'=>true  ));
    $out .= Form::myTextField12($item, $field . '_title', $module, $errors, array('langs'=>true  ));
    $out .= '</fieldset>';
    $out .= '</div>';

    return $out;
});

/**
 * Panel title
 */
Html::macro('myTitlePanelAccordion', function ($item, $field, $element, $index, $params = array())
{
    $out = '<div class="panel-heading" data-toggle="collapse" data-parent="#accordion-control-right" href="#item_panel_' . $index . '">';

    if (!empty($params['langs']) && $params['langs']==true && sizeof(Config::get('app.available_languages'))>1){
        foreach(Config::get('app.available_languages') as $key => $lang){
            $fieldLang = $field . '_' . $lang;
            $out .= '<div class="input-group haslang" data-locale="'. $lang .'" '.($key!=0?'style="display:none"':'').' >';
            $out .= '<' . $element . ' class="panel-title" id="title_' . $index . '_' . $lang . '" >' . $item->$fieldLang . '</' . $element . '>';
            $out .= '</div>';
        }
        
    }else{
        $out .= '<' . $element . ' class="panel-title" id="title_' . $index . '" >' . $item->$field . '</' . $element . '>';
    }

    $out .= '</div>';

    return $out;
});

