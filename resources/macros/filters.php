<?php

use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Models\Countries;
use Stringy\StaticStringy as Stringy;
use Illuminate\Http\Request;

/**
 * Custom macros
*/

Html::macro('myFilters', function($module)
{   
    $camelizeModule = Stringy::upperCamelize($module);  

    $out = '';
    $class = '\\App\\Models\\' . $camelizeModule;
    if (!class_exists($class)){
        $out .= 'Class ' . $class . ' doesn\'t exists!';
    } else {

        $model = new $class();
        
        $filters = $model->filters;
        if ($filters){
            foreach ($filters as $key => $value) {
                if(!isset($value['field']))continue;
                $type = !array_key_exists('type', $value)? 'text' : $value['type'];
                switch($type) {
                    case 'text':
                        $operators = [
                            'equal' => [
                                'title' => 'Igual a',
                                'sign' => '='
                            ], 
                            'nequal' => [
                                'title' => 'Desigual a',
                                'sign' => '≠'
                            ], 
                            'like' => [
                                'title' => 'Que contenga',
                                'sign' => '≃'
                            ]
                        ];
                        $out .= Form::textFilter($module, $key, $operators);
                        break;
                    case 'number':
                        $operators = [
                            'equal' => [
                                'title' => 'Igual a',
                                'sign' => '='
                            ], 
                            'nequal' => [
                                'title' => 'Desigual a',
                                'sign' => '≠'
                            ],  
                            'high' => [
                                'title' => 'Mayor que',
                                'sign' => '>'
                            ],
                            'less' => [
                                'title' => 'Menor que',
                                'sign' => '<'
                            ],
                            'eqhigh' => [
                                'title' => 'Mayor o igual que',
                                'sign' => '≥'
                            ],
                            'eqless' => [
                                'title' => 'Menor o igual que',
                                'sign' => '≤'
                            ],
                            'between' => [
                                'title' => 'Entre',
                                'sign' => '≶'
                            ]
                        ];
                        $out .= Form::numberFilter($module, $key, $operators);
                        break;
                    case 'bool':
                        $operators = [
                            'equal' => [
                                'title' => 'Igual a',
                                'sign' => '='
                            ], 
                            'nequal' => [
                                'title' => 'Desigual a',
                                'sign' => '≠'
                            ]
                        ];
                        $out .= Form::boolFilter($module, $key, $operators);
                        break;
                    case 'date':
                        $operators = [
                            'equal' => [
                                'title' => 'Igual a',
                                'sign' => '='
                            ], 
                            'nequal' => [
                                'title' => 'Desigual a',
                                'sign' => '≠'
                            ], 
                            'high' => [
                                'title' => 'Mayor que',
                                'sign' => '>'
                            ],
                            'less' => [
                                'title' => 'Menor que',
                                'sign' => '<'
                            ],
                            'eqhigh' => [
                                'title' => 'Mayor o igual que',
                                'sign' => '≥'
                            ],
                            'eqless' => [
                                'title' => 'Menor o igual que',
                                'sign' => '≤'
                            ],
                            'between' => [
                                'title' => 'Entre',
                                'sign' => '≶'
                            ]
                        ];
                        $out .= Form::dateFilter($module, $key, $operators);
                        break;
                    case 'multiple':
                        if (array_key_exists('table', $filters[$key])){
                            $table = $filters[$key]['table'];
                        }elseif(array_key_exists('nm', $filters[$key])) {
                            $table = $filters[$key]['nm']['otherTable'];
                        }
                        $displayFields = $filters[$key]['display_fields'];
                        $modelName='\\App\\Models\\'.Stringy::upperCamelize($table);
                        $model = new $modelName();
                        $queryBuilder = DB::table($table . ' AS t');

                        if($model->fieldsLangs){
                            $select = implode(",", $displayFields) . ',' . $table . '_id';
                            $mainLang = App\Models\Language::getMainLanguage();
                            $queryBuilder->leftJoin($table.'_langs AS tl', 't.id', '=',  'tl.'.$table.'_id')
                            ->where('tl.language_id', '=', $mainLang->id);
                        }else{
                            $select = implode(",", $displayFields) . ", id";
                        }
                        $queryBuilder->selectRaw($select);

                        if(array_key_exists('filters', $filters[$key])){
                            $queryBuilder->where($filters[$key]['filters']);
                        }

                        $queryBuilder = $queryBuilder
                        ->whereNull('t.deleted_at')
                        ->get();

                        $select_values = [];
                        foreach($queryBuilder as $item) {
                            $values= [];
                            $k = null;
                            foreach($item as $field => $fieldValue) {
                                if(in_array($field, $displayFields) && $fieldValue) {
                                    $values[] = $fieldValue;
                                }else{
                                    $k = $fieldValue;
                                }
                            }
                            if($values){
                                $values = implode(" | ", $values);
                                $select_values[$k] = $values;
                            }
                        }
                        
                        $out .= Form::multipleFilter($module, $key, $select_values);
                        break;
                }
            }
        }
    }

    return $out;
});

Form::macro('textFilter', function($module, $field, $operators) {
    $out = '<div id="filter_'. $field .'" class="form-group col-sm-2">';
    $out .= Form::label($field, trans($module . '.' . $field));
        
    $old = request()->old($field) || request()->old($field) != 'null'? request()->old($field) : '';
    $oldOperatorFsearch = request()->old($field . '_operator')?: 'like';
    $oldOperator = $operators[$oldOperatorFsearch];

    $out .= '<div class="input-group">';
    $out .= Form::text(
        $field, 
        $old, 
        [
            'class' => 'form-control customSearchFilter gridSearchFilter searchWithOperator', 
            'aria-controls' => $module.'Grid', 
            'rel' => $module.'Grid',
            'data-field' => $field
        ]
    );
    $out .= Form::hidden($field . '_operator', $oldOperatorFsearch);

    $out .= '<div class="input-group-btn">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="opSign" fsearch="'. $oldOperatorFsearch .'" title="'. $oldOperator['title'] .'">'. $oldOperator['sign'] .'</span></button>
    <ul class="dropdown-menu dropdown-menu-right operators-list" data-field="'. $field .'" style="min-width: 30px">';
    foreach($operators as $key => $operator) {
        $out .= '<li fsearch="'. $key .'" title="'. $operator['title'] .'"><a>' . $operator['sign'] . '</a></li>';
    }
    $out .= '</ul></div>';
    $out .= '</div></div>';

    return $out;
});

Form::macro('numberFilter', function($module, $field, $operators, $params = array()) {
    $out = '<div id="filter_'. $field .'" class="form-group col-sm-2">';
    $out .= Form::label($field, trans($module . '.' . $field));

    $old = request()->old($field);
    $oldOperatorFsearch = request()->old($field . '_operator')?: 'equal';
    $oldOperator = $operators[$oldOperatorFsearch];

    $out .= '<div class="input-group">';

    $out .= '<input';
    // type
    $out .= ' type="number"';
    $out .= ' value="' . (!is_numeric($old) ? '' : $old ) . '" id="' . $field . '" name="' . $field . '"';

    //Attributes
    $out .= ' min="'. (empty($params['min']) ? '0' : $params['min'] ) .'"';
    $out .= ' max="'. (empty($params['max']) ? '10000' : $params['max'] ) .'"';
    $out .= ' step="'. (empty($params['step']) ? '0.01' : $params['step'] ) .'"';

    // class
    $out .= ' class="form-control customSearchFilter gridSearchFilter searchWithOperator" aria-controls="'. $module . 'Grid" rel="'. $module .'Grid"';
    $out .= 'data-field="'. $field .'">';
    $out .= Form::hidden($field . '_operator', $oldOperatorFsearch);

    $out .= '<div class="input-group-btn">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="opSign" fsearch="'. $oldOperatorFsearch .'" title="'. $oldOperator['title'] .'">'. $oldOperator['sign'] .'</span></button>
    <ul class="dropdown-menu dropdown-menu-right operators-list" data-field="'. $field .'" style="min-width: 30px">';
    foreach($operators as $key => $operator) {
        $out .= '<li fsearch="'. $key .'" title="'. $operator['title'] .'"><a>' . $operator['sign'] . '</a></li>';
    }
    $out .= '</ul></div>';
    $out .= '</div></div>';

    return $out;
});

Form::macro('boolFilter', function($module, $field, $operators) {
    $out = '<div id="filter_'. $field .'" class="form-group col-sm-2">';
    $out .= Form::label($field, trans($module . '.' . $field));
    $out .= Form::select(
        $field, 
        [' ' => '--', '1' => 'Sí', '0' => 'No'], 
        null, 
        [
            'class' => 'form-control customSearchFilter gridSearchFilter searchWithOperator', 
            'aria-controls' => $module.'Grid', 
            'rel' => $module.'Grid',
            'data-field' => $field
        ]
    );
    $out .= '</div>';

    return $out;
});

Form::macro('dateFilter', function($module, $field, $operators) {
    $out = '<div id="filter_'. $field .'" class="form-group col-sm-3">';
    $out .= Form::label($field, trans($module . '.' . $field));

    $oldOperatorFsearch = request()->old($field . '_operator')?: 'equal';
    $oldOperator = $operators[$oldOperatorFsearch];

    $out .= '<div class="input-group">';

    $out .= Form::date(
        $field, 
        '', 
        [
            'class' => 'form-control pickadate customSearchFilter gridSearchFilter searchWithOperator', 
            'aria-controls' => $module.'Grid', 
            'rel' => $module.'Grid',
            'data-field' => $field
        ]
    );
    $out .= Form::hidden($field . '_operator', $oldOperatorFsearch);
    
    $out .= '<div class="input-group-btn">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="opSign" fsearch="'. $oldOperatorFsearch .'" title="'. $oldOperator['title'] .'">'. $oldOperator['sign'] .'</span></button>
    <ul class="dropdown-menu dropdown-menu-right operators-list" data-field="'. $field .'" style="min-width: 30px">';
    foreach($operators as $key => $operator) {
        $out .= "<li fsearch='". $key ."' title='". $operator['title'] ."'><a>" . ($operator['sign']) . "</a></li>";
    }
    $out .= '</ul></div>';

    $out .= Form::date(
        $field . '_max', 
        '', 
        [
            'class' => 'form-control pickadate customSearchFilter gridSearchFilter searchWithOperator', 
            'aria-controls' => $module.'Grid', 
            'rel' => $module.'Grid',
            'data-field' => $field,
            'style' => 'display: none',
            'id' => $field . '_max'
        ]
    );

    $out .= '</div></div>';

    return $out;
});

Form::macro('multipleFilter', function($module, $field, $select_values) {
    $out = '<div id="filter_'. $field .'" class="form-group col-sm-4">';
    $out .= Form::label($field, trans($module . '.' . $field));
    $out .= Form::select(
        $field, 
        $select_values, 
        null, 
        [
            'class' => 'form-control customSearchFilter gridSearchFilter select', 
            'multiple' => true, 
            'aria-controls' => $module.'Grid', 
            'rel' => $module.'Grid'
        ]
    );
    $out .= '</div>';

    return $out;
});