$(document).ready(function () {

    $('.img-load').click(function () {
        $($(this).data('target')).trigger('click');
    });

    $('.img-delete').click(function () {

        var idFile = $(this).data('target');
        var deleteStatus = '#input-img-' + idFile + '-delete-status';
        var idImg = '#' + idFile + '-img';
        var btnDelete = '#btn-img-' + idFile + '-delete';

        $('#' + idFile).val('');

        if ($(deleteStatus).val() == 0) {

            $(deleteStatus).val('1');
            $(idImg).css('opacity', '0.5');
            $(btnDelete).text($(btnDelete).data('text-cancel'));

        } else {

            $(deleteStatus).val('0');
            $(idImg).css('opacity', '1');
            $(btnDelete).text($(btnDelete).data('text-deleted'));
        }
    });

    $('.fileinput-remove-button, .fileinput-remove').click(function (event) {
        let id = $(event.result).attr('id');
        //let index = id.substring(id.indexOf('_') + 1, id.length);
        $('#input-file-' + id + '-delete-status').val('1');
    });

});