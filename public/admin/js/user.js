$(document).ready(function() {
    $('#btnSendUserCreatedEmail').click(function(e) {
        e.preventDefault();
        sendUserCreatedEmail();
        return false;
    });
});

function sendUserCreatedEmail() {
    var module = js_module;
    var id = $('input[name=id]').val();
    var ajaxUrl =  BASE_URL + '/' + module + '/' + id + '/sendEmailConfirmation';

    $.ajax({
        type: 'POST',
        url: ajaxUrl,
        contentType: 'application/json'
    }).done(function (response) {
        if(response.success) {
            showMessage('Éxito', 'Email enviado correctamente', 5000);
        }else{
            showMessage('Error', response.message, 0);
        }
    });
}