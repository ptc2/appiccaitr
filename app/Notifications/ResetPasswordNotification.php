<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class ResetPasswordNotification extends Notification
{
    use Queueable;

        /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {   
        if(get_class($notifiable) == 'App\Models\User'){
            $url = url(route('user.password.reset', $this->token, false));
        }else{
            $url = url(route('password.reset', $this->token, false));
            Auth::logout();
        }


        $userName = $notifiable->name . ' ' . $notifiable->surname;
        return (new MailMessage)
        ->greeting("¡Hola $userName!")
        ->subject('Cambio Contraseña - ' . env('APP_NAME'))
        ->line('Estás recibiendo este email porque hemos recibido una petición para restablecer la contraseña de tu cuenta.')
        ->action('Cambiar Contraseña', $url)
        ->line('Si no solicitó restablecer la contraseña, no se requieren más acciones.')
        ->salutation('Saludos,');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
