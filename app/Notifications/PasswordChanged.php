<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordChanged extends Notification
{
    use Queueable;

    /**
     * The new password.
     *
     * @var string
     */
    public $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $greeting = "¡Hola";

        if($notifiable->userData) {
            $userData = $notifiable->userData;
            $greeting .= " $userData->name $userData->surname";
        }

        $greeting .= "!";

        return (new MailMessage)
            ->greeting($greeting)
            ->subject('Tu contraseña ha cambiado - ' . env('APP_NAME'))
            ->line('La contraseña de acceso de la aplicación ha sido cambiada por el administrador del sistema.')
            ->line('Tu nueva contraseña es: ' . $this->password)
            ->salutation('Saludos,');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
