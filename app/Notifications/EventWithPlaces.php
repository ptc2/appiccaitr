<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EventWithPlaces extends Notification
{
    use Queueable;

    protected $reserveCount;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reserveCount)
    {
        $this->reserveCount = $reserveCount;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = route('event.edit', ['id' => $notifiable->id]);

        return (new MailMessage)
            ->subject('Evento ' . $notifiable->title . ' con Plazas Libres - ' . env('APP_NAME'))
            ->line('El evento ' . $notifiable->title . ' se ha quedado con plazas libres y actualmente tiene a ' . $this->reserveCount . ' empresa/s en reserva.')
            ->line('Puedes entrar en el panel y gestionar las reservar:')
            ->action('Ver Evento', $url)
            ->salutation('Saludos,');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
