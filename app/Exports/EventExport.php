<?php

namespace App\Exports;

use App\Models\Event;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EventExport extends BaseExport
{
    public function __construct(Event $model, string $module, array $data)
    {
        parent::__construct($model, $module, $data);
    }
}
