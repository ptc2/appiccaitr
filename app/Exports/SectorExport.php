<?php

namespace App\Exports;

use App\Models\Sector;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SectorExport extends BaseExport
{
    public function __construct(Sector $model, string $module, array $data)
    {
        parent::__construct($model, $module, $data);
    }
}
