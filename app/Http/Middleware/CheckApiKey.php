<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $apiKey = $request->header('Api-Key') ?: $request->input('api-key');

        if (!$apiKey && ($apiKey != env('API_KEY'))) {
            $error = trans('auth.no_api_key');
            return response([
                'message' => $error
            ], 401);
        }

        return $next($request);
    }
}
