<?php

namespace App\Http\Controllers\Cms;

use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Http\Controllers\Controller;
use App\Helpers\Application;

use App\Models\Timeline;

class SessionController extends Controller
{
    public function __construct()
    {
        Application::setCMS();
    }
    public function create()
    {
        $this->log(__FUNCTION__);

        // Check retries to avoid brute force attacks
        $data = $this->lockAttemps();
        $data = 0;

        if (\Auth::check()) {
            return Redirect::to('products_categories');
        } else {
            return \View::make('sessions.create', array(
                'body_classes' => 'login-page',
                'lock'         => $data['lock'],
                'nextAttemp'   => $data['nextAttemp'],
                'time_to_wait' => $data['time_to_wait'],
                'body_classes' => 'login-container bg-white'
            ));
        }
    }

    public function store(Request $request)
    {
        // todo: lockAttempts
        $remember = ($request->has('remember')) ? true : false;
        $attempt_data = array(
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'active' => 1
        );
        $username = $request->get('username');

        if (Auth::attempt($attempt_data, $remember)) {
            try {
                //Acl::resetAcl();
                //Acl::start();
                $timeline_data = array(
                    'action' => 'session.login',
                    'actor_ip' => request()->ip()
                );
                $model = new Timeline();
                $model->fill($timeline_data)->save();
            } catch (Exception $e) {
                $this->log($e->getMessage());
            }

            return redirect()->route('dashboard')->with('flash_message', trans('session.flash_logged'))->withInput();
        } else {
            try {
                $timeline_data = array(
                    'action' => 'session.failed_login',
                    'actor_ip' => request()->ip()
                );
                $model = new Timeline();
                $model->fill($timeline_data)->save();
            } catch (Exception $e) {
                $this->log($e->getMessage());
            }
            $model = new Timeline();
            $attemps = $model->getFailedLoginAttemps();

            $attemps = Session::get('login.attemps', 0);
            $attemps++;
            Session::put('login.attemps', $attemps);
            Session::put('login.last_attemp', Carbon::now());
            Session::put('flash_message_classes', 'alert bg-danger alert-styled-left');

            return Redirect::back()->with('flash_message', trans('session.flash_invalid'))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy($id = null)
    {
        $this->log(__FUNCTION__);
        try {
            $timeline_data = array(
                'action' => 'session.logout',
                'actor_ip' => request()->ip()
            );
            if (Auth::check()) {
                $user = Auth::getUser();
                $timeline_data['object_type'] = get_class($user);
                $timeline_data['object_id'] = $user->id;
            }
            $model = new Timeline();
            $model->fill($timeline_data)->save();
        } catch (Exception $e) {
            $this->log($e->getMessage());
        }
        Auth::logout();

        return Redirect::to('/cms')->with('flash_message', trans('session.flash_logout'));
    }

    /**
     * Auxiliary function to avoid the brute-force attack
     *
     * @return multitype:boolean \Carbon\Carbon
     */
    private function lockAttemps()
    {
        $result = array(
            'lock' => false,
            'nextAttemp' => Carbon::now(),
            'time_to_wait' => 0
        );

        $threshold_seconds = 30;
        $attemps_in_a_row = 3;

        // Gets the last attemps from timeline  
        $model = new Timeline();
        $data = $model->getFailedLoginAttemps(ip2long(request()->ip()));

        $attemps = $data['attemps'];
        $lastAttemp = new \Carbon\Carbon($data['lastAttemp']);
        $time_to_wait = 0;
        $nextAttemp = Carbon::now();
        $lock = false;

        if ($data['attemps'] > 0 && $data['attemps'] % $attemps_in_a_row == 0) {
            $nextAttemp = $lastAttemp->copy()->addSeconds(pow(2, ceil($data['attemps'] / $attemps_in_a_row) - 1) * $threshold_seconds);
            $result['lock'] = $nextAttemp->gte(Carbon::now());
            $result['nextAttemp'] = $result['lock'] ? $nextAttemp : Carbon::now();
            $result['time_to_wait'] = $result['lock'] ? Carbon::now()->diffInSeconds($nextAttemp, false) : 0;
        }

        return $result;
    }
}
