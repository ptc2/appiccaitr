<?php

namespace App\Http\Controllers\Cms;

use Auth;
use Carbon\Carbon;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;

use App\Presenters\StatisticPresenter;

use App\Models\Statistic as StatisticModel;
use App\Models\User;
use App\Models\UserCms;
use App\Models\Question;
use App\Models\SocioData;
use App\Models\Kinship;
use App\Models\Studies;
use App\Models\Survey;

use Statistic;

use PDF;
use Stringy\StaticStringy as Stringy;

class StatisticController extends BaseController
{

    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
    public function __construct(StatisticModel $model, StatisticPresenter $presenter = null) {
        parent::__construct($model, $presenter);

        $this->module = 'statistic';
        $this->urlBack = array(
            'store' => '',
            'update' => ''
        );
    }
    public function edit($id = NULL)
    {
        $this->setDataToSahre($id);
        return parent::edit($id);
    }
	private function setDataToSahre($id = NULL)
    {		
		$user = User::find($id);
		$userRole = Auth::user()->user_cms_profile_id;

		$results = Statistic::getStatisticForAGivenUser($id);
		$surveys = (IntVal($userRole) == 1) ? StatisticModel::select('survey_id', 'socio_data_id')->where('id', $id)->withTrashed()->get()->toArray() : StatisticModel::select('survey_id', 'socio_data_id')->where('id', $id)->get()->toArray();
		$totalSurveys = count($surveys);
		$totalMembers = $user->userData->members;
		$percentageSurveyAnswered = (float) number_format((($totalSurveys*100) / $totalMembers), 2);
		
		$info = Statistic::getTotalsAndPercentagesInfo($surveys, $totalMembers);
		$totals = $info['totals'];
		$percentages = $info['percentages'];
		
        View::share([
			'results' => $results,
			'percentageSurveyAnswered' => $percentageSurveyAnswered,
            'totalSurveys' => $totalSurveys,
			'totals' => $totals,
			'percentages' => $percentages,
			'members' => $totalMembers,
			'user_name' => $user->userData->name
        ]);
    }
    // download pdf
    public function pdfStatisticDownload($id)
    {
        $title = trans($this->module . '.mtitle');
        $user = User::find($id);
        $user_name = $user->userData->name;
		$members = $user->userData->members;
		$userRole = Auth::user()->user_cms_profile_id;
		
		$surveys = (IntVal($userRole) == 1) ? StatisticModel::where('id', $id)->withTrashed()->count() : StatisticModel::where('id', $id)->count();
		$percentageSurveyAnswered = (float) number_format((($surveys*100) / $members), 2);
		
        $data = Statistic::getStatisticForAGivenUser($id);
		
		if($surveys > 0) {
			$pdf = PDF::loadView('pdf.statistic', compact('title', 'user_name', 'members', 'percentageSurveyAnswered', 'data'))->setPaper('a4', 'landscape');
			return $pdf->download('cms-' . trans($this->module . '.pdftitle') . '.pdf');			
		}
    }
	// download pdf
    public function pdfEstablishmentsStatisticDownload($id)
    {
		$days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
		$months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        
		$title = trans($this->module . '.mtitle');
        $user = User::find($id);
        $user_name = $user->userData->name;
		$members = $user->userData->members;
		$userRole = Auth::user()->user_cms_profile_id;
		
        $data = Statistic::getStatisticForAGivenUser($id);
		
		$minDate = (IntVal($userRole) == 1) ? StatisticModel::where('id', $id)->withTrashed()->min('created_at') : StatisticModel::where('id', $id)->min('created_at');
		$maxDate = (IntVal($userRole) == 1) ? StatisticModel::where('id', $id)->withTrashed()->max('created_at') : StatisticModel::where('id', $id)->max('created_at');
		$minDay = $days[date('w', strtotime($minDate))]." ".date('d', strtotime($minDate))." de ".$months[date('n', strtotime($minDate))-1]. " del ".date('Y', strtotime($minDate));
		$maxDay = $days[date('w', strtotime($maxDate))]." ".date('d', strtotime($maxDate))." de ".$months[date('n', strtotime($maxDate))-1]. " del ".date('Y', strtotime($maxDate));
		
		$surveys = (IntVal($userRole) == 1) ? StatisticModel::select('survey_id', 'socio_data_id')->where('id', $id)->withTrashed()->get()->toArray() : StatisticModel::select('survey_id', 'socio_data_id')->where('id', $id)->get()->toArray();
		
		$info = Statistic::getTotalsAndPercentagesInfo($surveys, $members);
		$totals = $info['totals'];
		$percentages = $info['percentages'];
		
		if(count($surveys) > 0) {
			$pdf = PDF::loadView('pdf.establishments_statistic', compact('title', 'user_name', 'members', 'totals', 'percentages', 'data', 'minDay', 'maxDay'))->setPaper('a4', 'landscape');
			return $pdf->download('cms-' . trans($this->module . '.pdfcentertitle') . '.pdf');
		}
    }
	public function refreshStatisticTable($id)
	{
		$user = User::find($id);
		$members = $user->userData->members;
		$userRole = Auth::user()->user_cms_profile_id;
		
		$request = request()->all();
		$createdAt = $request['created_at'];
		$createdAtMax = $request['created_at_max'];
		$operator = $request['operator'];
		
		$results = [];
		$surveys = [];
		
		if(!is_null($id)) {
            $query = StatisticModel::where('id', $id);
			
			if(!is_null($createdAt) || !is_null($createdAtMax)) {
				$field = 'created_at';
				$isBetween = false;
				$isNotBetween = false;
				
				$value = !is_null($createdAt) ? Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($createdAt)))->format('Y-m-d 00:00:00') : Carbon::mow()->format('Y-m-d 00:00:00');
                $valueMax = !is_null($createdAtMax) ? Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($createdAtMax)))->format('Y-m-d 23:59:59') : '';
				
				if(empty($valueMax)) {
					$valueMax = Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($value)))->format('Y-m-d 23:59:59');
				}
				
				if(!is_null($operator)) {
					switch ($operator) {
						case 'equal':
						case '=':
							$isBetween = true;
							break;
						case 'nequal':
						case '!=':
							$isNotBetween = true;
							break;
						case 'high':
						case '>':
							$operator = '>';
							break;
						case 'less':
						case '<':
							$operator = '<';
							break;
						case 'eqhigh':
						case '>=':
							$operator = '>=';
							break;
						case 'eqless':
						case '<=':
							$operator = '<=';
							break;
						case 'between':
						case '≶':
							$isBetween = true;
							break;
						case 'like':
							$operator = 'like';
							$value = '%' . $value . '%';
							break;
						default:
							$operator = '=';
							break;
					};
				}
				
				if ($isBetween) {
					$query->whereBetween($field, [$value, $valueMax]);
				} elseif ($isNotBetween) {
					$query->whereNotBetween($field, [$value, $valueMax]);
				} else {
					$query->where($field, $operator, $value);
				}
			}
			
			if(IntVal($userRole) == 1) {
				$query->withTrashed();
			}

			$items = $query->get();
			$surveys = $query->select('survey_id', 'socio_data_id')->get()->toArray();

            $results = Statistic::buildStatisticItemsToShow($items);
        }

		$info = Statistic::getTotalsAndPercentagesInfo($surveys, $members);
		$totals = $info['totals'];
		$percentages = $info['percentages'];
		
		$view = view('statistic.table')
                    ->with('module', $this->module)
                    ->with('results', $results)
					->with('totals', $totals)
					->with('percentages', $percentages);
					
		return Response::json(['view' => (string) $view]);
	}
	public function deleteAllSurveysAnswered($id)
	{
		$surveys = Survey::where('user_id', $id)->get();
		foreach($surveys as $survey) {
			if ($survey->delete()) {
                // Delete relations
                if ($survey && $survey->relationsToDelete) {
                    foreach ($survey->relationsToDelete as $relation) {
                        $survey->{$relation}()->delete();
                    }
                }
			}			
		}
		
		$user = User::find($id);
		$members = $user->userData->members;
		$surveys = StatisticModel::select('survey_id', 'socio_data_id')->where('id', $id)->get()->toArray();
		
		$results = Statistic::getStatisticForAGivenUser($id);
		
		$info = Statistic::getTotalsAndPercentagesInfo($surveys, $members);
		$totals = $info['totals'];
		$percentages = $info['percentages'];
		
        $view = view('statistic.table')
                    ->with('module', $this->module)
                    ->with('results', $results)
					->with('totals', $totals)
					->with('percentages', $percentages);
					
		return Response::json(['view' => (string) $view]);
	}
    public function pdfStatisticQrSimpleDownload($id)
    {
        $title = trans($this->module . '.mtitle');
        $user = User::find($id);
        $user_name = $user->userData->name;
        $code = $user->username;

        $pdf = PDF::loadView('pdf.qr-simple', compact('code', 'user_name'))->setPaper('a4', 'portrait');
        return $pdf->download('cms-' . trans($this->module . '.pdfqrsimpletitle') . '.pdf');
    }
    public function pdfStatisticQrMultipleDownload($id)
    {
        $title = trans($this->module . '.mtitle');
        $user = User::find($id);
        $user_name = $user->userData->name;
        $code = $user->username;

        $pdf = PDF::loadView('pdf.qr-multiple', compact('code', 'user_name'))->setPaper('a4', 'portrait');
        return $pdf->download('cms-' . trans($this->module . '.pdfqrmultipletitle') . '.pdf');
    }
}