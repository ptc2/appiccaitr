<?php

namespace App\Http\Controllers\Cms;

use App\Presenters\UserCmsPresenter;

use App\Models\UserCms;

class UserCmsController extends BaseController
{

     /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
      */
     public function __construct(UserCms $model, UserCmsPresenter $presenter = null)
     {
          parent::__construct($model, $presenter);

          $this->module = 'user_cms';
          $this->urlBack = array(
               'store' => '',
               'update' => ''
          );
     }
        
     public function getInfo(){
         $response = "{response}";
         return $response:
     }
}
