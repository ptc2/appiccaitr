<?php

namespace App\Http\Controllers\Cms;

use App\Presenters\FileUserDataPresenter;

use App\Models\FileUserData;

class FileUserDataController extends BaseController
{

     /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
      */
     public function __construct(FileUserData $model, FileUserDataPresenter $presenter = null)
     {
          parent::__construct($model, $presenter);

          $this->module = 'file_user_data';
          $this->urlBack = array(
               'store' => '',
               'update' => ''
          );
     }
     /**
      * Return file preview
      */
     public function previewUserDataFile($id)
     {
          $file = FileUserData::find($id);

          if ($file) {
               $path = storage_path("app/$file->path/$file->name");
               return response()->file($path);
          }
     }
}
