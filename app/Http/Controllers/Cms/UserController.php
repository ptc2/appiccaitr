<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

use App\Presenters\UserPresenter;

use App\Models\User;
use App\Models\UserData;
use App\Models\UserCms;
use App\Models\UserCmsProfile;

use App\Helpers\Helper;

use App\Mail\UserCreated;

class UserController extends BaseController
{

    /**
      * Creates a new instance of the controller with a new instance of the model
      *
      * @param BaseModel $model
      * @param Presentable $presenter
    */
    public function __construct(User $model, UserPresenter $presenter = null)
    {
        parent::__construct($model, $presenter);

        $this->module = 'user';
        $this->urlBack = array(
            'store' => '',
            'update' => ''
        );
    }
    /**
      * Show the form for editing the specified resource.
      *
      * @param int $id
      * @return Response
    */
    public function edit($id = NULL)
    {
        return parent::edit($id);
    }
    /**
      * Store a newly created resource in storage.
      *
      * @return Response
    */
    public function store($id = NULL)
    {
        $result = DB::transaction(function () use ($id) {
            if ($id != null) {
                if (!$this->model->options['can_edit']) return redirect()->route($this->module . '.index');
                $user = User::find($id);
            } else {
                if (!$this->model->options['can_create']) return redirect()->route($this->module . '.index');
            }

            $rules = (new UserData())->getRules();
			$rules['email'][1] = $rules['email'][1] . $id . ',id,deleted_at,NULL';

            $success = false;
            $success = $this->model->validate(array_filter(request()->all()), $rules);

            if (!$success) {
                return $this->responseInvalid(__FUNCTION__);
            }

            if(!isset($user)) {
                $code = Helper::createCode();
                $password = Hash::make($code);

                $data = [
                    'username' => $code, 
                    'password' => $code,
                    'active' => 1,
                ];

                $user = User::create($data);

                $cms_data = [
                    'name' => request()->name,
                    'email' => request()->email,
                    'username' => $code,
                    'password' => $password,
                    'user_cms_profile_id' => 2,
                    'active' => 1,
                    'related_user_id' => $user->id
                ];

                $userCMS = UserCms::create($cms_data);
            }

            $user_data = [
                'name' => request()->name,
                'email' => request()->email,
                'manager' => request()->manager,
                'address' => request()->address,
                'province' => request()->province,
                'city' => request()->city,
                'members' => request()->members,
				'phone' => request()->phone
            ];

            $userData = $user->userData;
            if(!$userData) {
                $userData = UserData::create($user_data);
                $userData->user()->associate($user);
                $userData->save();
            }else{
                $userData->update($user_data);
            }

            return $this->responseSucess('store', $id);
        });

        return $result;
    }
	
	public function sendEmailConfirmation($id = NULL)
	{
		if ($id != null) {
			$user = User::find($id);
			
			Mail::to($user->userData->email)
				->bcc('josemanuelmarin@solbyte.com')					
				->send(new UserCreated($user));
					
			return $this->responseSucess(__FUNCTION__, $user->id);
		} else {
			return $this->responseInvalid(__FUNCTION__);
		}
	}
}
