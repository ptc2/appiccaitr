<?php

/**
 * Foundation Controller
 *
 * Foundation controller based on Laravel's BaseController.
 *
 * @author fmgonzalez
 * @uses \BaseController
 * @since 2014/4/15
 */

namespace App\Http\Controllers\Cms;

use Carbon\Carbon;

use Auth;
use Session;
use Redirect;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use Route;
use PDF;
use Illuminate\Support\Str;
use App\Services\TimelineService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Helpers\Application;
use App\Presenters\Presentable;
use App\Exceptions\AppException;
use Illuminate\View\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Stringy\StaticStringy as Stringy;

use App\Models\BaseModel;

/**
 * Foundation Controller
 *
 * Foundation controller based on Laravel's BaseController.
 */
class BaseController extends Controller
{
    use AuthorizesRequests;
    /**
     * The main model of the controller
     *
     * @var App\Models\BaseModel
     */
    protected $model;

    /**
     * The Presenter layer used by the main model
     *
     * @var App\Presenters\Presenter
     */
    protected $presenter;

    /**
     * Module name
     *
     * @var string
     */
    protected $module;

    /**
     * Controller name
     *
     * @var string
     */
    protected $name;

    /**
     * Array of urlBack for each method
     *
     * @var array
     */
    protected $urlBack;

    /**
     * Array of views for each method
     *
     * @var array
     */
    protected $views;

    /**
     * Array of relations
     *
     * @var array
     */
    protected $relations;

    /**
     * Enables the log
     *
     * @var bool
     */
    protected $log = true;

    protected $storeInputExtra = [];

    protected $allowCheckInGrind = false;

    protected $modelToTimeline = null;

    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
    public function __construct(BaseModel $model, Presentable $presenter = null)
    {
        if (!empty($model)) {
            $this->model = new $model();
        }
        $this->setPresenter($presenter);

        $this->urlBack = array(
            'store' => $this->module . '.edit',
            'update' => $this->module . '.edit'
        );

        $this->name = $this->getName();
        Application::setCMS();
    }

    /**
     * Autoset presenter layer
     *
     * @param Presentable $presenter
     */
    protected function setPresenter(Presentable $presenter = null)
    {
        $moduleName = Stringy::underscored(preg_replace('/^App\\\Http\\\Controllers\\\([A-Za-z0-9]+)Controller$/', '$1', get_class($this)));

        if (in_array(Route::currentRouteName(), [$moduleName . '.export', $moduleName . '.listToPdf'])) {
            $presenterClass = str_replace(array('Controllers', 'Http\\'), array('Presenters\Download', ''), rtrim(get_class($this), 'Controller') . 'DownloadPresenter');
            if (class_exists($presenterClass)) {
                $this->presenter = new $presenterClass();
            }
        } else {
            if ($presenter) {
                $this->presenter = $presenter;
            } else {
                if (!$this->presenter) {
                    $presenterClass = str_replace(array('Controllers', 'Http\\'), array('Presenters', ''), rtrim(get_class($this), 'Controller') . 'Presenter');
                    if (class_exists($presenterClass)) {
                        $this->presenter = new $presenterClass();
                    }
                }
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            //$this->authorize(__FUNCTION__ . '_' . $this->model->gEttable(), get_class($this->model));
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        $this->log(__FUNCTION__);

        // Empty items
        $items = array();

        // Get the template
        $template = $this->getTemplate(__FUNCTION__);

        $module = DB::table('module')->where('name', $this->module)->first();
        $iconHeader = 'fa fa-table';
        if ($module) {
            $iconHeader = $module->icon . ' ' . $module->type_icon;
        }

        $view = view($template)->with('items', $items)
            ->with('module', $this->module)
            ->with('iconHeader', $iconHeader)
            //->with('columns', $columns)
            ->with('params', array());

        $this->timeline(__FUNCTION__, get_class($this->model));

        // In search
        if (request()->ajax()) {
            $return['view'] = (string) $view;
            $return['message'] = 'B&uacute;squeda finalizada correctamente';
            return Response::json($return);
        } else {
            return $view;
        }
    }

    /**
     * Display contents to bootstrap datatable grid component
     *
     * @return json
     */
    public function grid($excelPdfPresenter = null, $params = null)
    {
        try {
            //$this->authorize(__FUNCTION__ . '_' . $this->model->gEttable(), get_class($this->model));
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        self::log(__FUNCTION__, 'debug');

        $cols = request()->input('columns');
        $tablesrelated = [];
        $relationsColumns = [];
        $relationsManyColumns = [];

        if ($this->model->relationsGridColumns) {
            foreach ($this->model->relationsGridColumns as $alias => $field) {
                $relationsColumns[] =  $alias;
            }
        }

        if ($this->model->relationsManyGridColumns) {
            foreach ($this->model->relationsManyGridColumns as $alias => $field) {
                $relationsManyColumns[] =  $alias;
            }
        }

        $sSkip = request()->input('iDisplayStart', 0);
        $sLimit = request()->input('iDisplayLength', 25);
        $sSearch = request()->input('sSearch');

        $args = func_get_args();

        // Only Simple sort
        $sOrder = request()->input('iSortCol_0');
        $sSortDir = request()->input('sSortDir_0', 'asc');

        $onlyTrashed = request()->input('onlyTrashed', false);

        $model = $this->model;

        $table = $model->getTable();

        // Projection
        if (!empty($cols))
            $cols = array_map('trim', explode(',', $cols));

        $qCols = [];
        $orderCols = [];
        foreach ($cols as $c) {
            if (in_array($c, $model->fieldsLangs) && !in_array($c, $this->model->gridColumnsToHide)) {
                $c = $table . '_langs.' . $c;
                $qCols[] = $c;
                $orderCols[] = $c;
            } elseif (in_array($c, $relationsColumns) && !in_array($c, $this->model->gridColumnsToHide)) {
                $relationsGridColumns = $this->model->relationsGridColumns[$c];

                $fields = explode('_', $c);
                $field = array_pop($fields);
                $alias = implode('_', $fields);
                $modelName = '\\App\\Models\\' . Stringy::upperCamelize($alias);

                if (isset($relationsGridColumns['field'])) {
                    $field = $relationsGridColumns['field'];
                    $t = isset($relationsGridColumns['table']) ? $relationsGridColumns['table'] :  $alias;
                    $alias = isset($relationsGridColumns['alias']) ? $relationsGridColumns['alias'] : $t;
                    $modelName = '\\App\\Models\\' . Stringy::upperCamelize($t);
                }

                $m = new $modelName();
                if (in_array($field, $m->fieldsLangs)) {
                    $alias = $alias . '_langs';
                }

                if (isset($relationsGridColumns['seo']) && $relationsGridColumns['seo']) {
                    $qCols[] = 'seo_data_langs_' . $t . '.' . $relationsGridColumns['field'] . ' AS ' . $c;
                } else {
                    $qCols[] = $alias . '.' . $field . ' AS ' . $c;
                }
                $orderCols[] = $alias . '.' . $field;
            } elseif (in_array($c, $relationsManyColumns)) {
                // dd($this->model->$c);
            } else {
                if(!in_array($c, $this->model->gridColumnsToHide)) {
					$c = $table . '.' . $c;
					$qCols[] = $c;
					$orderCols[] = $c;					
				}
            }
        }

        $cols = $qCols;
		
		if ($this->model->gridColumnsToHide) {
			$cols = array_diff($cols, $this->model->gridColumnsToHide);
		}
		
        // Match
        $where = '';

        if (!empty($sSearch)) {
            self::log(__FUNCTION__, 'debug', $sSearch);
            $cont = 0;
            foreach ($cols as $field) {
                // searchables
                if (request()->input('bSearchable_' . $cont, false)) {
                    $where .= Helper::dbEscapeAs($field) . ' LIKE \'%' . Helper::dbEscapeLike($sSearch) . '%\' OR ';
                }
                $cont++;
            }
            $where = rtrim($where, " OR ");
        }

        // Match
        $where_and = '';

        if (!empty($args)) {
            foreach ($args as $arg) {
                if (is_array($arg)) {
                    $where_and = isset($arg['where']) ? $arg['where'] . ' AND ' : '';
                }
            }
        }

        // Adding custom search
        $where_and .= $this->queryWithOperators();

        if ($model->isPrivate()) {
            $where_and .= ' user_id = ' . Auth::user()->getScopeKey() . ' AND ';
        }

        $related_filter = $this->relationsFilter();

        if (!empty($related_filter)) {
            $where_and .= '(' . $related_filter . ') AND ';
        }

        // Borrar en un futuro cuando usemos para todos los modelos solo el campo deleted_at.
        /*if (method_exists($model, 'getDeletedAtColumn') && ! $onlyTrashed ){
            $where_and .= $model->getDeletedAtColumn() . ' IS NULL AND ';
        }*/

        $where_and = rtrim($where_and, " AND ");

        $where = empty($where_and) ? $where : $where_and . (empty($where) ? '' : ' AND (' . $where . ')');

        // sort
        $orderCol = $orderCols[$sOrder];

        $items = $model->select(empty($cols) ? '*' : $cols);

        if ($model->fieldsLangs) {
            $items = $items->leftJoin($table . '_langs', function ($join) use ($table) {
                $join->on($table . '_langs.' . $table . '_id', '=', $table . '.id')
                    ->where($table . '_langs.language_id', Language::getMainLanguage()->id);
            });
        }

        if ($this->model->relationsGridColumns) {
            foreach ($this->model->relationsGridColumns as $alias => $value) {
                if (!isset($value['table'])) {
                    $fields = explode('_', $alias);
                    $field = array_pop($fields);
                    $t = implode('_', $fields);
                } else {
                    $t = $value['table'];
                }

                $tAlias = isset($value['alias']) ? $value['alias'] : $t;

                if (isset($value['seo']) && $value['seo']) {

                    $items = $items->leftJoin('seo_data as seo_data_' . $tAlias, function ($q) use ($value, $tAlias) {
                        $q->on('seo_data_' . $tAlias . '.external_id', '=',  $tAlias . '.id');
                        $q->where('seo_data_' . $tAlias . '.module_id', '=', $value['module']);
                    })
                        ->leftJoin('seo_data_langs as seo_data_langs_' . $tAlias, function ($q) use ($tAlias) {
                            $q->on('seo_data_langs_' . $tAlias . '.seo_data_id', '=', 'seo_data_' . $tAlias . '.id');
                            $q->where('seo_data_langs_' . $tAlias . '.language_id', '=', Language::getMainLanguage()->id);
                        });
                }

                if (in_array($tAlias, $tablesrelated)) continue;
                $tablesrelated[] = $tAlias;
                $tLangs = $t . '_langs';
                $tAliasLangs = $tAlias . '_langs';
                $modelName = '\\App\\Models\\' . Stringy::upperCamelize($t);
                $m = new $modelName();

                if (array_key_exists('mul', $value)) {
                    $fkTable = isset($value['mul']['table']) ? $value['mul']['table'] : $table;
                    $fkField = isset($value['mul']['field']) ? $value['mul']['field'] : $t . '_id';
                    $items->leftJoin("$t as $tAlias", $tAlias . '.' . $fkField, $fkTable . '.id');
                } else {
                    $fkTable = isset($value['fk']['table']) ? $value['fk']['table'] : $table;
                    $fkField = isset($value['fk']['field']) ? $value['fk']['field'] : $t . '_id';
                    $items->leftJoin("$t as $tAlias", $tAlias . '.id', $fkTable . '.' . $fkField);
                }

                if ($m->fieldsLangs) {
                    if (!in_array($tAliasLangs, $tablesrelated)) {
                        $tablesrelated[] = $tLangs;
                        //$items = $items->leftJoin("$tLangs as $tAliasLangs", $tAliasLangs.'.'.$t.'_id', '=', $tAlias.'.id');
                        $items->leftJoin("$tLangs as $tAliasLangs", function ($join) use ($t, $tAliasLangs, $tAlias, $tLangs) {
                            $join->on($tAliasLangs . '.' . $t . '_id', '=', $tAlias . '.id')
                                ->where($tAliasLangs . '.language_id', '=', Language::getMainLanguage()->id);
                        });
                    }
                }
            }
        }
        /*
        * Filter
        */
        $arrayWhere = [];
        if ($model->filters) {
            $filters = $model->filters;

            $values = array_filter(request()->only(array_keys($filters)), function ($value) {
                return $value !== '';
            });

            if (!empty($values)) {
                foreach ($filters as $key => $filter) {
                    //$filter = $filters[$key];
                    $relation = Arr::get($filter, 'relation');
                    $field = Arr::get($filter, 'field');
                    $type = Arr::get($filter, 'type') ?: 'text';
                    $operator = request()->input($key . '_operator');
                    $value = Arr::get($values, $key);
                    $valueMax = request()->input($key . '_max');
                    $isMultiple = $type == 'multiple' ? true : false;
                    $isBetween = false;

                    switch ($relation) {
                        case 'fk':
                            $t = $filter['table'];
                            if (array_key_exists('mul', $filter)) {
                                $fkTable = isset($filter['mul']['table']) ? $filter['mul']['table'] : $table;
                                $fkField = isset($filter['mul']['field']) ? $filter['mul']['field'] : $t . '_id';
                                $tc1 = $fkTable . '.id';
                                $tc2 = $t . '.' . $fkField;
                            } else {
                                $fkTable = isset($filter['fk']['table']) ? $filter['fk']['table'] : $table;
                                $fkField = isset($filter['fk']['field']) ? $filter['fk']['field'] : $t . '_id';
                                $tc1 = $fkTable . '.' . $fkField;
                                $tc2 = $t . '.id';
                            }
                            $camelizeModule = Stringy::upperCamelize($t);
                            $class = '\\App\\Models\\' . $camelizeModule;
                            $modelFK = new $class();

                            if (!in_array($t, $tablesrelated)) {
                                $tablesrelated[] = $t;
                                $items = $items->leftJoin($t, $tc1, '=', $tc2);
                            }

                            if ($field) {
                                if (in_array($field, $modelFK->fieldsLangs)) {
                                    $tableLang = $t . '_langs';
                                    if (!in_array($tableLang, $tablesrelated)) {
                                        $tablesrelated[] = $tableLang;
                                        $items = $items->leftJoin($tableLang, $t . '.id', '=', $tableLang . '.' . $t . '_id');
                                    }
                                    $field = $tableLang . '.' . $field;
                                    $arrayWhere[] = [$tableLang . '.language_id', '=', Language::getMainLanguage()->id];
                                } else {
                                    $field = $t . '.' . $field;
                                }
                            }
                            break;
                        case 'nm':
                            $nm = $filter['nm'];
                            $camelizeModule = Stringy::upperCamelize($nm['otherTable']);
                            $class = '\\App\\Models\\' . $camelizeModule;
                            $modelNM = new $class();

                            if (!in_array($nm['tableNM'], $tablesrelated)) {
                                $tablesrelated[] = $nm['tableNM'];
                                $t = array_key_exists('table', $filter) ? $filter['table'] : $table;
                                $items = $items->leftJoin($nm['tableNM'], $t . '.id', '=', $nm['tableNM'] . '.' . $nm['thisID']);
                            }
                            if (!in_array($nm['otherTable'], $tablesrelated)) {
                                $tablesrelated[] = $nm['otherTable'];
                                $items = $items
                                    ->leftJoin($nm['otherTable'], $nm['tableNM'] . '.' . $nm['otherID'], '=', $nm['otherTable'] . '.id');
                            }

                            if ($field) {
                                if (in_array($field, $modelNM->fieldsLangs)) {
                                    $items = $items->leftJoin($nm['otherTable'] . '_langs', $nm['otherTable'] . '.id', '=', $nm['otherTable'] . '_langs.' . $nm['otherTable'] . '_id');
                                    $field = $nm['otherTable'] . '_langs.' . $field;
                                    $arrayWhere[] = [$nm['otherTable'] . '_langs.language_id', '=', Language::getMainLanguage()->id];
                                } else {
                                    $field = $nm['otherTable'] . '.' . $field;
                                }
                            }
                            break;
                        default:
                            if (array_key_exists('table', $filter)) {
                                $t = $filter['table'];
                                $camelizeModule = Stringy::upperCamelize($nm['otherTable']);
                                $class = '\\App\\Models\\' . $camelizeModule;
                                $m = new $class();
                            } else {
                                $t = $table;
                                $m = $model;
                            }

                            if ($field) {
                                if (in_array($field, $m->fieldsLangs)) {
                                    $field = $t . '_langs.' . $field;
                                } else {
                                    $field = $t . '.' . $field;
                                }
                            }
                    }

                    if (!is_null($value)) {
                        switch ($type) {
                            case 'text';
                                $value = trim($value);
                                break;
                            case 'date';
                                $value = \Carbon\Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($value)))->toDateString();
                                $valueMax = $valueMax ? \Carbon\Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($valueMax)))->toDateString() : '';
                                break;
                        }

                        switch ($operator) {
                            case 'equal':
                                $operator = '=';
                                break;
                            case 'nequal':
                                $operator = '!=';
                                break;
                            case 'high':
                                $operator = '>';
                                break;
                            case 'less':
                                $operator = '<';
                                break;
                            case 'eqhigh':
                                $operator = '>=';
                                break;
                            case 'eqless':
                                $operator = '<=';
                                break;
                            case 'between':
                                $isBetween = true;
                                break;
                            case 'like':
                                $operator = 'like';
                                $value = '%' . $value . '%';
                                break;
                            default:
                                $operator = '=';
                                break;
                        };

                        if ($isBetween) {
                            $items = $items->whereBetween($field, [$value, $valueMax]);
                        } elseif ($isMultiple) {
                            if (!is_array($value)) {
                                $value = [$value];
                            }
                            $items = $items->whereIn($field, $value);
                        } else {
                            $arrayWhere[] =  [$field, $operator, $value];
                        }
                    }
                }
            }
        }

        $items = $items->whereRaw('(' . (empty($where) ? '1=1' : $where) . ')');

        if ($model->sortDefaultColumn && $model->sortDefaultDir && $sOrder == 0) {
            $items = $items->orderBy($table . '.' . $model->sortDefaultColumn, $model->sortDefaultDir);
        } else {
            $items = $items->orderBy($orderCol, $sSortDir);
        }

        if ($onlyTrashed)
            $items = $items->onlyTrashed();

        $items = $items->where($arrayWhere);

        $items->groupBy($table . '.id');

        $auxItems = clone $items;
        $auxItems = $auxItems->get();

        $filtered = $auxItems->count();

        if ($sLimit > 0 && is_null($excelPdfPresenter)) {
            $items->skip($sSkip)->take($sLimit);
        }

        if (count($relationsManyColumns) > 0) {
            $items->with($relationsManyColumns);
        }
		
		$userRole = Auth::user()->user_cms_profile_id;
		if(IntVal($userRole) == 1) {
			$items->withTrashed();
		}

        $items = $items->get();

        // Debug query
        if (Config::get('app.debug')) {
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $this->log(__FUNCTION__, 'debug', $last_query['query']);
        }

        if ($model->isNotUsingDBView()) {
            // Presenters
            if ($excelPdfPresenter) {
                $presenter = new $excelPdfPresenter();
                $items = array_map('array_values', $presenter->collection($items, $presenter)->toArray());
            } elseif ($this->presenter) {
                $items = array_map('array_values', $this->presenter->collection($items, $this->presenter)->toArray());
            } else {
                $items = array_map('array_values', $items->toArray());
            }
        } else {

            foreach ($items as $key => $i) {
                $items[$key] = (array) $i;
            }

            $items = array_map('array_values', $items);
        }

        $result = array(
            'sEcho'                 => intval(request()->input('sEcho')),
            'iTotalRecords'         => isset($total) ? $total : $filtered,
            'iTotalDisplayRecords'  => $filtered,
            'aaData'                => $items,
            'types'                 => array(),
            'items'                 => $auxItems

        );
        return Response::json($result);
    }

    public function manyRelationGrid()
    {
		$userRole = Auth::user()->user_cms_profile_id;
        $id         = request()->input('relatedId');
        $relation   = request()->input('relation');
        $item       = (IntVal($userRole) == 1) ? $this->model->withTrashed()->find($id) : $this->model->find($id);

        $itemsRelation = $item->$relation;

        if ($this->model->relationsManyGridColumns[$relation]['reverse'])
            $itemsRelation = $itemsRelation->reverse();

        if ($this->model->relationsManyGridColumns[$relation]['take'])
            $itemsRelation = $itemsRelation->slice(0, $this->model->relationsManyGridColumns[$relation]['take']);

        if ($this->model->relationsManyGridColumns[$relation]['pivotKeys'] && count($this->model->relationsManyGridColumns[$relation]['pivotKeys']) > 0) {
            foreach ($this->model->relationsManyGridColumns[$relation]['pivotKeys'] as $key => $pivotKey) {
                foreach ($itemsRelation as $key => $a) {
                    $itemsRelation[$key][$pivotKey] = $a->pivot->$pivotKey;
                }
            }
        }

        // Find the template page
        $initial_template = "items-related-modal";
        $template = $this->getTemplate($initial_template);

        // Show the page
        $view = view($template)->with('module', $this->module)
            ->with('item', $item)
            ->with('items', $itemsRelation)
            ->with('relation', $this->model->relationsManyGridColumns[$relation])
            ->with('blade_form_params',  [])
            ->with('form', array(
                'method'    => 'POST',
                'route'     => $this->module . '.manyRelationStore',
                'id'        => 'model-relation-form',
                'files'     => false,
            ));

        if ($this->model->relationsCombo) {
            foreach ($this->model->relationsCombo as $key => $value) {
                $modelName = '\\App\\Models\\' . Stringy::upperCamelize($key);
                $model = new $modelName();
                $varName = Stringy::camelize($key);
                $select = implode(",", $value) . ',' . $key . '.id as id';
                $data =  DB::table($key)->selectRaw($select);

                $displayFields = $value;

                if ($model->fieldsLangs) {
                    $data = $data->leftJoin($key . '_langs AS tl', function ($join) use ($key) {
                        $join->on('tl.' . $key . '_id', '=', $key . '.id')
                            ->where('tl.language_id', '=', Language::getMainLanguage()->id);
                    });
                }
                $data = $data->get();

                $select_values = [];
                foreach ($data as $item) {
                    $values = [];
                    $k = null;
                    foreach ($item as $field => $fieldValue) {
                        if (in_array($field, $displayFields) && $fieldValue) {
                            $values[] = $fieldValue;
                        } else {
                            $k = $fieldValue;
                        }
                    }
                    if ($values) {
                        $values = implode(" | ", $values);
                        $select_values[$k] = $values;
                    }
                }
                $select_values[0] = trans('global.select-none');
                ksort($select_values);
                $view->with($varName, $select_values);
            }
        }

        return Response::json(['view' => (string) $view]);
    }

    public function storeRelationGrid()
    {
        $model    = request()->input('model');
        $relation = request()->input('relation');

        $item = $this->model->find($model['id']);

        $relationItems = [];


        if ($this->model->relationsManyGridColumns[$relation]['pivotKeys'] && count($this->model->relationsManyGridColumns[$relation]['pivotKeys']) > 0) {
            // With pivot key
            foreach ($model[$relation] as $key => $value) {


                foreach ($this->model->relationsManyGridColumns[$relation]['pivotKeys'] as $pivotKey) {
                    if (isset($value[$pivotKey]))
                        $relationItems[$value['id']] = [$pivotKey => intval($value[$pivotKey])];
                }
            }
        } else {
            // without pivot key
            foreach ($model[$relation] as $key => $value) {
                $relationItems[] = $value['id'];
            }
        }

        $item->$relation()->sync($relationItems);

        return Response::json([$item]);
    }

    /**
     * Gets the related condition for the model
     *
     * @return string
     */
    protected function relatedFilter()
    {
        $result = '';
        if (in_array('related_type', $this->model->getFillable())) {
            $table = $this->model->getTable();
            $related_type = request()->input('related_type');
            $namespace = (new \ReflectionClass(get_class($this->model)))->getNamespaceName();

            $related_type = strpos($related_type, '\\')
                ? $related_type
                : $namespace . '\\' . $related_type;

            $related_id = request()->input('related_id');
            if (!empty($related_type)) {
                $result .= ' ' . $table . '.related_type = \'' . str_replace('\\', '\\\\', $related_type) . '\' AND ';
                $result .= $table . '.related_id = ' . $related_id;
            } else {
                $result .= ' 1 = 0 AND ';
            }
        }
        return $result;
    }
    /**
     * Gets the related condition for the model
     *
     * @return string
     */
    protected function relationsFilter()
    {

        $result = '';
        $relatedType = request()->input('related_type');
        if ($relatedType) {
            $table = $this->model->getTable();
            $relatedID = request()->input('related_id');
            $result .= "$table.$relatedType = $relatedID";
        }
        return $result;
    }
    /**
     * Destroy model and its relations. Then redirect to index.
     *
     * @return Response
     */
    public function cancel($id)
    {
        self::log(__FUNCTION__);
		
		$userRole = Auth::user()->user_cms_profile_id;
        $item = (IntVal($userRole) == 1) ? $this->model->withTrashed()->find($id) : $this->model->find($id);

        // Delete relations
        foreach ($this->model->relations as $relation) {
            $parsedRelationName = explode('_', $relation);
            $relationClass = '';

            foreach ($parsedRelationName as $namePiece) {
                $relationClass .= ucfirst($namePiece);
            }

            $relationModel = '\\App\\Models\\' . ucfirst($relationClass);
            $relationObj = $relationModel::where('related_id', $id)->get();

            foreach ($relationObj as $obj) {
                $obj->forceDelete();
            }
        }

        // Delete model
        $item->forceDelete();

        return request()->ajax() ? $this->responseSucess(__FUNCTION__) : redirect()->action($this->name . '@index');
    }

    /**
     * Create a new resource and redirect to edit. To disable the creation and to show the view instead, just send a URL query parameter called 'autocreate' set to '0'.
     *
     * @return Response
     */
    public function create()
    {
        try {
            //$this->authorize(__FUNCTION__ . '_' . $this->model->gEttable(), get_class($this->model));
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        // Dave Timeline navigation
        $this->timeline('navigation', get_class($this->model));

        self::log(__FUNCTION__);

        $autocreate = request()->has('autocreate') ? request()->input('autocreate') : true;
        $columns = DB::getSchemaBuilder()->getColumnListing($this->model->getTableName());

        if ($autocreate && in_array('saved', $columns)) {    // Create the resource and redirect to edit
            $rules = $this->model->getRules();

            // Remove required validation rules
            foreach ($rules as $field => $rule) {
                $requiredIndex = array_search('required', $rule);

                if ($requiredIndex !== false) {
                    unset($rules[$field][$requiredIndex]);

                    if (empty($rules[$field])) {
                        unset($rules[$field]);
                    }
                }
            }

            // Remove unique validation rules
            foreach ($rules as $field => $rule) {
                foreach ($rule as $uniqueIndex => $ruleType) {
                    if (stripos($ruleType, 'unique') !== false) {
                        unset($rules[$field][$uniqueIndex]);
                    }
                }

                if (empty($rules[$field])) {
                    unset($rules[$field]);
                }
            }

            $module = '\\App\\Models\\' . ucfirst($this->module);

            /*if(in_array('code', $columns)) {
				$this->model->code = $module::where('saved', true)->max('code') + 1;
			}*/

            $this->model->saved = 0;
            $this->model->save($rules);
            $view = redirect()->action($this->name . '@edit', ['id' => $this->model->id, 'deleteOnCancel' => 1]);
        } else {    // Return the view

            /*if(in_array('code', $columns)) {
                $module = '\\App\\Models\\'.Stringy::upperCamelize($this->module);
                $this->model->code = $module::max('code') + 1;
            }*/

            // Find the template page
            $initial_template = 'edit';
            $template = $this->getTemplate($initial_template);

            $this->model->getSEO();

            $blade_form_params = array();

            // Show the page
            $view = view($template)->with('module', $this->module)
                ->with('item', $this->model)
                ->with('relations', array())
                ->with('blade_form_params',  $blade_form_params)
                ->with('form', array(
                    'method' => 'POST',
                    'route' => $this->module . '.store',
                    'id' => 'model-create-form',
                    'files' => true,
                ));


            if ($this->model->relationsCombo) {
                foreach ($this->model->relationsCombo as $key => $value) {
                    $modelName = '\\App\\Models\\' . Stringy::upperCamelize($key);
                    $model = new $modelName();
                    $varName = Stringy::camelize($key);
                    $select = implode(",", $value) . ',' . $key . '.id as id';
                    $data =  DB::table($key)->selectRaw($select);
                    $displayFields = $value;

                    if ($model->fieldsLangs) {
                        $data = $data->leftJoin($key . '_langs AS tl', function ($join) use ($key) {
                            $join->on('tl.' . $key . '_id', '=', $key . '.id')
                                ->where('tl.language_id', '=', Language::getMainLanguage()->id);
                        });
                    }
                    $data = $data->get();

                    $select_values = [];
                    foreach ($data as $item) {
                        $values = [];
                        $k = null;
                        foreach ($item as $field => $fieldValue) {
                            if (in_array($field, $displayFields) && $fieldValue) {
                                $values[] = $fieldValue;
                            } else {
                                $k = $fieldValue;
                            }
                        }
                        if ($values) {
                            $values = implode(" | ", $values);
                            $select_values[$k] = $values;
                        }
                    }
                    $select_values[null] = trans('global.select-none');
                    ksort($select_values);
                    $view->with($varName, $select_values);
                }
            }

            // Master Data
            if ($this->model->masterDataFields) {
                foreach ($this->model->masterDataFields as $field) {
                    $varName = Stringy::camelize($field);
                    $masterDataModel = new MasterData();

                    $masterDataType = DB::table('master_data_type')->select('id')->where('label', $varName)->first();

                    $data = DB::table('master_data AS t')->select('text', 't.id');
                    if ($masterDataModel->fieldsLangs) {
                        $data = $data->leftJoin('master_data_langs AS tl', function ($join) {
                            $join->on('tl.master_data_id', '=', 't.id')
                                ->where('tl.language_id', '=', Language::getMainLanguage()->id);
                        });
                    }
                    $data = $data->where('master_data_type_id', '=', $masterDataType->id);
                    $data = $data->pluck('text', 'id')
                        ->put('', trans('global.select-none'))
                        ->toArray();
                    ksort($data);
                    $view->with($varName, $data);
                }
            }
        }

        return (request()->ajax())
            ? Response::json(['view' => (string) $view])
            : $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id = null)
    {
        try {
            /*$id 
            ? $this->authorize('update' . '_' . $this->model->getTable(), get_class($this->model)) 
            : $this->authorize('create' . '_' . $this->model->getTable(), get_class($this->model));*/
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        if ($id != null) {
            if (!$this->model->options['can_edit']) return redirect()->route($this->module . '.index');
        } else {
            if (!$this->model->options['can_create'] && !$this->model->options['can_send']) return redirect()->route($this->module . '.index');
        }

        \DB::enableQueryLog();

        self::log(__FUNCTION__);

        // $input = request()->except(array('_token', 'id'));

        $input = request()->only($this->model->fields);

        //unset($input['id']);
        if (count($this->storeInputExtra) > 0) {
            $input = array_merge($input, $this->storeInputExtra);
        }
        if ($id == NULL || $id < 0) {
            unset($input['id']);
        }
        $this->log(__FUNCTION__, 'info', $id);

        $only = request()->only($this->model->fields);
        $fillables = request()->all();
        //dd($fillables);
        $modelClassUses = class_uses($this->model);

        // Get the model to update
        $this->log(__FUNCTION__, 'debug', 'Begin transaction ' . get_class($this->model) . ' ' . $id);
        DB::beginTransaction();
        try {
            $success = false;
            try {
                $success = $this->preStore($this->model); //Mejorar el control de excepciones.

                if ($success) {
                    // Sets the authored fields
                    if ($this->model->isAuthoredBehaviour()) {
                        $this->model->updateAuthored();
                    }

                    $rules = $this->model->getRules();
                    $success = false;
                    $success = $this->model->validate(request()->all(), $rules);

                    if ($id != null) {
						$userRole = Auth::user()->user_cms_profile_id;
                        if (array_key_exists('Illuminate\Database\Eloquent\SoftDeletes', $modelClassUses)) {
                            $this->model = $this->model->withTrashed()->find($id);
                        } else {
							$this->model = (IntVal($userRole) == 1) ? $this->model->withTrashed()->find($id) : $this->model->find($id);
                        }

                        $this->modelToTimeline = $this->model->replicate();
                    } else {
                        //$input['created_at'] = $input['updated_at'];
                    }

                    if ($success) {
                        $success = $this->model->fill($only)->save();
                    }
                } else {
                    if (!$this->model->validationErrors)
                        $this->model->validationErrors = new MessageBag();

                    $this->model->validationErrors->add('preSaveError', 1);
                }
            } catch (MassAssignmentException $exception) {
                $this->model->validationErrors->add($exception->getMessage(), trans('validation.date', ['attribute' => $exception->getMessage()]));
            }

            if (!$success) {
                $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                DB::rollback();
                //dd('aqui1');
                return $this->responseInvalid(__FUNCTION__);
            } else {
                // Update files
                $this->updateImages();
                $this->updateFiles();
                //$this->rewriteFiles();
                $this->updatePositionsBanners();
                $this->removeCacheKeys($this->model);

                $id = $this->model->id;

                // Extract related values
                $related = $this->getRelatedInput(request()->all());
                $save_related = true;
                // Save Related                    
                foreach ($related as $module => $values) {

                    $relatedClass = 'App\\Models\\' . ucfirst($module);
                    $this->log(__FUNCTION__, 'debug', get_class($this->model) . ' ' . $id . ' related ' . $relatedClass);
                    if (isset($values['0'])) {
                        // Varios registros << Sin testear >>

                        foreach ($values as $key => $one_related) {
                            $class = new $relatedClass();
                            if ($class->fill($one_related)->save()) {
                                $save_related = $save_related && $this->model->$module()->save($class);
                                if (!$save_related) {
                                    $save_related = false;
                                    $this->log(__FUNCTION__, 'error', get_class($this->model) . ' ' . $id . ' related ' . get_class($class));
                                    break 2;
                                }
                            } else {
                                $save_related = false;
                                $this->log(__FUNCTION__, 'debug', 'Invalid input for ' . get_class($class));
                                // Rollback transaccion
                                $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                                DB::rollback();

                                return Redirect::back()->withInput()->withErrors($class->errors);
                            }
                        }
                    } else {

                        // Registro unico
                        $class = new $relatedClass();

                        if (!empty($values[$class->getKeyName()])) {
                            // Update
                            $this->log(__FUNCTION__, 'debug', 'Attempting to update ' . get_class($this->model) . ' ' . $id . ' related ' . get_class($class) . ' ' . $values[$class->getKeyName()]);
                            $class = $class::find($values[$class->getKeyName()]);
                        }
                        unset($values[$class->getKeyName()]);

                        if ($class->fill($values)->save()) {
                            $save_related = $this->model->$module()->save($class);
                            if (!$save_related) {
                                $this->log(__FUNCTION__, 'error', get_class($this->model) . ' ' . $id . ' related ' . get_class($class));
                                break;
                            } else {
                                $this->log(__FUNCTION__, 'debug', 'save ' . get_class($this->model) . ' ' . $id . ' related ' . get_class($class) . ' ' . $class->getKey());
                            }
                        } else {
                            if (!empty($class->validationErrors)) {
                                $errors = $class->validationErrors->toArray();

                                foreach ($errors as $key => $messages) {
                                    foreach ($messages as $message) {
                                        $this->model->validationErrors->add($key, $message);
                                    }
                                }
                            }

                            // Rollback transaccion
                            $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                            DB::rollback();
                            $save_related = false;
                            //dd('aqui3');
                            return $this->responseInvalid(__FUNCTION__, $class->errors);
                        }
                    }
                }
            }

            if ($this->model->relationsCheckboxes) {
                foreach ($this->model->relationsCheckboxes as $table => $config) {
                    $modelName = '\\App\\Models\\' . Stringy::upperCamelize($table);
                    $model = new $modelName();

                    $data = DB::table($table . ' AS t')->leftJoin($config['nm'], function ($join) use ($id, $config) {
                        $join->on($config['nm'] . '.' . $config['otherID'], '=', 't.id');
                        $join->on($config['nm'] . '.' . $config['thisID'], '=', DB::raw($id));
                    });
                    if ($model->fieldsLangs) {
                        $data = $data->leftJoin($table . '_langs AS tl', function ($join) use ($config) {
                            $join->on('tl.' . $config['otherID'], '=', 't.id')
                                ->where('tl.language_id', '=', Language::getMainLanguage()->id);
                        });
                        $data = $data->select('t.*', 'tl.*', $config['nm'] . '.' . $config['thisID']);
                    } else {
                        $data = $data->select('t.*', $config['nm'] . '.' . $config['thisID']);
                    }
                    if ($config['filtered'])
                        $data = $data->where($config['filtered'], $this->model->{$config['filtered']});

                    $data = $data->get();

                    foreach ($fillables as $key => $value) {
                        if (strpos($key, $config['nm']) === 0) {
                            $id_check = intVal(str_replace($config['nm'] . '_', '', $key));
                            if ($value == 1) {
                                if (DB::table($config['nm'])->where($config['thisID'], $id)->where($config['otherID'], $id_check)->count() == 0) {
                                    DB::table($config['nm'])->insert(
                                        [$config['otherID'] => $id_check, $config['thisID'] => $id]
                                    );
                                }
                            } else {
                                DB::table($config['nm'])->where($config['thisID'], $id)->where($config['otherID'], $id_check)->delete();
                            }
                        }
                    }
                }
            }

            if ($save_related && $this->postStore($this->model)) {
                $this->log(__FUNCTION__, 'debug', 'Commit ' . get_class($this->model) . ' ' . $id);
                DB::commit();

                return $this->responseSucess(__FUNCTION__, $id);
            } else {
                // Rollback transaccion
                $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                DB::rollback();
                throw new AppException('POSTUPDATE ERROR in ' . get_class($this) . '->' . __FUNCTION__);
            }
        } catch (Exception $e) {
            $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
            DB::rollback();
            throw $e;

            throw new AppException('POSTUPDATE ERROR in ' . get_class($this) . '->' . __FUNCTION__ . '\n' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        self::log(__FUNCTION__, 'info', $id);

        // Get the items
		$userRole = Auth::user()->user_cms_profile_id;
        $item = (IntVal($userRole) == 1) ? $this->model->withTrashed()->find($id) : $this->model->find($id);

        // Show the page
        return Response::json($item);
    }
    /**
     * Inactivate a item
     * 
     * @param int $id
     * @return Response
     */
    public function inactivate($id)
    {
		$userRole = Auth::user()->user_cms_profile_id;
        $item = (IntVal($userRole) == 1) ? $this->model->withTrashed()->find($id) : $this->model->find($id);
        $saved = 0;

        if ($item->active) {
            $item->active = 0;
            $saved = $item->save();
            if (isset($item->user)) {
                $user = $item->user;
                $user->active = 0;
                $user->save();
            }
        }

        return $this->responseSucess(__FUNCTION__, $id);
    }
    /**
     * activate a item
     * 
     * @param int $id
     * @return Response
     */
    public function activate($id)
    {
		$userRole = Auth::user()->user_cms_profile_id;
        $item = (IntVal($userRole) == 1) ? $this->model->withTrashed()->find($id) : $this->model->find($id);
        $saved = 0;

        if (!$item->active) {
            $item->active = 1;
            $saved = $item->save();
            if (isset($item->user)) {
                $user = $item->user;
                $user->active = 1;
                $user->save();
            }
        }

        return $this->responseSucess(__FUNCTION__, $id);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id = NULL)
    {
        try {
            //$this->authorize(__FUNCTION__ . '_' . $this->model->gEttable(), get_class($this->model));
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        self::log(__FUNCTION__, 'info', $id);

        if (empty($id)) {
            return Redirect::to(Route($this->module . '.create'));
        } else {
			
			$userRole = Auth::user()->user_cms_profile_id;
            if ($this->model->relationship)
                $item = (IntVal($userRole) == 1) ? $this->model::with($this->model->relationship)->withTrashed()->find($id) : $this->model::with($this->model->relationship)->find($id);
            else
                $item = (IntVal($userRole) == 1) ? $this->model->withTrashed()->find($id) : $this->model->find($id);

            $item->getLang();
            $item->getSEO();

            if (!$item) {
                return Redirect::to(Route($this->module . '.create'));
            } else {
                $template = $this->getTemplate(__FUNCTION__);
                $form = array(
                    'method' => 'PUT',
                    'route' => $this->module . '.update',
                    'files' => true,
                    'id' => 'model-edit-form'
                );
                if (request()->ajax()) {

                    // Set the Form params
                    $blade_form_params = array(
                        'method' => (isset($form['method']) ? $form['method'] : 'POST'),
                        'class'  => 'form-horizontal',
                        'route' => empty($item->id) ? array($form['route']) : array($form['route'], 'id' => $item->id),
                        'id' => str_random(64)
                    );
                } else {
                    $blade_form_params = array();
                }

                // get related tags
                if ($this->model->relationsTags) {
                    foreach ($this->model->relationsTags as $key => $value) {
                        $this->getRelatedTags($key, $value, $item->id, $item);
                    }
                }

                // Check if exists upload massive
                $uploadMassive = count($this->model->relationsUploadMassive) > 0 ? $this->model->relationsUploadMassive : false;

                // Show the page
                $view = view($template)
                    ->with('module', $this->module)
                    ->with('item', $item)
                    ->with('relations', $this->model->relationsGrid)
                    ->with('related_field', $this->model->relatedField)
                    ->with('body_classes', 'datatables-page')
                    ->with('upload_massive', $uploadMassive)
                    ->with('form', $form)
                    ->with('blade_form_params', $blade_form_params);

                if ($this->model->relationsCombo) {
                    foreach ($this->model->relationsCombo as $key => $value) {
                        $modelName = '\\App\\Models\\' . Stringy::upperCamelize($key);
                        $model = new $modelName();
                        $varName = Stringy::camelize($key);
                        $select = implode(",", $value) . ',' . $key . '.id as id';
                        $data =  DB::table($key)->selectRaw($select);
                        $displayFields = $value;

                        if ($model->fieldsLangs) {
                            $data = $data->leftJoin($key . '_langs AS tl', function ($join) use ($key) {
                                $join->on('tl.' . $key . '_id', '=', $key . '.id')
                                    ->where('tl.language_id', '=', Language::getMainLanguage()->id);
                            });
                        }
                        $data = $data->get();

                        $select_values = [];
                        foreach ($data as $item) {
                            $values = [];
                            $k = null;
                            foreach ($item as $field => $fieldValue) {
                                if (in_array($field, $displayFields) && $fieldValue) {
                                    $values[] = $fieldValue;
                                } else {
                                    $k = $fieldValue;
                                }
                            }
                            if ($values) {
                                $values = implode(" | ", $values);
                                $select_values[$k] = $values;
                            }
                        }
                        $select_values[null] = trans('global.select-none');
                        ksort($select_values);
                        $view->with($varName, $select_values);
                    }
                }

                // Master Data
                if ($this->model->masterDataFields) {
                    foreach ($this->model->masterDataFields as $field) {
                        $varName = Stringy::camelize($field);
                        $masterDataModel = new MasterData();

                        $masterDataType = DB::table('master_data_type')->select('id')->where('type', $varName)->first();

                        $data = DB::table('master_data AS t')->select('text', 't.id');
                        if ($masterDataModel->fieldsLangs) {
                            $data = $data->leftJoin('master_data_langs AS tl', function ($join) {
                                $join->on('tl.master_data_id', '=', 't.id')
                                    ->where('tl.language_id', '=', Language::getMainLanguage()->id);
                            });
                        }
                        $data = $data->where('master_data_type_id', '=', $masterDataType->id);
                        $data = $data->pluck('text', 'id')
                            ->put('', trans('global.select-none'))
                            ->toArray();
                        ksort($data);

                        $view->with($varName, $data);
                    }
                }

                $view = $this->viewAddEditVariables($view, $item);
                if ($this->model->relationsCheckboxes) {
                    foreach ($this->model->relationsCheckboxes as $table => $config) {
                        $modelName = '\\App\\Models\\' . Stringy::upperCamelize($table);
                        $model = new $modelName();

                        $data = DB::table($table . ' AS t')->leftJoin($config['nm'], function ($join) use ($id, $config) {
                            $join->on($config['nm'] . '.' . $config['otherID'], '=', 't.id');
                            $join->on($config['nm'] . '.' . $config['thisID'], '=', DB::raw($id));
                        });
                        if ($model->fieldsLangs) {
                            $data = $data->leftJoin($table . '_langs AS tl', function ($join) use ($config) {
                                $join->on('tl.' . $config['otherID'], '=', 't.id')
                                    ->where('tl.language_id', '=', Language::getMainLanguage()->id);
                            });
                            $data = $data->select('tl.*', 't.*', $config['nm'] . '.' . $config['thisID']);
                        } else {
                            $data = $data->select('t.*', $config['nm'] . '.' . $config['thisID']);
                        }
                        if ($config['filtered'])
                            $data = $data->where($config['filtered'], $item->{$config['filtered']});

                        $data = $data->get();
                        $view->with($config['var'], $data);
                    }
                }

                if (request()->ajax()) {
                    $view = Response::json(['view' => $view->render()]);
                } else {
                    // To avoid frame vulnerability
                    $view = Response::make($view, 200, array('X-Frame-Options' => 'sameorigin'));
                }
                $this->timeline(__FUNCTION__, get_class($this->model), $id);

                return $view;
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function updateBack($id)
    {

        $this->log(__FUNCTION__, 'info', $id);

        $only = request()->only($this->model->fields);
        $fillables = request()->all();

        $modelClassUses = class_uses($this->model);

        //dd($only);
        // Get the model to update
		$userRole = Auth::user()->user_cms_profile_id;
        if (array_key_exists('Illuminate\Database\Eloquent\SoftDeletes', $modelClassUses)) {
            $this->model = $this->model->withTrashed()->find($id);
        } else {
            $this->model = (IntVal($userRole) == 1) ? $this->model->withTrashed()->find($id) : $this->model->find($id);
        }

        $this->log(__FUNCTION__, 'debug', 'Begin transaction ' . get_class($this->model) . ' ' . $id);
        DB::beginTransaction();
        try {
            $success = false;
            try {
                $success = $this->preUpdate($this->model); //Mejorar el control de excepciones.
                if ($success) {
                    // Sets the authored fields
                    if ($this->model->isAuthoredBehaviour()) {
                        $this->model->updateAuthored();
                    }
                    $rules = $this->model->getRules();
                    //dd($rules);
                    $success = false;
                    // dd($this->model);
                    /*$this->model->fill($fillables);
                    dd($this->model);*/
                    $success = $this->model->validate(request()->all(), $rules);
                    //dd($only);
                    if ($success)
                        $success = $this->model->fill($only)->save();
                } else {
                    if (!$this->model->validationErrors)
                        $this->model->validationErrors = new MessageBag();

                    $this->model->validationErrors->add('preSaveError', 1);
                }
            } catch (MassAssignmentException $exception) {
                $this->model->validationErrors->add($exception->getMessage(), trans('validation.date', ['attribute' => $exception->getMessage()]));
            }

            if (!$success) {

                $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                DB::rollback();
                return $this->responseInvalid(__FUNCTION__);
            } else {
                // Extract related values
                $related = $this->getRelatedInput(request()->all());
                $save_related = true;
                // Save Related
                foreach ($related as $module => $values) {

                    $relatedClass = 'App\\Models\\' . ucfirst($module);
                    $this->log(__FUNCTION__, 'debug', get_class($this->model) . ' ' . $id . ' related ' . $relatedClass);
                    if (isset($values['0'])) {
                        // Varios registros << Sin testear >>

                        foreach ($values as $key => $one_related) {
                            $class = new $relatedClass();
                            if ($class->fill($one_related)->save()) {
                                $save_related = $save_related && $this->model->$module()->save($class);
                                if (!$save_related) {
                                    $save_related = false;
                                    $this->log(__FUNCTION__, 'error', get_class($this->model) . ' ' . $id . ' related ' . get_class($class));
                                    break 2;
                                }
                            } else {
                                $save_related = false;
                                $this->log(__FUNCTION__, 'debug', 'Invalid input for ' . get_class($class));
                                // Rollback transaccion
                                $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                                DB::rollback();
                                return Redirect::back()->withInput()->withErrors($class->errors);
                            }
                        }
                    } else {

                        // Registro unico
                        $class = new $relatedClass();

                        if (!empty($values[$class->getKeyName()])) {
                            // Update
                            $this->log(__FUNCTION__, 'debug', 'Attempting to update ' . get_class($this->model) . ' ' . $id . ' related ' . get_class($class) . ' ' . $values[$class->getKeyName()]);
                            $class = $class::find($values[$class->getKeyName()]);
                        }
                        unset($values[$class->getKeyName()]);

                        if ($class->fill($values)->save()) {
                            $save_related = $this->model->$module()->save($class);
                            if (!$save_related) {
                                $this->log(__FUNCTION__, 'error', get_class($this->model) . ' ' . $id . ' related ' . get_class($class));
                                break;
                            } else {
                                $this->log(__FUNCTION__, 'debug', 'save ' . get_class($this->model) . ' ' . $id . ' related ' . get_class($class) . ' ' . $class->getKey());
                            }
                        } else {
                            if (!empty($class->validationErrors)) {
                                $errors = $class->validationErrors->toArray();

                                foreach ($errors as $key => $messages) {
                                    foreach ($messages as $message) {
                                        $this->model->validationErrors->add($key, $message);
                                    }
                                }
                            }

                            // Rollback transaccion
                            $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                            DB::rollback();
                            $save_related = false;
                            return $this->responseInvalid(__FUNCTION__, $class->errors);
                        }
                    }
                }
            }
            echo '<pre>';
            if ($this->model->relationsCheckboxes) {
                foreach ($this->model->relationsCheckboxes as $table => $config) {
                    $modelName = '\\App\\Models\\' . Stringy::upperCamelize($table);
                    $query =   DB::table($table)->leftJoin($config['nm'], function ($join) use ($id, $table, $config) {
                        $join->on($config['nm'] . '.' . $config['otherID'], '=', $table . '.id');
                        $join->on($config['nm'] . '.' . $config['thisID'], '=', DB::raw($id));
                    })
                        ->select($table . '.*', $config['nm'] . '.' . $config['thisID']);
                    if ($config['filtered'])
                        $query = $query->where($config['filtered'], $item->{$config['filtered']});
                    $data = $query->get();

                    foreach ($fillables as $key => $value) {

                        if (strpos($key, $config['nm']) === 0) {
                            $id_check = intVal(str_replace($config['nm'] . '_', '', $key));
                            if ($value == 1) {
                                if (DB::table($config['nm'])->where($config['thisID'], $id)->where($config['otherID'], $id_check)->count() == 0) {
                                    DB::table($config['nm'])->insert([$config['thisID'] => $id, $config['otherID'] => $id_check]);
                                }
                            } else {
                                DB::table($config['nm'])->where($config['thisID'], $id)->where($config['otherID'], $id_check)->delete();
                            }
                        }
                    }
                }
            }
            if ($save_related && $this->postUpdate($this->model)) {
                $this->log(__FUNCTION__, 'debug', 'Commit ' . get_class($this->model) . ' ' . $id);
                DB::commit();

                return $this->responseSucess(__FUNCTION__);
            } else {
                // Rollback transaccion
                $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
                DB::rollback();
                throw new AppException('POSTUPDATE ERROR in ' . get_class($this) . '->' . __FUNCTION__);
            }
        } catch (Exception $e) {
            $this->log(__FUNCTION__, 'debug', 'Rollback ' . get_class($this->model) . ' ' . $id);
            DB::rollback();
            throw $e;

            throw new AppException('POSTUPDATE ERROR in ' . get_class($this) . '->' . __FUNCTION__ . '\n' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id = null)
    {
        try {
            //$this->authorize(__FUNCTION__ . '_' . $this->model->gEttable(), get_class($this->model));
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        if ($id) {
            if (!$this->model->options['can_delete']) return redirect()->route($this->module . '.index');
        }

        self::log(__FUNCTION__, 'info', $id);

        $this->log(__FUNCTION__, 'debug', 'Begin transaction ' . get_class($this->model) . ' ' . $id);
        DB::beginTransaction();
        try {
			$userRole = Auth::user()->user_cms_profile_id;
            $item = (IntVal($userRole) == 1) ? $this->model->withTrashed()->find($id) : $this->model->find($id);

            $this->modelToTimeline = $item->replicate();

            if ($item->delete()) {
                // Delete relations
                if ($item && $this->model->relationsToDelete) {
                    foreach ($item->relationsToDelete as $relation) {
                        $item->{$relation}()->delete();
                    }
                }
                // Delete Seo Data
                if ($item && $this->model->options['seo']) {
                    $this->destroySeoData($item);
                }

                if ($this->postDestroy($item)) {
                    $this->log(__FUNCTION__, 'debug', 'Commit ' . get_class($item) . ' ' . $id);
                    DB::commit();
                    return $this->responseSucess(__FUNCTION__, $id);
                } else {
                    $this->log(__FUNCTION__, 'debug', 'Rollback transaction ' . get_class($item) . ' ' . $id);
                    DB::rollback();
                    return $this->responseFail(__FUNCTION__);
                }
            } else {
                $data = [
                    'success' => false,
                    'message' => trans('buttons.delete-fail')
                ];
                return Response::make(json_encode($data), 200);
            }
        } catch (Exception $e) {
            $this->log(__FUNCTION__, 'error', $e->getMessage());
            return $this->responseFail(__FUNCTION__);
        }
    }

    /**
     * ====================
     * Trigger functions
     * ====================
     */

    /**
     * Pre Store function
     *
     * @param $model to be inserted.
     *
     * @return boolean
     */
    protected function preStore($model = null)
    {
        $this->log(__FUNCTION__, 'debug');
        return true;
    }

    /**
     * Post Store function
     *
     * @param $model currently inserted.
     *
     * @return boolean
     */
    protected function postStore($model = null)
    {
        if ($model->fieldsLangs) {
            $this->updateLangs($model, $model->fieldsLangs);
        }
        if ($model->options['seo']) {
            $this->updateSEO($model);
        }
        if ($model->relationsTags) {
            foreach ($model->relationsTags as $key => $value) {
                $this->updateRelatedTags($model, $key, $value);
            }
        }
        $this->log(__CLASS__ . '->' . __FUNCTION__, 'debug');

        //dd(DB::getQueryLog());
        return true;
    }

    /**
     * Post Destroy function
     *
     * @param integer $id
     * @return bool
     */
    protected function postDestroy($model = null)
    {
        $this->log(__CLASS__ . '->' . __FUNCTION__, 'debug');
        return true;
    }

    /**
     * ====================
     * Auxiliary functions
     * ====================
     */

    protected function destroySeoData($model = null)
    {

        $now = Carbon::now();

        return SeoData::where('external_id', $model->id)
            ->where('module_id', $this->model->module_id)
            ->update(['deleted_at' => $now->ToDateTimeString()]);
    }

    /**
     * Update the fields at model lang table 
     *
     * @param Model $model
     * @param Array $fields
     */
    protected function updateLangs($model, $fields)
    {


        $fieldsLangs = array();
        foreach ($fields as $fL) {
            foreach (Language::getAvailable() as $lang) {
                $locale = $lang->code;
                $fieldsLangs[] = $fL . '_' . $locale;
            }
        }

        $only = request()->only($fieldsLangs);


        foreach (Language::getAvailable() as $lang) {
            $values = array();
            foreach ($fields as $fL) {
                $fieldLang = $fL . '_' . $lang->code;
                if (isset($only[$fieldLang]) || $only[$fieldLang] === NULL)
                    $values[$fL] = $only[$fieldLang];
            }
            $langData = DB::table($model->getTable() . '_langs')
                ->where($model->getTable() . '_id', $model->id)->where('language_id', $lang->id);
            if ($langData->first()) {
                $langData->update($values);
            } else {
                $values[$model->getTable() . '_id'] = $model->id;
                $values['language_id'] = $lang->id;
                $langData->insert($values);
            }
        }
    }
    /**
     * Remove cache keys that have relation with model
     */
    protected function removeCacheKeys($model)
    {
        if ($model->cacheKeys) {
            foreach ($model->cacheKeys as $key) {
                if (str_contains($key, '{table}')) {
                    $pos = strpos($key, '{table}');
                    $key = substr($key, 0, $pos - 1);
                    $newKey = $key . '_' . $model->getTable() . '_' . $model->id;
                    Cache::forget($newKey);
                    $newKey = $key . '_' . $this->model->getTable() . '_' . $this->model->id;
                    Cache::forget($newKey);
                } elseif (str_contains($key, '{url}')) {
                    $pos = strpos($key, '{url}');
                    $key = substr($key, 0, $pos - 1);
                    $moduleId = $model->module_id;
                    $externalId = $model->id;

                    $seoData = DB::table('seo_data AS sd')
                        ->select('sdl.friendly_url', 'l.code')
                        ->leftJoin('seo_data_langs AS sdl', 'sd.id', 'sdl.seo_data_id')
                        ->leftJoin('language AS l', 'l.id', 'sdl.language_id')
                        ->where('sd.id', $model->id)
                        ->get();

                    foreach ($seoData as $value) {
                        $newKey = $key . '_' . $value->friendly_url . '_' . $value->code;
                        Cache::forget($newKey);
                    }
                } elseif (str_contains($key, '{language}')) {
                    $pos = strpos($key, '{language}');
                    $key = substr($key, 0, $pos - 1);

                    $languages = DB::table('language AS l')
                        ->select('l.code')
                        ->where('l.available', 1)
                        ->get();

                    foreach ($languages as $value) {
                        $newKey = $key . '_' . $value->code;
                        Cache::forget($newKey);
                    }
                } elseif (str_contains($key, '{country}')) {
                    $pos = strpos($key, '{country}');
                    $key = substr($key, 0, $pos - 1);

                    $countries = DB::table('countries AS c')->get();

                    foreach ($countries as $country) {
                        $newKey = $key . '_' . $country->id;
                        Cache::forget($newKey);
                    }
                } else {
                    Cache::forget($key);
                }
            }
        }
    }
    /**
     * Update positions Banners
     */
    protected function updatePositionsBanners()
    {
        if (isset($this->model->hasPositions) && $this->model->hasPositions) {
            $model = $this->model;
            $table = 'posiciones_banners';
            $maxBanners = env('NUM_MAX_BANNER_BY_POSITION', 3);

            $positions = DB::table($table)->get();

            foreach ($positions as $p) {
                $hasThisBanner = false;
                for ($i = 1; $i <= $maxBanners; $i++) {
                    $column = 'banner_' . $i . '_id';
                    if (request()->has($table . '_' . $p->id) && is_null($p->$column) && !$hasThisBanner) {
                        DB::table($table)
                            ->where('id', $p->id)
                            ->update([$column => $model->id]);
                        $hasThisBanner = true;
                    }
                    if (!request()->has($table . '_' . $p->id) && $p->$column == $model->id) {
                        DB::table($table)
                            ->where('id', $p->id)
                            ->update([$column => null]);
                    }
                }
            }
        }
    }
    /**
     * Update model images
     */
    protected function updateImages()
    {
        $model = $this->model;

        $numImages = $model->numImages;
        for ($i = 1; $i <= $numImages; $i++) {
            $imageField = "image$i";
            $fieldImageName = "fieldImageName$i";
            $fieldImageName = isset($model->$fieldImageName) ? $model->$fieldImageName : 'title';
            $this->updateImage($imageField, $fieldImageName);
        }

        $numImages = $model->numButtonsGroups;
        for ($i = 1; $i <= $numImages; $i++) {
            $imageField = 'button' . $i . '_image';
            $this->updateImage($imageField);
        }
        $model->save();
    }
    /**
     * Update model image
     */
    private function updateImage($imageField, $fieldImageName = 'title')
    {
        $model = $this->model;
        $table = $model->getTable();
        $currentImageName = $model->$imageField;
        // Delete the image
        if (request()->input("input-file-$imageField-delete-status")) {
            $model->$imageField = '';
            Storage::disk('myPublic')->delete("uploads/$table/$currentImageName");
        }

        if (request()->hasFile($imageField) && request()->file($imageField)->isValid()) {
            if ($currentImageName && !request()->input("input-file-$imageField-delete-status")) {
                $model->$imageField = '';
                Storage::disk('myPublic')->delete("uploads/$table/$currentImageName");
            }
            // Create directory
            if (!Storage::disk('myPublic')->exists("uploads/$table/"))
                Storage::disk('myPublic')->makeDirectory("uploads/$table/");
            //Store the new image
            $result = DB::table($table . ' AS t');

            if ($model->fieldsLangs && in_array($fieldImageName, $model->fieldsLangs)) {
                $result->leftJoin($table . '_langs AS tl', 't.id', '=', 'tl.' . $table . '_id')
                    ->where([
                        ['t.id', '=', $model->id],
                        ['tl.language_id', '=', Language::getMainLanguage()->id]
                    ])
                    ->select('tl.' . $fieldImageName);
            } else {
                $result->where('t.id', '=', $model->id)
                    ->select('t.' . $fieldImageName);
            }
            $result = $result->first();

            $extension = request()->$imageField->extension();

            if ($result)
                $name = $result->$fieldImageName;
            else
                $name = 'image' . time() . '.' . $extension;

            $name = makeName($name, $table, $extension);
            $imageName = public_path("/uploads/$table/") . $name;
            // Redimensionar imagen
            if (request()->$imageField->storeAs("uploads/$table/", $name, 'myPublic')) {
                if ($extension !== 'gif') {
                    $img = Image::make($imageName);
                    // prevent possible upsizing
                    $img = $img->resize($model->widthImages, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

                    $img = $img->save($imageName, $model->qualityImages);
                }
                $model->$imageField = $name;
                $model->{$imageField . '_url'} = env('APP_URL') . "/uploads/$table/$name";
            }
        }
    }
    /**
     * Update model files
     */
    protected function updateFiles()
    {
        $model = $this->model;
        $numFiles = $model->numFiles;

        for ($i = 1; $i <= $numFiles; $i++) {
            $fileField = "file$i";
            $fieldFileName = "fieldFileName$i";
            $fieldFileName = isset($model->$fieldFileName) ? $model->$fieldFileName : 'title';
            $this->updateFile($fileField, $fieldFileName);
        }

        $numFiles = $model->numButtonsGroups;
        for ($i = 1; $i <= $numFiles; $i++) {
            $fileField = 'button' . $i . '_file';
            $this->updateFile($fileField);
        }

        if ($model->filesLangs) {
            $filesLangs = [];
            foreach ($model->filesLangs as $fL) {
                foreach (Language::getAvailable() as $lang) {
                    $locale = $lang->code;
                    $this->updateFile($fL . '_' . $locale, $fL, $lang);
                }
            }
        }

        $model->save();
    }
    /**
     * Update model file
     */
    private function updateFile($fileField, $fieldFileName = 'title', $field = '', $lang = null)
    {
        $model = $this->model;
        $table = $model->getTable();
        $currentFileName = $model->$fileField;
        //Delete file
        if (request()->input("input-file-$fileField-delete-status") && $currentFileName) {
            $model->$fileField = '';
            Storage::disk('myPublic')->delete("uploads/$currentFileName");
        }
        if (request()->hasFile($fileField) && request()->file($fileField)->isValid()) {
            if ($currentFileName && !request()->input("input-file-$fileField-delete-status")) {
                $model->$fileField = '';
                Storage::disk('myPublic')->delete("uploads/$table/$currentFileName");
            }
            //Store the new image
            $result = DB::table($table . ' AS t');

            if ($model->fieldsLangs) {
                $result->leftJoin($table . '_langs AS tl', 't.id', '=', 'tl.' . $table . '_id')
                    ->where([
                        ['t.id', '=', $model->id],
                        ['tl.language_id', '=', Language::getMainLanguage()->id]
                    ])
                    ->select('tl.' . $fieldFileName);
            } else {
                $result->where('t.id', '=', $model->id)
                    ->select('t.' . $fieldFileName);
            }
            $result = $result->first();

            $name = $result->$fieldFileName;
            $extension = request()->$fileField->extension();
            $name = makeName($name, $table, $extension);
            $fileName = public_path("/uploads/$table/") . $name;

            if (request()->$fileField->storeAs("uploads/$table/", $name, 'myPublic')) {
                if ($model->filesLangs) {
                    $langData = DB::table($model->getTable() . '_langs')
                        ->where($model->getTable() . '_id', $model->id)->where('language_id', $lang->id);

                    $values = [
                        $field => $name
                    ];

                    if ($langData->first()) {
                        $langData->update($values);
                    } else {
                        $values[$model->getTable() . '_id'] = $model->id;
                        $values['language_id'] = $lang->id;
                        $langData->insert($values);
                    }
                } else {
                    $model->$fileField = $name;
                    $model->{$fileField . '_url'} = env('APP_URL') . "/uploads/$table/$name";
                }
            }
        }
    }

    protected function rewriteFiles()
    {
        $model = $this->model;
        $numFiles = $model->numFiles;

        if ($numFiles) {
            dd(DB::table($model->getTable() . '_langs')
                ->select("image2_long_desc")
                ->where([
                    [$model->getTable() . '_id', $model->id],
                    ['language_id', 1]
                ])
                ->first());
            dd(Storage::disk('myPublic')->get("uploads/slid-1-3.txt"));
            for ($i = 1; $i <= $numFiles; $i++) {
                if (request()->has("file$i_rewrite")) {
                    $fileField = "file$i";
                    $currentName = $model->$fileField;
                    $newName = makeName(request()->input("file$i_rewrite"));
                    $model->$fileField = $newName;
                }
            }
        }

        $numFiles = $model->numButtonsGroups;
        for ($i = 1; $i <= $numFiles; $i++) {
            $fileField = 'button' . $i . '_file';
            $currentName = $model->$fileField;
            $newName = makeName(request()->input("button$i_file_rewrite"));
            $model->$fileField = $newName;
        }

        if ($model->filesLangs) {
            $filesLangs = [];
            foreach ($model->filesLangs as $fL) {
                foreach (Language::getAvailable() as $lang) {
                    $locale = $lang->code;
                    $langData = DB::table($model->getTable() . '_langs')
                        ->select("$fL")
                        ->where([
                            [$model->getTable() . '_id', $model->id],
                            ['language_id', $lang->id]
                        ])
                        ->first();
                    $currentName = $langData->$fL;
                    $newName = makeName(request()->input("$fL_$locale_rewrite"));

                    $values = [
                        $fL => $newName
                    ];

                    if ($langData->first()) {
                        $langData->update($values);
                    } else {
                        $values[$model->getTable() . '_id'] = $model->id;
                        $values['language_id'] = $lang->id;
                        $langData->insert($values);
                    }
                }
            }
        }

        $model->save();
    }
    /**
     * Associate a new address
     */
    protected function associateAddress($gm_object, $model)
    {
        $gmObject = json_decode($gm_object);

        $currentAddress = $model->address;

        if (!isset($currentAddress) || $currentAddress->place_id != $gmObject->place_id) {
            $address = Address::where('place_id', $gmObject->place_id)->first();
            if (!$address) {
                $address = Address::create([
                    'place_id' => $gmObject->place_id,
                    'name' => $gmObject->formatted_address,
                    'lat' => $gmObject->geometry->location->lat,
                    'lng' => $gmObject->geometry->location->lng,
                    'gm_object' => $gm_object,
                ]);
            }

            if (array_key_exists('address_components', $gmObject)) {
                $addressComponents = $gmObject->address_components;
                $province = array_where($addressComponents, function ($value, $key) {
                    return in_array('administrative_area_level_2', $value->types);
                });
                $provinceName = array_first($province)->long_name;
                $province = Province::where('name', 'like', '%' . $provinceName . '%')->orWhere('label', 'like', '%' . $provinceName . '%')->first();

                if ($province) {
                    $address->province()->dissociate();
                    $address->province()->associate($province);
                    $address->save();
                }
            }

            $model->address()->dissociate();
            $model->address()->associate($address);
            $model->save();
        }
    }
    /**
     * Update the fields at SEO table 
     *
     * @param Model $model
     */
    protected function updateSEO($model)
    {
        \DB::enableQueryLog();
        $seoModel = new SeoData();

        $seoData = $seoModel->where('external_id', $model->id)->where('module_id', $model->module_id)->first();

        $values = request()->only($seoModel->fields);
        if (!is_null($seoData)) {
            $seoData->update($values);
            $this->updateLangs($seoData, $seoModel->fieldsLangs);
            $this->removeCacheKeys($seoData);
        } else {
            $request = request()->all();
            $values['external_id'] = $model->id;
            $values['module_id'] = $model->module_id;
            $values['method'] = 'index';
            if ($model->module_id == 1)
                $values['view'] = $request['friendly_url_es'];

            $seoModel->fill($values)->save();
            $this->updateLangs($seoModel, $seoModel->fieldsLangs);
        }
    }
    /**
     * Returns the custom template if exists
     *
     * @param unknown $function
     */
    protected function getTemplate($function, $module = null)
    {
        $this->log(__FUNCTION__, 'debug', $function);

        $templates = [];
        $ajaxSufix = '-ajax';

        // Generate the template priority matrix
        //
        if (!empty($this->views[$function])) {
            if (request()->ajax()) {
                $templates[] = $this->views[$function] . $ajaxSufix;
            }
            $templates[] = $this->views[$function];
        }

        if (request()->ajax()) {
            $templates[] = $function . $ajaxSufix;
        }

        $templates[] = $function;
        $module = ($module) ? $module : $this->module;
        $moduleTemplates = array_map(function ($element) use ($module) {
            return $module . '.' . $element;
        }, $templates);
        $templates = array_merge($moduleTemplates, $templates);

        // get the better template
        //dd($templates);
        while ($template = array_shift($templates)) {
            if (view()->exists($template)) {

                break;
            } else {
                $template = null;
            }
        }

        if (!$template) {
            $this->log(__FUNCTION__, 'error', 'Missing template ' . $function . ' in ' . get_class($this));
            app()->abort(500, 'Missing template ' . $function . ' in ' . get_class($this));
        }

        $this->log(__FUNCTION__, 'debug', 'Loading template ' . $template);
        return $template;
    }

    /**
     * Returns the current controller's name
     *
     * @return string
     */
    protected function getName()
    {
        $route = app('request')->route()->getAction();
        $controllerName = class_basename($route['controller']);
        list($controller) = explode('@', $controllerName);

        return $controller;
    }

    /**
     * Returns the related items inputs from the request input
     *
     * @param $input Request
     *            input
     * @return multitype:
     */
    protected function getRelatedInput($input)
    {
        // Extract related values
        $related = array();
        foreach ($input as $key => $value) {
            $rel = explode('_', $key);

            if ($rel[0] == 'relation') {
                // removing related_ prefix
                array_shift($rel);

                $module = array_shift($rel);
                if (!isset($related[$module])) {
                    $related[$module] = array();
                }

                $num = array_shift($rel);
                if (is_numeric($num)) {
                    if (!isset($related[$module][$num])) {
                        $related[$module][$num] = array();
                    }
                    $field = implode('_', $rel);
                    $related[$module][$num][$field] = $value;
                } else {
                    array_unshift($rel, $num);
                    $field = implode('_', $rel);
                    $related[$module][$field] = $value;
                }
            }
        }
        return $related;
    }

    protected function getRelatedTags($tagModule, $tagField, $parentId, &$item)
    {
        $modelName = '\\App\\Models\\' . Stringy::upperCamelize($tagModule);
        $intermediateTable = $tagModule . '_' . $this->module;

        foreach (Language::getAvailable() as $l) {
            $lang = $l->id;
            $langData = $modelName::select($tagField, $tagModule . '.id as id')
                ->join($intermediateTable, $intermediateTable . '.' . $tagModule . '_id', '=', $tagModule . '.id')
                ->where($intermediateTable . '.' . $this->module . '_id', $parentId)
                ->where($tagModule . '.language_id', $lang)
                ->where('active', true)
                ->pluck($tagModule . '.' . $tagField, 'id')
                ->toArray();

            if (count($langData) > 0) {
                $relatedTagsKey = $tagModule . '_' . $lang;

                $item->{$relatedTagsKey} = implode(",", $langData);
            }
        }
    }

    protected function updateRelatedTags($model, $relatedModule, $field)
    {
        $relatedModelName = '\\App\\Models\\' . Stringy::upperCamelize($relatedModule);
        $intermediateTable = $relatedModule . '_' . $this->module;
        $fieldsLangs = array();
        foreach (Language::getAvailable() as $lang) {
            $locale = $lang->code;
            $fieldsLangs[] = $relatedModule . '_' . $locale;
        }
        $only = request()->only($fieldsLangs);
        foreach (Language::getAvailable() as $lang) {
            $locale = $lang->code;
            $values = array();
            $fieldLang = $relatedModule . '_' . $locale;
            $values = explode(",", $only[$fieldLang]);

            // delete removed items
            $this->deleteRelatedTagsRemoved($relatedModule, $field, $values, $locale, $model);

            foreach ($values as $key => $value) {
                if (!empty($value)) {
                    $createRelationItem = true;

                    // check if tag item exist
                    $langData = $relatedModelName::where($field, $value)
                        ->where('lang', $locale)
                        ->first();

                    if ($langData) {
                        $tagId = $langData->id;

                        // check if tag is already related with parent
                        $relationItem = DB::table($intermediateTable)->where($relatedModule . '_id', $tagId)
                            ->where($this->module . '_id', $model->id)
                            ->first();

                        $createRelationItem = (!$relationItem);
                    } else {
                        $tagId = DB::table($relatedModule)->insertGetId(
                            array($field => $value, 'lang' => $locale)
                        );
                    }

                    if ($createRelationItem) {
                        DB::table($intermediateTable)->insert(
                            array($intermediateTable . '.' . $this->module . '_id' => $model->id, $relatedModule . '_id' => $tagId)
                        );
                    }
                }
            }
        }
    }

    protected function deleteRelatedTagsRemoved($relatedModule, $field, $values, $locale, $model)
    {
        $intermediateTable = $relatedModule . '_' . $this->module;

        // get removed tags ids
        $deleteTagsIds = DB::table($relatedModule)->join($intermediateTable, $intermediateTable . '.' . $relatedModule . '_id', '=', $relatedModule . '.id')
            ->where($intermediateTable . '.' . $this->module . '_id', $model->id)
            ->whereNotIn($field, $values)
            ->where('lang', $locale)
            ->pluck($relatedModule . '.id');

        if (count($deleteTagsIds) > 0) {
            DB::table($intermediateTable)->whereIn($relatedModule . '_id', $deleteTagsIds)->where($this->module . '_id', $model->id)->delete();
        }
    }


    /**
     * Save timeline action.
     *
     * @param $action Action
     *            to be stored
     * @param string $object_type
     *            Object type context for the action
     * @param string $object_id
     *            Object id context for the action
     */
    protected function timeline($action, $object_type = null, $object_id = null)
    {
        try {
            $timeline_data = [
                'module'        => $this->module,
                'action'        => $action,
                'object_id'     => $object_id,
                'object_type'   => $object_type,
                'before_model'  => $this->modelToTimeline
            ];
            TimelineService::add($timeline_data);
        } catch (Exception $e) {
            $this->log(__FUNCTION__, 'error', $e->getMessage());
        }
    }

    private function queryWithDate($field, $timestamp)
    {

        $where = '';
        $date = Carbon::createFromFormat('d/m/Y', $timestamp);
        $dateToDB = $date->format('Y-m-d');
        $op = request()->input($field . '_operator');

        switch ($op) {

            case '=':
                $where .= "$field LIKE '%$dateToDB%' AND ";
                break;
            case '!=':
                $where .= "$field NOT LIKE '%$dateToDB%' AND ";
                break;

            case '>':
            case '>=':
                $where .= "$field $op '$dateToDB' AND ";
                break;
            case '<':
            case '<=':
                $where .= "( $field $op '$dateToDB' OR $field IS NULL ) AND ";
                break;

            case '><':

                $max = request()->input($field . '_max');
                if (Helper::isCorrectDate($max)) {

                    $dateMax = Carbon::createFromFormat('d/m/Y', $max);
                    $dateMaxToDB = $dateMax->format('Y-m-d');

                    $where .= "( $field BETWEEN '$dateToDB' AND '$dateMaxToDB' ) AND ";
                }

                break;
        }

        return $where;
    }

    /**
     * Add custom operators <, <=, >, >= , != if needed.
     *
     */
    protected function queryWithOperators()
    {

        $where = '';
        foreach (request()->only($this->model->getFillables()) as $key => $value) {
            if (request()->has($key) && request()->has($key . '_operator')) {

                if (Helper::isCorrectDate($value))
                    $where .= $this->queryWithDate($key, $value);
                //elseif( is a number )


            }
        }

        if ($where != '')
            rtrim($where, ' AND ');

        return $where;
    }



    // ============================================================================
    // Response outputs
    // ============================================================================
    /**
     * Return for Invalid input
     *
     * @param string $function
     * @param $errors Errores
     * @return Response
     */
    protected function responseInvalid($function, $errors = null)
    {
        $this->log($function, 'info', 'Invalid input');

        if (!$errors) {
            $errors = $this->model->validationErrors ?: request()->session()->get('errors');
        }
        if (request()->ajax()) {
            $result = array(
                'success' => false,
                'message' => trans('buttons.' . $function . '-fail'),
                'errors' => $errors ? $errors->getMessages() : array()
            );
            // dd($this->model->errors->getMessages());
            return Response::json($result);
        } else {
            return Redirect::back()->withInput()->withErrors($errors);
        }
    }

    /**
     * Return for Success action
     *
     * @param string $function
     * @return \Illuminate\Http\JsonResponse
     */
    protected function responseSucess($function, $id)
    {
        self::log($function, 'info');

        $this->timeline($function, get_class($this->model), $id);

        if (request()->ajax()) {
            $result = array(
                'success' => true,
                'message' => trans('buttons.' . $function . '-success'),
                'data' => $this->model->toArray()
            );
            return Response::json($result);
        } else {
            switch ($function) {
                case 'store':
                    if (request()->has('save_new')) {
                        $redirect = Redirect::route($this->module . '.create');
                    } elseif (request()->has('apply')) {
                        $redirect = Redirect::route($this->module . '.edit', ['id' => $id]);
                    } else {
                        $redirect = Redirect::route($this->module . '.index');
                    }
                    return $redirect->with('flash_message', trans('buttons.' . $function . '-success'))->with('flash_message_title', trans('global.success'));
                    break;
                default:

                    if (empty($this->urlBack[$function])) {
                        return Redirect::route($this->module . '.index')
                            ->with('flash_message', trans('buttons.' . $function . '-success'))
                            ->with('flash_message_title', trans('global.success'));
                    }

                    return Redirect::route($this->urlBack[$function], array(
                        'id' => $this->model->getkey()
                    ))->with('flash_message', trans('buttons.' . $function . '-success'))->with('flash_message_title', trans('global.success'));
                    break;
            }
        }
    }

    /**
     * Return for Failed action
     *
     * @param string $function
     * @param string $message
     * @param array $errors
     *
     * @throws AppException
     * @return \Illuminate\Http\JsonResponse
     */
    protected function responseFail($function, $message = null, $errors = array())
    {
        if (empty($errors)) {
            if (!is_null($this->model->validationErrors)) {
                $errors = $this->model->validationErrors->getMessages();
            }
        }

        self::log(__FUNCTION__, 'error', 'ERROR in ' . get_class($this) . '->' . $function);
        $result = array(
            'success' => false,
            'message' => ($message) ? $message : trans('buttons.' . $function . '-fail'),
            'errors' => $errors,
        );
        return Response::json($result);
    }

    /**
     *
     */
    protected function viewAddEditVariables(View $view, $item)
    {
        if (!empty(request()->input('deleteOnCancel'))) {
            $view->with('buttonsActions', array(
                'cancel' => array(
                    'name' => $this->module . '.cancel',
                    'route' => route($this->module . '.cancel', ['id' => $item->id])
                )
            ));

            return $view;
        }

        return $view;
    }

    protected function preGrid($items)
    {
        $newItems = array();
        foreach ($items as $key => $item) {
            $columns = array();
            foreach ($item->gridColumns as $key => $column)
                $columns[$column] = $item->$column ? array($item->$key, $item->$column) : null;
            foreach ($item->getAttributes() as $key => $attr)
                if (!in_array($key, $item->gridColumns))
                    $columns[$key] = $attr;
            $newItems[] = $item->newFromBuilder($columns);
        }
        return EloquentCollection::make($newItems);
    }

    protected function filterItems(&$items, $model, $sSearch = false)
    {
        $hasFilter = false;
        $items->where(function ($query) use ($sSearch, $model, &$hasFilter) {
            foreach ($model->gridColumnsToQuery as $method => $operators) {
                foreach ($operators as $operator => $columns) {
                    $clearColumns = array();
                    foreach ($columns as $key => $column) {
                        if (!is_array($column)) {
                            $parts = explode("|", $column);
                            $column = array_shift($parts);
                            $clearColumns[$key] = $column;
                        } else
                            $clearColumns[$key] = $column;
                    }

                    switch ($method) {
                        case 'where':

                            if (!$sSearch)
                                $clearColumns = request()->only($clearColumns);
                            else
                                $clearColumns = array_flip($clearColumns);

                            $count = 0;
                            foreach ($clearColumns as $key => $value) {

                                if ($sSearch) $value = $sSearch;

                                $parts = explode("|", $columns[$count]);
                                $hasFormat = count($parts) > 1 ? $parts[1] : false;
                                $key = array_shift($parts);

                                switch ($hasFormat) {
                                    case 'int':
                                        $value = (int)$value;
                                        break;
                                }
                                if (!empty($value)) {
                                    $hasFilter = true;
                                    switch ($operator) {
                                        case 'like':
                                            $value = "%" . Helper::dbEscapeLike($value, false) . "%";
                                            break;
                                    }
                                    if (request()->has($key . '_operator'))
                                        $operator = request()->input($key . '_operator');

                                    if ($sSearch)
                                        $query->orWhere($key, $operator, $value);
                                    else
                                        $query->where($key, $operator, $value);
                                }
                                ++$count;
                            }
                            break;
                    }
                }
            }
        });

        $gitems = $items->get();



        if (!$sSearch && isset($model->gridColumnsToQuery['relations'])) {
            foreach ($model->gridColumnsToQuery['relations'] as $foreignMethod => $operators) {
                foreach ($gitems as $key => $item) {
                    $foreignItems = $item->$foreignMethod();

                    $hasValue = false;
                    foreach ($operators as $operator => $columns) {
                        foreach (request()->only($columns) as $key => $value) {
                            if (!empty($value)) {
                                $hasFilter = true;
                                $hasValue = true;
                                if (request()->has($key . '_operator'))
                                    $operator = request()->input($key . '_operator');
                                switch ($operator) {
                                    case 'like':
                                        $value = "%" . Helper::dbEscapeLike($value, false) . "%";
                                        break;
                                }
                                $foreignItems = $foreignItems->where($key, $operator, $value);
                            }
                        }
                    }


                    if ($hasValue) {
                        $foreignItems = $foreignItems->first();
                        if (!$foreignItems)
                            $items->where('id', '!=', $item->id);
                    }
                }
            }
        }
    }

    // export to excel
    protected function excelDownload($fieldsHidden = [])
    {
        $title       = trans($this->module . '.excelTitle');
        $title       = ($title == $this->module . '.excelTitle') ? trans($this->module . '.mtitle') : $title;
        $data        = $this->dataDownload($fieldsHidden);

        array_shift($data);

        $exportClass = '\\App\\Exports\\' . Stringy::upperCamelize($this->module) . 'Export';

        return Excel::download(new $exportClass($this->model, $this->module, $data), $title . '.xlsx');
    }

    // download pdf
    protected function pdfDownload($fieldsHidden = [])
    {
        $title = trans($this->module . '.pdfTitle');
        $title = ($title == $this->module . '.pdfTitle') ? trans($this->module . '.mtitle') : $title;

        $data = $this->dataDownload($fieldsHidden);
        $headers = array_shift($data);
        $headers = array_keys($headers);

        $pdf = PDF::loadView('pdf.table', compact('title', 'headers', 'data'))->setPaper('a4', 'landscape');
        return $pdf->download('cms-' . Str::slug($title) . '.pdf');
    }

    protected function dataAddressDownload($fieldsHidden = [])
    {
        if (!$fieldsHidden) $fieldsHidden = [];

        $indexExclude = [];

        $itemsGrid = $this->grid('\App\Presenters\ExcelPdfPresenter')->getData()->aaData;
        $ids = [];
        foreach ($itemsGrid as $key => $ig) {
            $ids[] = $ig[0];
        }

        $tableName = $this->model->getTableName();
        $data = DB::table($tableName)
            ->selectRaw('company_name, CONCAT(' . $tableName . '.address1, ", " , ' . $tableName . '.zip) as full_address, states.name as state_name, cities.name as city_name')
            ->leftJoin('states', 'states.id', $tableName . '.state')
            ->leftJoin('cities', 'cities.id', $tableName . '.city')
            ->whereIn($tableName . '.id', $ids)
            ->get()
            ->toArray();

        // Añadir la cabecera
        $excelFields = ['name', 'full_address', 'city_name', 'state_name'];

        foreach ($excelFields as $key => $column) {
            if (in_array($column, $fieldsHidden)) {
                //var_dump($column);
                $indexExclude[] = $key;
                $heading['aux_' . $column] = 0;
            } else {
                $heading[trans($this->module . '.' . $column)] = trans($this->module . '.' . $column);
            }
        }

        array_unshift($data, $heading);
        // filtrar campos ocultos de los datos
        $dataFinal = [];

        foreach ($data as $key => $value) {
            $dataItem = [];
            $cont = 0;

            foreach ($value as $key2 => $value2) {
                if (is_array($value2)) {
                    unset($value[$key2]);
                } else {
                    if (!in_array($cont, $indexExclude)) {
                        $dataItem[$key2] = $value2;
                    }
                }

                $cont++;
            }

            $dataFinal[] = $dataItem;
        }

        return $dataFinal;
    }

    protected function dataDownload($fieldsHidden = [])
    {
        if (!$fieldsHidden) $fieldsHidden = [];

        $indexExclude = [];
        $data = $this->grid('\App\Presenters\ExcelPdfPresenter')->getData()->aaData;

        // Añadir la cabecera
        $excelFields = array_values(
            array_unique(
                array_merge($this->model->gridColumns, array_keys($this->model->relationsGridColumns ?: []))
            )
        );

        foreach ($excelFields as $key => $column) {
            if (in_array($column, $fieldsHidden)) {
                //var_dump($column);
                $indexExclude[] = $key;
                $heading['aux_' . $column] = 0;
            } else {
                $heading[trans($this->module . '.' . $column)] = trans($this->module . '.' . $column);
            }
        }

        array_unshift($data, $heading);
        // filtrar campos ocultos de los datos
        $dataFinal = [];

        foreach ($data as $key => $value) {
            $dataItem = [];
            $cont = 0;

            foreach ($value as $key2 => $value2) {
                if (is_array($value2)) {
                    unset($value[$key2]);
                } else {
                    if (!in_array($cont, $indexExclude)) {
                        $dataItem[$key2] = $value2;
                    }
                }

                $cont++;
            }

            $dataFinal[] = $dataItem;
        }

        return $dataFinal;
    }

    protected function getCodeNumber($item, $fieldname = 'code')
    {
        $newNumber = null;
        $columns   = DB::getSchemaBuilder()->getColumnListing($this->model->getTableName());
        $saved     = in_array('saved', $columns);

        // si el numero ya esta en uso
        $query = $this->model->whereNull('deleted_at')->where($fieldname, $item->{$fieldname});
        if ($saved) {
            $query->where('saved', true);
        }
        if (!is_null($item->id)) {
            $query->where('id', '<>', $item->id);
        }
        $countNumber = $query->count();

        if ($countNumber != 0) {
            // obtenemos el psiguiente numero sin uso
            $query = $this->model->whereNull('deleted_at');
            if ($saved) {
                $query->where('saved', 1);
            }
            $newNumber = $query->max($fieldname) + 1;

            // actualiza en bbdd
            if ($saved && !is_null($item->id)) {
                $this->model->where('id', $item->id)->update([$fieldname => $newNumber]);
            }

            // actualiza session old
            if (Session::has('_old_input.' . $fieldname)) {
                //Session::set('_old_input.' . $fieldname, $newNumber);
                session(['_old_input.' . $fieldname => $newNumber]);
            }
        }

        return $newNumber;
    }
    protected function isChangeCodeNumber($item, $newNumber, $fieldname = 'code')
    {
        if (!is_null($newNumber) && $item->{$fieldname} != $newNumber) {
            return true;
        }

        return false;
    }

    protected function getRelatedType($type)
    {
        switch ($type) {
            default:
                throw new Exception('Chat - Create - Invalid _type');
                break;
        }

        return $relatedType;
    }

    public function exportAddress()
    {
        $this->excelAddressDownload($this->model->notExcel);
    }

    public function export()
    {
        try {
            //$this->authorize(__FUNCTION__ . '_' . $this->model->gEttable(), get_class($this->model));
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        return $this->excelDownload($this->model->notExcel);
    }

    public function listToPdf()
    {
        try {
            //$this->authorize(__FUNCTION__ . '_' . $this->model->gEttable(), get_class($this->model));
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        return $this->pdfDownload($this->model->notPDF);
    }

    public function uploadFile($file, $path, $id)
    {
        $uniqid = uniqid(rand());
        $this->extension    = $file->getClientOriginalExtension();
        $this->originalName = $file->getClientOriginalName();
        $this->name         = $uniqid;
        $this->size         = $file->getClientSize();
        $storagePath        = AppFile::getPathToSave($path);
        $file->move($storagePath, $this->name);

        return $uniqid;
    }

    public static function getPathToSave($path)
    {
        return storage_path() .
            DIRECTORY_SEPARATOR .
            'app' .
            DIRECTORY_SEPARATOR .
            $path .
            DIRECTORY_SEPARATOR;
    }

    public function getPublicPathToSave($path)
    {
        return public_path() .
            DIRECTORY_SEPARATOR .
            'uploads' .
            DIRECTORY_SEPARATOR .
            $path .
            DIRECTORY_SEPARATOR;
    }

    public function getNameMultiUpload($path, $name, $extension, $number = 1)
    {

        $flag = false;
        $nameFile  = '';

        while (!$flag) {
            if (!$this->checkNameMultiUpload($path, $name, $extension, $number)) {
                $nameFile  = $name . '-' . $number . '.' . $extension;
                $flag = true;
            }

            $number++;
        }

        return $nameFile;
    }

    public function checkNameMultiUpload($path, $name, $extension, $number)
    {

        $extensions = array('jpg', 'jpeg', 'png', 'gif');
        foreach ($extensions as $ext) {
            if (file_exists($path . $name . '-' . $number . '.' . $ext)) {
                return true;
                break;
            }
        }

        return false;
    }

    public function getMailsTo()
    {
        $ids = request()->input('ids');
        $items = $this->model->whereIn('id', $ids)->get();

        $emails = [];
        foreach ($items as $item) {
            if (!empty($item->email) && strpos($item->email, '@') !== false)
                array_push($emails, $item->email);
        }

        $return = implode(',', $emails);
        return Response::json($return);
    }

    public function editBilling($cid = null)
    {
        if (!is_null($cid)) {
            $user = $this->model->where('crm_account_id', $cid)->first();

            if ($user)
                return redirect('cms/user_cms/' . $user->id . '/edit');
        }
    }
}
