<?php

namespace App\Http\Controllers\Cms;


use App\Presenters\QuestionPresenter;

use App\Models\Question;

class QuestionController extends BaseController
{

    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
    public function __construct(Question $model, QuestionPresenter $presenter = null) {
        parent::__construct($model, $presenter);

        $this->module = 'question';
        $this->urlBack = array(
            'store' => '',
            'update' => ''
        );
    }
	
	/**
     * Display contents to bootstrap datatable grid component
     *
     * @return json
     */
    public function grid($excelPdfPresenter = null, $params = null)
    {
        $this->model->setTable('question_view');

        return parent::grid($excelPdfPresenter, $params);
    }
    
}