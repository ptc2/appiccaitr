<?php

namespace App\Http\Controllers\Cms;

use Auth;
use Carbon\Carbon;

use App\Presenters\SurveyPresenter;

use App\Models\Survey;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Arr;

use Stringy\StaticStringy as Stringy;

class SurveyController extends BaseController
{

    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
    public function __construct(Survey $model, SurveyPresenter $presenter = null) {
        parent::__construct($model, $presenter);

        $this->module = 'survey';
        $this->urlBack = array(
            'store' => '',
            'update' => ''
        );
    }
    
	public function grid($excelPdfPresenter = null, $params = null)
    {
        try {
            //$this->authorize(__FUNCTION__ . '_' . $this->model->gEttable(), get_class($this->model));
        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            return Redirect::route('dashboard')
                ->with('flash_message_title', trans('global.' . $e->getMessage()));
        }

        self::log(__FUNCTION__, 'debug');

        $cols = request()->input('columns');
        $tablesrelated = [];
        $relationsColumns = [];
        $relationsManyColumns = [];

        if ($this->model->relationsGridColumns) {
            foreach ($this->model->relationsGridColumns as $alias => $field) {
                $relationsColumns[] =  $alias;
            }
        }

        if ($this->model->relationsManyGridColumns) {
            foreach ($this->model->relationsManyGridColumns as $alias => $field) {
                $relationsManyColumns[] =  $alias;
            }
        }

        $sSkip = request()->input('iDisplayStart', 0);
        $sLimit = request()->input('iDisplayLength', 25);
        $sSearch = request()->input('sSearch');

        $args = func_get_args();

        // Only Simple sort
        $sOrder = request()->input('iSortCol_0');
        $sSortDir = request()->input('sSortDir_0', 'asc');

        $onlyTrashed = request()->input('onlyTrashed', false);

        $model = $this->model;

        $table = $model->getTable();

        // Projection
        if (!empty($cols))
            $cols = array_map('trim', explode(',', $cols));

        $qCols = [];
        $orderCols = [];
        foreach ($cols as $c) {
            if (in_array($c, $model->fieldsLangs) && !in_array($c, $this->model->gridColumnsToHide)) {
                $c = $table . '_langs.' . $c;
                $qCols[] = $c;
                $orderCols[] = $c;
            } elseif (in_array($c, $relationsColumns) && !in_array($c, $this->model->gridColumnsToHide)) {
                $relationsGridColumns = $this->model->relationsGridColumns[$c];

                $fields = explode('_', $c);
                $field = array_pop($fields);
                $alias = implode('_', $fields);
                $modelName = '\\App\\Models\\' . Stringy::upperCamelize($alias);

                if (isset($relationsGridColumns['field'])) {
                    $field = $relationsGridColumns['field'];
                    $t = isset($relationsGridColumns['table']) ? $relationsGridColumns['table'] :  $alias;
                    $alias = isset($relationsGridColumns['alias']) ? $relationsGridColumns['alias'] : $t;
                    $modelName = '\\App\\Models\\' . Stringy::upperCamelize($t);
                }

                $m = new $modelName();
                if (in_array($field, $m->fieldsLangs)) {
                    $alias = $alias . '_langs';
                }

                if (isset($relationsGridColumns['seo']) && $relationsGridColumns['seo']) {
                    $qCols[] = 'seo_data_langs_' . $t . '.' . $relationsGridColumns['field'] . ' AS ' . $c;
                } else {
                    $qCols[] = $alias . '.' . $field . ' AS ' . $c;
                }
                $orderCols[] = $alias . '.' . $field;
            } elseif (in_array($c, $relationsManyColumns)) {
                // dd($this->model->$c);
            } else {
				if(!in_array($c, $this->model->gridColumnsToHide)) {
					$c = $table . '.' . $c;
					$qCols[] = $c;
					$orderCols[] = $c;					
				}
            }
        }

        $cols = $qCols;

        // Match
        $where = '';

        if (!empty($sSearch)) {
            self::log(__FUNCTION__, 'debug', $sSearch);
            $cont = 0;
            foreach ($cols as $field) {
                // searchables
                if (request()->input('bSearchable_' . $cont, false)) {
                    $where .= Helper::dbEscapeAs($field) . ' LIKE \'%' . Helper::dbEscapeLike($sSearch) . '%\' OR ';
                }
                $cont++;
            }
            $where = rtrim($where, " OR ");
        }

        // Match
        $where_and = '';

        if (!empty($args)) {
            foreach ($args as $arg) {
                if (is_array($arg)) {
                    $where_and = isset($arg['where']) ? $arg['where'] . ' AND ' : '';
                }
            }
        }

        // Adding custom search
        $where_and .= $this->queryWithOperators();

        if ($model->isPrivate()) {
            $where_and .= ' user_id = ' . Auth::user()->getScopeKey() . ' AND ';
        }

        $related_filter = $this->relationsFilter();

        if (!empty($related_filter)) {
            $where_and .= '(' . $related_filter . ') AND ';
        }

        // Borrar en un futuro cuando usemos para todos los modelos solo el campo deleted_at.
        /*if (method_exists($model, 'getDeletedAtColumn') && ! $onlyTrashed ){
            $where_and .= $model->getDeletedAtColumn() . ' IS NULL AND ';
        }*/

        $where_and = rtrim($where_and, " AND ");

        $where = empty($where_and) ? $where : $where_and . (empty($where) ? '' : ' AND (' . $where . ')');

        // sort
        $orderCol = $orderCols[$sOrder];

        $items = $model->select(empty($cols) ? '*' : $cols);

        if ($model->fieldsLangs) {
            $items = $items->leftJoin($table . '_langs', function ($join) use ($table) {
                $join->on($table . '_langs.' . $table . '_id', '=', $table . '.id')
                    ->where($table . '_langs.language_id', Language::getMainLanguage()->id);
            });
        }

        if ($this->model->relationsGridColumns) {
            foreach ($this->model->relationsGridColumns as $alias => $value) {
                if (!isset($value['table'])) {
                    $fields = explode('_', $alias);
                    $field = array_pop($fields);
                    $t = implode('_', $fields);
                } else {
                    $t = $value['table'];
                }

                $tAlias = isset($value['alias']) ? $value['alias'] : $t;

                if (isset($value['seo']) && $value['seo']) {

                    $items = $items->leftJoin('seo_data as seo_data_' . $tAlias, function ($q) use ($value, $tAlias) {
                        $q->on('seo_data_' . $tAlias . '.external_id', '=',  $tAlias . '.id');
                        $q->where('seo_data_' . $tAlias . '.module_id', '=', $value['module']);
                    })
                        ->leftJoin('seo_data_langs as seo_data_langs_' . $tAlias, function ($q) use ($tAlias) {
                            $q->on('seo_data_langs_' . $tAlias . '.seo_data_id', '=', 'seo_data_' . $tAlias . '.id');
                            $q->where('seo_data_langs_' . $tAlias . '.language_id', '=', Language::getMainLanguage()->id);
                        });
                }

                if (in_array($tAlias, $tablesrelated)) continue;
                $tablesrelated[] = $tAlias;
                $tLangs = $t . '_langs';
                $tAliasLangs = $tAlias . '_langs';
                $modelName = '\\App\\Models\\' . Stringy::upperCamelize($t);
                $m = new $modelName();

                if (array_key_exists('mul', $value)) {
                    $fkTable = isset($value['mul']['table']) ? $value['mul']['table'] : $table;
                    $fkField = isset($value['mul']['field']) ? $value['mul']['field'] : $t . '_id';
                    $items->leftJoin("$t as $tAlias", $tAlias . '.' . $fkField, $fkTable . '.id');
                } else {
                    $fkTable = isset($value['fk']['table']) ? $value['fk']['table'] : $table;
                    $fkField = isset($value['fk']['field']) ? $value['fk']['field'] : $t . '_id';
                    $items->leftJoin("$t as $tAlias", $tAlias . '.id', $fkTable . '.' . $fkField);
                }

                if ($m->fieldsLangs) {
                    if (!in_array($tAliasLangs, $tablesrelated)) {
                        $tablesrelated[] = $tLangs;
                        //$items = $items->leftJoin("$tLangs as $tAliasLangs", $tAliasLangs.'.'.$t.'_id', '=', $tAlias.'.id');
                        $items->leftJoin("$tLangs as $tAliasLangs", function ($join) use ($t, $tAliasLangs, $tAlias, $tLangs) {
                            $join->on($tAliasLangs . '.' . $t . '_id', '=', $tAlias . '.id')
                                ->where($tAliasLangs . '.language_id', '=', Language::getMainLanguage()->id);
                        });
                    }
                }
            }
        }
        /*
        * Filter
        */
        $arrayWhere = [];
        if ($model->filters) {
            $filters = $model->filters;

            $values = array_filter(request()->only(array_keys($filters)), function ($value) {
                return $value !== '';
            });

            if (!empty($values)) {
                foreach ($filters as $key => $filter) {
                    //$filter = $filters[$key];
                    $relation = Arr::get($filter, 'relation');
                    $field = Arr::get($filter, 'field');
                    $type = Arr::get($filter, 'type') ?: 'text';
                    $operator = request()->input($key . '_operator');
                    $value = Arr::get($values, $key);
                    $valueMax = request()->input($key . '_max');
                    $isMultiple = $type == 'multiple' ? true : false;
                    $isBetween = false;
					$isNotBetween = false;

                    switch ($relation) {
                        case 'fk':
                            $t = $filter['table'];
                            if (array_key_exists('mul', $filter)) {
                                $fkTable = isset($filter['mul']['table']) ? $filter['mul']['table'] : $table;
                                $fkField = isset($filter['mul']['field']) ? $filter['mul']['field'] : $t . '_id';
                                $tc1 = $fkTable . '.id';
                                $tc2 = $t . '.' . $fkField;
                            } else {
                                $fkTable = isset($filter['fk']['table']) ? $filter['fk']['table'] : $table;
                                $fkField = isset($filter['fk']['field']) ? $filter['fk']['field'] : $t . '_id';
                                $tc1 = $fkTable . '.' . $fkField;
                                $tc2 = $t . '.id';
                            }
                            $camelizeModule = Stringy::upperCamelize($t);
                            $class = '\\App\\Models\\' . $camelizeModule;
                            $modelFK = new $class();

                            if (!in_array($t, $tablesrelated)) {
                                $tablesrelated[] = $t;
                                $items = $items->leftJoin($t, $tc1, '=', $tc2);
                            }

                            if ($field) {
                                if (in_array($field, $modelFK->fieldsLangs)) {
                                    $tableLang = $t . '_langs';
                                    if (!in_array($tableLang, $tablesrelated)) {
                                        $tablesrelated[] = $tableLang;
                                        $items = $items->leftJoin($tableLang, $t . '.id', '=', $tableLang . '.' . $t . '_id');
                                    }
                                    $field = $tableLang . '.' . $field;
                                    $arrayWhere[] = [$tableLang . '.language_id', '=', Language::getMainLanguage()->id];
                                } else {
                                    $field = $t . '.' . $field;
                                }
                            }
                            break;
                        case 'nm':
                            $nm = $filter['nm'];
                            $camelizeModule = Stringy::upperCamelize($nm['otherTable']);
                            $class = '\\App\\Models\\' . $camelizeModule;
                            $modelNM = new $class();

                            if (!in_array($nm['tableNM'], $tablesrelated)) {
                                $tablesrelated[] = $nm['tableNM'];
                                $t = array_key_exists('table', $filter) ? $filter['table'] : $table;
                                $items = $items->leftJoin($nm['tableNM'], $t . '.id', '=', $nm['tableNM'] . '.' . $nm['thisID']);
                            }
                            if (!in_array($nm['otherTable'], $tablesrelated)) {
                                $tablesrelated[] = $nm['otherTable'];
                                $items = $items
                                    ->leftJoin($nm['otherTable'], $nm['tableNM'] . '.' . $nm['otherID'], '=', $nm['otherTable'] . '.id');
                            }

                            if ($field) {
                                if (in_array($field, $modelNM->fieldsLangs)) {
                                    $items = $items->leftJoin($nm['otherTable'] . '_langs', $nm['otherTable'] . '.id', '=', $nm['otherTable'] . '_langs.' . $nm['otherTable'] . '_id');
                                    $field = $nm['otherTable'] . '_langs.' . $field;
                                    $arrayWhere[] = [$nm['otherTable'] . '_langs.language_id', '=', Language::getMainLanguage()->id];
                                } else {
                                    $field = $nm['otherTable'] . '.' . $field;
                                }
                            }
                            break;
                        default:
                            if (array_key_exists('table', $filter)) {
                                $t = $filter['table'];
                                $camelizeModule = Stringy::upperCamelize($nm['otherTable']);
                                $class = '\\App\\Models\\' . $camelizeModule;
                                $m = new $class();
                            } else {
                                $t = $table;
                                $m = $model;
                            }

                            if ($field) {
                                if (in_array($field, $m->fieldsLangs)) {
                                    $field = $t . '_langs.' . $field;
                                } else {
                                    $field = $t . '.' . $field;
                                }
                            }
                    }

                    if (!is_null($value)) {
                        switch ($type) {
                            case 'text';
                                $value = trim($value);
                                break;
                            case 'date';
								$date = $value;								
                                $value = \Carbon\Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($date)))->toDateString();
                                $valueMax = $valueMax ? \Carbon\Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($valueMax)))->toDateString() : '';

								if(strpos($field, 'created_at') !== false)
								{
									$format = 'Y-m-d 00:00:00';
									$formatMax = 'Y-m-d 23:59:59';
									
									switch ($operator) {
										case 'equal':
										case '=':
											$value = \Carbon\Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($date)))->format($format);
											$valueMax = \Carbon\Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($date)))->format($formatMax);
											$isBetween = true;
											break;
										case 'nequal':
										case '!=':
											$value = \Carbon\Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($date)))->format($format);
											$valueMax = \Carbon\Carbon::createFromFormat('d/m/Y', date('d/m/Y', strtotime($date)))->format($formatMax);
											$isNotBetween = true;
											break;
									};
								}

                                break;
                        }
						
						switch ($operator) {
							case 'equal':
							case '=':
								$operator = '=';
								break;
							case 'nequal':
							case '!=':
								$operator = '!=';
								break;
							case 'high':
							case '>':
								$operator = '>';
								break;
							case 'less':
							case '<':
								$operator = '<';
								break;
							case 'eqhigh':
							case '>=':
								$operator = '>=';
								break;
							case 'eqless':
							case '<=':
								$operator = '<=';
								break;
							case 'between':
							case '≶':
								$isBetween = true;
								break;
							case 'like':
								$operator = 'like';
								$value = '%' . $value . '%';
								break;
							default:
								$operator = '=';
								break;
						};

                        if ($isBetween) {
                            $items = $items->whereBetween($field, [$value, $valueMax]);
						} elseif ($isNotBetween) {
                            $items = $items->whereNotBetween($field, [$value, $valueMax]);
                        } elseif ($isMultiple) {
                            if (!is_array($value)) {
                                $value = [$value];
                            }
                            $items = $items->whereIn($field, $value);
                        } else {
                            $arrayWhere[] =  [$field, $operator, $value];
                        }
                    }
                }
            }
        }

        $items = $items->whereRaw('(' . (empty($where) ? '1=1' : $where) . ')');

        if ($model->sortDefaultColumn && $model->sortDefaultDir && $sOrder == 0) {
            $items = $items->orderBy($table . '.' . $model->sortDefaultColumn, $model->sortDefaultDir);
        } else {
            $items = $items->orderBy($orderCol, $sSortDir);
        }

        if ($onlyTrashed)
            $items = $items->onlyTrashed();

        $items = $items->where($arrayWhere);

        $items->groupBy($table . '.id');

        $auxItems = clone $items;
        $auxItems = $auxItems->get();

        $filtered = $auxItems->count();

        if ($sLimit > 0 && is_null($excelPdfPresenter)) {
            $items->skip($sSkip)->take($sLimit);
        }

        if (count($relationsManyColumns) > 0) {
            $items->with($relationsManyColumns);
        }

		$userRole = Auth::user()->user_cms_profile_id;
        $items = (IntVal($userRole) == 1) ? $items->withTrashed()->get() : $items->get();

        // Debug query
        if (Config::get('app.debug')) {
            $queries = DB::getQueryLog();
            $last_query = end($queries);
            $this->log(__FUNCTION__, 'debug', $last_query['query']);
        }

        if ($model->isNotUsingDBView()) {
            // Presenters
            if ($excelPdfPresenter) {
                $presenter = new $excelPdfPresenter();
                $items = array_map('array_values', $presenter->collection($items, $presenter)->toArray());
            } elseif ($this->presenter) {
                $items = array_map('array_values', $this->presenter->collection($items, $this->presenter)->toArray());
            } else {
                $items = array_map('array_values', $items->toArray());
            }
        } else {

            foreach ($items as $key => $i) {
                $items[$key] = (array) $i;
            }

            $items = array_map('array_values', $items);
        }

        $result = array(
            'sEcho'                 => intval(request()->input('sEcho')),
            'iTotalRecords'         => isset($total) ? $total : $filtered,
            'iTotalDisplayRecords'  => $filtered,
            'aaData'                => $items,
            'types'                 => array(),
            'items'                 => $auxItems

        );
        return Response::json($result);
    }
	
}