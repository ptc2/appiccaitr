<?php

namespace App\Http\Controllers\Cms;


use App\Presenters\KinshipPresenter;

use App\Models\Kinship;

class KinshipController extends BaseController
{

	/**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
	public function __construct(Kinship $model, KinshipPresenter $presenter = null) {
		parent::__construct($model, $presenter);

		$this->module = 'kinship';
		$this->urlBack = array(
            'store' => '',
            'update' => ''
        );
	}
	
}