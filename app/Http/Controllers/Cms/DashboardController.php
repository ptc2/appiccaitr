<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Helpers\Application;
use App\Models\Module;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Statistic as StatisticModel;

use Statistic;

use Auth;

class DashboardController extends Controller
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     */
    public function __construct()
    {
        Application::setCMS();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$userRole = Auth::user()->user_cms_profile_id;
        $items = [];
		if(IntVal($userRole) != 2) {			
			$modules = Module::where([
				['active', '=', 1],
				['is_dashboard', '=', 1]
			])
				->orderBy('data_order', 'asc')
				->get();
				
			foreach ($modules as $module) {
				$count = DB::table($module->table_name)->whereNull('deleted_at')->count();
				$items[] = [
					'title' => $module->description,
					'icon' => $module->type_icon . ' ' . $module->icon,
					'count' => $count,
					'slug' => $module->slug
				];
			}
			
			return view('dashboard')->with('items', $items)->with('module', 'dashboard');
		} else {
			$user = User::find(Auth::user()->related_user_id);

			$results = Statistic::getStatisticForAGivenUser($user->id);
			$surveys = StatisticModel::select('survey_id', 'socio_data_id')->where('id', $user->id)->get()->toArray();
			$totalSurveys = count($surveys);
			$totalMembers = $user->userData->members;
			$percentageSurveyAnswered = (float) number_format((($totalSurveys*100) / $totalMembers), 2);
			
			$info = Statistic::getTotalsAndPercentagesInfo($surveys, $totalMembers);
			$totals = $info['totals'];
			$percentages = $info['percentages'];
			
			return view('dashboard-center')->with('centerId', $user->id)->with('user', $user)->with('module', 'statistic')->with('results', $results)->with('percentageSurveyAnswered', $percentageSurveyAnswered)
				->with('totalSurveys', $totalSurveys)->with('totals', $totals)->with('percentages', $percentages);
		}
    }
}
