<?php

namespace App\Http\Controllers\Cms;


use App\Presenters\StudiesPresenter;

use App\Models\Studies;

class StudiesController extends BaseController
{

	/**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param BaseModel $model
     * @param Presentable $presenter
     */
	public function __construct(Studies $model, StudiesPresenter $presenter = null) {
		parent::__construct($model, $presenter);

		$this->module = 'studies';
		$this->urlBack = array(
            'store' => '',
            'update' => ''
        );
	}
	
}