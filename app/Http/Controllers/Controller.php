<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Enables the log
     *
     * @var bool
     */
    protected $log = true;

    /**
     * Module name
     *
     * @var string
     */
    protected $module;

    /**
     * Logs the function callback
     *
     * @param string $function
     * @param callable $callback
     * @param string $message
     * @throws \Exception
     * @return void
     */
    protected function log($function, $callback = 'info', $message = NULL)
    {
        if ($this->log && (\Config::get('app.debug') || $callback != 'debug')) {
            $uri = \Request::path();
            $method = \Request::getMethod();
            $clientIp = \Request::getClientIp();
            $controller = get_class($this);
            \Log::$callback($clientIp . ' | ' . $method . ' | ' . $uri . ' | ' . $this->module . ' | ' . get_class($this) . '->' . $function . ' | ' . (empty($message) ? '' : $message));
        }
    }
    protected function redirect()
    {
        die(@header("Location: " . $this->module));
    }
}
