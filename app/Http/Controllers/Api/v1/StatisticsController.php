<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Statistics;

class StatisticsController extends BaseController
{
    public function __construct(Statistics $model)
    {
        parent::__construct($model);
    }
}
