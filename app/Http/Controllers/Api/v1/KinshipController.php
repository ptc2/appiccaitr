<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Kinship;

class KinshipController extends BaseController
{
    public function __construct(Kinship $model)
    {
        parent::__construct($model);
    }
}
