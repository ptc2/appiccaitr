<?php

namespace App\Http\Controllers\Api\v1;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;

use Stringy\StaticStringy as Stringy;

use App\Http\Controllers\Controller;

use App\Models\Language;
use App\Models\User;
use App\Models\Sector;
use App\Models\EventState;
use App\Models\FileType;
use App\Models\UserData;

class BaseController extends Controller
{
    /**
     * The main model of the controller
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    protected $sortColumns;
    /**
     * Name for access tokens
     */
    protected $accessTokenName = 'PersonalAccessToken';

    protected $diskPublic = 'public';
    protected $diskPrivate = 'local';

    public function __construct(Model $model = null)
    {
        if ($model) {
            $this->model = new $model();
            $this->sortColumns = array_merge(
                DB::getSchemaBuilder()->getColumnListing($this->model->getTable()),
                array_keys($this->model->relationsApi)
            );
        }
    }
    /**
     * Return all items
     *
     * @param Request $request
     * @return Response object
     */
    public function index(Request $request)
    {

        $table = $this->model->getTable();
        $objects = DB::table($table);
        $objects = $this->formatData($request, $objects, true);
        
        if ($request->has('page') && $request->has('per_page')) {
            $limit = $request->per_page;
            $objects = $objects->paginate($limit);
            $collection = $objects->getCollection();
            $paginated = true;
        } else {
            $objects = $objects->get();
            

            $collection = $objects;
            $paginated = false;
        }


        $collection->transform(function ($object) {
            $object = $this->dateToIso8601($object);
            return $object;
        });

        $objects = $paginated ? $objects->setCollection($collection) : $collection;

        return response()->json($objects);
    }
    /**
     * Store a item
     *
     * @params Requet $request
     * @return Response object
     */
    public function store(Request $request)
    {
        $input = $request->only($this->model->fields);
        $object = $this->model::create($input);

        return response()->json($object, 201);
    }
    /**
     * Return a item
     *
     * @params Requet $request
     * @return Response object
     */
    public function show(Request $request, $id)
    {
        $table = $this->model->getTable();
        $object = DB::table($table);
        $object = $this->formatData($request, $object, false);

        $object = $object->where($table . '.id', $id)->first();
        $object = $this->dateToIso8601($object);

        return response()->json($object);
    }
    /**
     * Update a item
     *
     * @params Requet $request
     * @return Response object
     */
    public function update(Request $request, $id)
    {
        $input = $request->only($this->model->fields);
        $object = $this->model::where('id', $id)->update($input);
        $object = $this->model::find($id);

        return response()->json($object);
    }
    /**
     * Make filters and left joins
     *
     * @params Requet $request
     * @return Response object
     */
    protected function formatData(Request $request, Builder $object, $withFilter = false, $table = '')
    {
        $get = array_filter($request->all(), function ($value) {
            return !is_null($value);
        });

        $table = $table ?: $this->model->getTable();
        $tablesrelated = [];
        $aliasRelations = [];
        $sortColumns = [];
        $columns = [];
        $select = [];
        if ($this->model->columnsApi) {
            foreach ($this->model->columnsApi as $key => $value) {
                $field = $table . '.' . $value;
                $columns[] = $field;
                $select[] = $field;
            }
        } else {
            foreach (DB::getSchemaBuilder()->getColumnListing($table) as $key => $value) {
                $field = $table . '.' . $value;
                $columns[] = $field;
            }
            $select[] = $table . '.*';
        }
        if ($this->model->relationsApi) {
            foreach ($this->model->relationsApi as $key => $value) {
                $relationTable = $value['table'];
                $relationFields = array_key_exists('field', $value) ? $value['field'] : null;
                if ($relationFields) {
                    $field = $relationTable . '.' . $relationFields;
                    $columns[] = $field;
                    $field = $field . ' AS ' . $key;
                    $aliasRelations[] = $key;
                    $select[] = $field;
                }/*else{
                    $select[] = $relationTable . '.*';
                }*/
            }
        }

        if ($columns && $withFilter) array_push($select, get_concat($columns));
        if ($select) {
            $object->selectRaw(
                implode(",", $select)
            );
        }

        if ($this->model->relationsApi) {
            foreach ($this->model->relationsApi as $alias => $value) {
                if (!isset($value['table'])) continue;
                $t = $value['table'];
                $tAlias = isset($value['alias']) ? $value['alias'] : $value['table'];
                if (in_array($tAlias, $tablesrelated)) continue;
                $tablesrelated[] = $tAlias;
                $tLangs = $t . '_langs';
                $tAliasLangs = $tAlias . '_langs';

                if (array_key_exists('mul', $value)) {
                    $fkTable = isset($value['mul']['table']) ? $value['mul']['table'] : $table;
                    $fkField = isset($value['mul']['field']) ? $value['mul']['field'] : $t . '_id';
                    $object->leftJoin("$t as $tAlias", $tAlias . '.' . $fkField, $fkTable . '.id');
                } else {
                    $fkTable = isset($value['fk']['table']) ? $value['fk']['table'] : $table;
                    $fkField = isset($value['fk']['field']) ? $value['fk']['field'] : $t . '_id';
                    $object->leftJoin("$t as $tAlias", $tAlias . '.id', $fkTable . '.' . $fkField);
                }
                if (class_exists('\\App\\Models\\' . Stringy::upperCamelize($t))) {
                    $modelName = '\\App\\Models\\' . Stringy::upperCamelize($t);
                    $m = new $modelName();
                    if ($m->fieldsLangs) {
                        if (!in_array($tAliasLangs, $tablesrelated)) {
                            $tablesrelated[] = $tLangs;
                            $object->leftJoin("$tLangs as $tAliasLangs", function ($join) use ($t, $tAliasLangs, $tAlias, $tLangs) {
                                $join->on($tAliasLangs . '.' . $t . '_id', '=', $tAlias . '.id')
                                    ->where($tAliasLangs . '.language_id', '=', Language::getMainLanguage()->id);
                            });
                        }
                    }
                }
            }
        }
        /*foreach($tablesrelated as $t) {
            $object->whereNull("$t.deleted_at");
        }*/
        $arrayWhere = [];
        $arrayWhereIn = [];
        $filterActive = [$table . '.active', '=', 1];

        if ($get && $withFilter) {
            $filtersModel = $this->model->relationsApi;
            foreach ($get as $key => $value) {
                switch ($key) {
                    case 'sort':
                        $fields = explode(",", $value);
                        $fields = array_filter($fields, function ($value) {
                            return strpos($value, 'comparative') == false;
                        });
                        foreach ($fields as $f) {
                            $pos = strrpos($f, '_');
                            $field = substr($f, 0, $pos);
                            $ord = substr($f, $pos + 1, strlen($f));
                            if ($this->canSortBy($this->sortColumns, $field)) $object->orderBy($field, strtoupper($ord));
                        }
                        break;
                    case 'search':
                        //$object->having('data', 'like', "%$value%");
                        $d = str_replace(' as data', '', get_concat($columns));
                        $value = str_replace(' ', '', $value);
                        $object->whereRaw("$d like '%$value%'");
                        break;
                    case 'per_page':
                        break;
                    case 'page':
                        break;
                    default:
                        $pos = strrpos($key, '_');
                        $field = substr($key, 0, $pos);
                        $operator = substr($key, $pos + 1, strlen($key));
                        switch ($operator) {
                            case 'equal':
                                $operator = '=';
                                break;
                            case 'nequal':
                                $operator = '!=';
                                break;
                            case 'high':
                                $operator = '>';
                                break;
                            case 'less':
                                $operator = '<';
                                break;
                            case 'eqhigh':
                                $operator = '>=';
                                break;
                            case 'eqless':
                                $operator = '<=';
                                break;
                            case 'between':
                                $isBetween = true;
                                break;
                            case 'like':
                                $operator = 'like';
                                $value = '%' . $value . '%';
                                break;
                            case 'multiple':
                                $value = explode(",", $value);
                                break;
                        };
                        $filter = [];
                        if (array_key_exists($field, $filtersModel)) {
                            $filter = $filtersModel[$field];
                        } elseif ($this->checkFieldInTable($table, $field)) {
                            $filter = [
                                'field' => $field,
                                'table' => $table
                            ];
                        }
                        if ($filter) {
                            if ($operator == 'multiple') {
                                $arrayWhereIn[] = [
                                    'field' => $filter['table'] . '.' . $filter['field'],
                                    'value' => $value
                                ];
                            } else {
                                if ($filter['field'] == 'active') {
                                    $filterActive = [$filter['table'] . '.' . $filter['field'], $operator, $value];
                                } else {
                                    $arrayWhere[] = [$filter['table'] . '.' . $filter['field'], $operator, $value];
                                }
                            }
                        }
                        break;
                }
            }
        }

        if ($this->checkFieldInTable($table, 'active')) $arrayWhere[] = $filterActive;

        if ($arrayWhere) {
            $object->where($arrayWhere);
        }

        $object->whereNull($table . '.deleted_at');

        if ($arrayWhereIn) {
            foreach ($arrayWhereIn as $wIn) {
                $value = $wIn['value'];
                $object->whereIn($wIn['field'], $value);
            }
        }

        return $object;
    }
    /**
     * Generate pagination for request
     */
    protected function generatePagination($objects)
    {
        if (request()->has('page')) {
            $limit = request()->per_page;
            $objects = $objects->paginate($limit);
            $collection = $objects->getCollection();
            $paginated = true;
        } else {
            $objects = $objects->get();
            $collection = $objects;
            $paginated = false;
        }

        $objects = $paginated ? $objects->setCollection($collection) : $collection;

        return $objects;
    }
    /**
     * Check if a field exists in columns of data base table
     *
     * @params string $field
     * @params string $table
     * @return Builder object
     */
    private function checkFieldInTable($table, $field)
    {
        $fields = DB::getSchemaBuilder()->getColumnListing($table);

        return in_array($field, $fields);
    }
    /**
     * Check if a field exists in columns to sort by
     *
     * @params Array $fields
     * @params string $field
     * @return Builder object
     */
    private function canSortBy($fields, $field)
    {
        return in_array($field, $fields);
    }
    /**
     * Validate data
     * @param Request $request
     * @param Array $rules
     * @return Array
     */
    public function validator(Request $request, $rules)
    {
        $validator = Validator::make($request->all(), $rules);
        $errors = [];

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errors = [
                'message' => implode(".\n", $errors->all())
            ];
        }

        return $errors;
    }
    /**
     * Create access token for API login
     * 
     * @param User $user
     * @return Array 
     */
    public function createAccessToken(User $user)
    {
        /*$tokenResult = $user->createToken($this->accessTokenName);
        $token = $tokenResult->token;
        $accessToken = $tokenResult->accessToken;
        $token->save();*/
        $token = $user->createToken($this->accessTokenName . $user->username);

        return [
            'access_token' => $token->plainTextToken,
            'type' => 'Bearer'
        ];
    }
    /**
     * Convert date to ISO 8601 format
     */
    protected function dateToIso8601($collection)
    {
        $dates = $this->model->datesToIso8601;

        foreach ($collection as $key => $value) {
            if ($dates && in_array($key, $dates)) {
                $collection->{$key} = to_iso_8601_string($value);
            }
        }
        return $collection;
    }
    /**
     * Return count of unreaded messages interest
     * @param Request $request
     * @return Reponse object
     */
    protected function countUnreadedMessages($table, $where = [])
    {
        $user = request()->user();

        $object = DB::table($table)
            ->where([
                ['receiver_id', $user->id],
                ['readed', 0]
            ]);

        if ($where) {
            $object = $object->where($where);
        }

        $count = $object->count();

        return response()->json([
            'count' => $count
        ]);
    }
    /**
     * Associate new sector
     * 
     * @param Int $sectorId
     * @param Model $model
     */
    protected function associateSector($sectorId, Model $model)
    {
        $sector = $model->sector;

        if (!isset($sector) || $sector->id !== $sectorId) {
            $newSector = Sector::find($sectorId);

            if ($newSector) {
                $model->sector()->dissociate();
                $model->sector()->associate($newSector);
                $model->save();
            }
        }
    }
    /**
     * Associate new event state
     * 
     * @param Int $sectorId
     * @param Model $model
     */
    protected function associateEventState($stateId, Model $model)
    {
        $state = $model->state;

        if (!isset($state) || $state->id !== $stateId) {
            $newState = EventState::find($stateId);

            if ($newState) {
                $model->state()->dissociate();
                $model->state()->associate($newState);
                $model->save();
            }
        }
    }
    /**
     * Associate new file type
     * 
     * @param Int $sectorId
     * @param Model $model
     */
    protected function associateFileType($typeId, Model $model)
    {
        $type = $model->type;

        if (!isset($type) || $type->id !== $typeId) {
            $newType = FileType::find($typeId);

            if ($newType) {
                $model->type()->dissociate();
                $model->type()->associate($newType);
                $model->save();
            }
        }
    }
    /**
     * Associate new user data
     * 
     * @param Int $sectorId
     * @param Model $model
     */
    protected function associateUserData($userDataId, Model $model)
    {
        $userData = $model->userData;

        if (!isset($userData) || $userData->id !== $userDataId) {
            $newUserData = UserData::find($userDataId);

            if ($newUserData) {
                $model->userData()->dissociate();
                $model->userData()->associate($newUserData);
                $model->save();
            }
        }
    }
}
