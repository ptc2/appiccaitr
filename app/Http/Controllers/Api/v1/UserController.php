<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\PersonalAccessToken;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

use App\Models\User;
use App\Models\UserData;

use App\Helpers\Helper;
use App\Helpers\FilesTypes;

class UserController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
    /**
     * Get user by access token
     */
    private function getUserByAccessToken()
    {
        $user = null;

        if (request()->header('Authorization'))
            $user = User::find(
                PersonalAccessToken::findToken(
                    Str::replaceFirst('Bearer ', '', request()->header('Authorization'))
                )->tokenable_id
            );

        return $user;
    }
    /**
     * Return users list
     *
     * @param Request $request
     * @return Response object
     */
    public function index(Request $request)
    {
        $user = $this->getUserByAccessToken();

        $objects = Cache::remember('users', env('CACHE_TIMEOUT'), function () {
            $objects = User::with([
                'userData.files',
                'company.sector'
            ])
                ->where('active', 1)
                ->get();

            return $objects->map(function ($item) {
                $name = isset($item->userData->name) ? $item->userData->name : '';
                $company = isset($item->company) ? $item->company->name . $item->company->services : '';
                $sector = isset($item->company->sector) ? $item->company->sector->label : '';
                $item->data = strtolower(str_replace(' ', '', $name . $company . $sector));

                return $item;
            })->sortBy(function ($item) {
                return Str::slug($item->userData->name, ' ');
            });
        });


        if ($user) {
            $objects = $objects->filter(function ($value) use ($user) {
                return $value->id !== $user->id;
            });
        }
        if ($request->has('search')) {
            $objects = $objects->filter(function ($value) use ($request) {
                return Str::contains($value->data, strtolower($request->search));
            });
        }

        $objects = $objects->values();

        if ($request->has('page') && $request->has('per_page')) {
            $limit = $request->per_page;
            $objects = $objects->paginate($limit);
        }

        return response()->json($objects);
    }
    /**
     * Show a user
     * 
     * @param Request $request
     * @param Int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
       // $user = User::with([//'userData.files',
       //  'company.sector'
       // ])
        $user = User::find($id);

        if (!$user) {
            return response()->json([
                'message' => trans('user.not_found')
            ], 404);
        }
        $user->user_data = json_decode($user->user_data);
        return response()->json($user);
    }
    /**
     * Show authenticated user
     * 
     * @param Request $request
     * @return Response
     */
    public function showAuthenticatedUser(Request $request)
    {
        $id = $request->user()->id;

        return $this->show($request, $id);
    }
    /**
     * Update authenticated user
     * 
     * @param Request $request
     * @return Response
     */
    public function updateAuthenticatedUser(Request $request)
    {
        $result = DB::transaction(function () use ($request) {
            $user = $request->user();
            $data = $request->all();

            $userData = $user->userData;

            $userDataRules = $userData->getRulesApi();
            
            $userDataRules['email'] = 'required|unique:user_data,email,' . $userData->id . ',id';

            //$errors = $this->validator($request,$userDataRules,);
            //if ($errors) { return response()->json($errors, 422); }
            if($this->validator($request,$userDataRules))
                return response()->json($errors, 422);

            $userData->update($data);

            return $this->show($request, $user->id);
        });

        return $result;
    }

    public function store(Request $req){
        $result = DB::transaction(function () use ($req) {
            if ($this->validator($req, $this->model->getRulesApi()))
                return response()->json($errors, 422);

            $data = $req->all();
            
            if(UserData::where('name',$data["name"])->orWhere('email',$data["email"])->first())
                return response('Ese usuario ya existe.',200);

            $code = Helper::createCode();

            $i = 0;
            $same = true;
            while($same && $i < 50){
                if(User::where('username', $code)->first())
                    $code = Helper::createCode();
                else
                    $same = false;
                $i++;
            }

            if($i == 50)
                return response('',508);

            $user = User::create([
                "username"=>$code,
                "active"=>1,
            ]);
            
            $user->password = $user->username;
            $user->save();

            $data["name"] = $req->name;
            $userData = UserData::create($data);

            $userData->user_id = $user->id;
            $userData->save();

            $this->associateUserData($userData->id, $user);
            $user->user_data = json_encode($userData);
            $user->save();

            return response()->json($user);
        });
        return $result;
    }
}
