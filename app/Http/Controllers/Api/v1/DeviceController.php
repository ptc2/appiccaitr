<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Models\Device as DeviceModel;
use Carbon\Carbon;
use Device;

class DeviceController extends BaseController
{
    /**
     * Creates a new instance of the controller with a new instance of the model
     *
     * @param Device $model
     */
    public function __construct(DeviceModel $model)
    {
        parent::__construct($model);
    }
    /**
     * Store a new device
     * @param Request $request
     * @return Response $object
     */
    public function store(Request $request)
    {
        $data = $this->getArrayData($request);

        $device = Device::storeDevice($data);
        $deviceOnesignal = $device->deviceOneSignals()->orderBy('created_at', 'desc')->first();
        $deviceOnesignal->body = json_encode($data);
        $deviceOnesignal->save();

        return response()->json($device);
    }
    /**
     * Update a device
     * @param Request $request
     * @param Int $id
     * @return Response $object
     */
    public function update(Request $request, $uuid)
    {
        $data = $this->getArrayData($request);

        $device = Device::updateDevice($data, $uuid);
        $deviceOnesignal = $device->deviceOneSignals()->orderBy('created_at', 'desc')->first();
        $deviceOnesignal->body = json_encode($data);
        $deviceOnesignal->save();

        return response()->json($device);
    }
    /**
     * Return formatted data from request
     * 
     * @param Request $request
     * @return Array object
     */
    private function getArrayData(Request $request)
    {
        $data = [
            'platform_id' => $request->header('Platform-Device'),
            'app_version' => $request->header('Version-App'),
            'os_version' => $request->header('Version-Device'),
            'model_device' => $request->header('Model-Device'),
            'language' => $request->header('Accept-Language'),
            'uuid' => $request->uuid,
            'push_token' => $request->push_token,
            'last_activity' => Carbon::now()->format('Y-m-d H:i:s'),
            'unsubscribe' => false
        ];

        return $data;
    }
}
