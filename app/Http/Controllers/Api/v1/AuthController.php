<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class AuthController extends BaseController
{
    /**
     * Login a user
     * 
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        $errors = $this->validator(
            $request,
            [
                'username' => 'required',
                //'password' => 'required',
            ]
        );

        if ($errors) { return response()->json($errors, 422); }

        $user = User::where('username', $request->username)->first();


        if (!$user || !Hash::check($request->username, $user->password)) {
            return response()->json([
                'message' => trans('auth.unauthorized')
            ], 401);
        }

        if (!$user->active) { return response()->json([ 'message' => trans('auth.inactive') ], 401); }

        //if (!$user || !Hash::check($request->password, $user->password)) {
        //    return response()->json([
        //        'message' => trans('auth.unauthorized')
        //    ], 401);
        //}

        $token = $this->createAccessToken($user);
        $user->access_token = $token['access_token'];
        $user->token_type = $token['type'];
        $user->userData;

        return response()->json($user);
    }
    /**
     * Logout a user
     * 
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'isAuthenticated' => false
        ]);
    }
}
