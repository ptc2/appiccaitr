<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Studies;

class StudiesController extends BaseController
{
    public function __construct(Studies $model)
    {
        parent::__construct($model);
    }
}
