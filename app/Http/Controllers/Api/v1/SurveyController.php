<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Survey;
use App\Models\SocioData;
use App\Models\User;
use App\Models\Ratings;

class SurveyController extends BaseController
{
    
    //{
    //    "genre":"masculino",
    //    "age":"351",
    //    "birthplace":"marte",
    //    "studies_id":"15",
    //    "other_stuides":"no",
    //    "kinship_id":"39",
    //    "other_kinship":"no",
    //    "time_going":"1622099536",
    //    "other_center":"no",
    //    "ratings": [
    //        {
    //            "survey_id":"15",
    //            "question":"12",
    //            "value":"3"
    //        },
    //        {
    //            "survey_id":"25",
    //            "question":"14",
    //            "value":"5"
    //        },
    //        {
    //            "survey_id":"54",
    //            "question":"23",
    //            "value":"2"
    //        },
    //        {
    //            "survey_id":"72",
    //            "question":"18",
    //            "value":"4"
    //        }
    //    ]
    //}
    
    public function __construct(Survey $model)
    {
        parent::__construct($model);
    }


    public function index(Request $req){

        $table = $this->model->getTable();
        $objects = DB::table($table);

        $objects = $this->formatData($req, $objects, true);
        
        if ($req->has('page') && $req->has('per_page')) {
            $limit = $req->per_page;
            $objects = $objects->paginate($limit);
            $collection = $objects->getCollection();
            $paginated = true;
        } else {
            $objects = $objects->get();

            for($i=0; $i<count($objects); $i++){
                $sd = SocioData::find($objects[$i]->socio_data_id); //sd = socio data
                $objects[$i]->socio_data= $sd;

                $r = Ratings::where('survey_id',$objects[$i]->id)->get(); //r = ratings
                $objects[$i]->ratings= $r;

            }

            return response()->json($objects);

            $collection = $objects;
            $paginated = false;
        }



        $collection->transform(function ($object) {
            $object = $this->dateToIso8601($object);
            return $object;
        });

        $objects = $paginated ? $objects->setCollection($collection) : $collection;

        return response()->json($objects);
    }


    public function store(Request $req){

        //$json = json_encode($req->input());
        //$json_decoded = json_decode($json,true);

        //hacer un validator
        $socioData = SocioData::create($req->input());
        $socioData->active = 0;
        $socioData->save();

        $survey = Survey::create([
            "socio_data_id" => $socioData->id,
            "user_id" => $req->user()->id
        ]);

        $ratings = $req->ratings;
        
        $ratings = json_decode($ratings,true);
        //var_dump($ratings);
        
        if($ratings == null)
            return response('Ratings incorrectlt formed',400);

        foreach ($ratings as $rating) {
            $new_rating = Ratings::create([
                "survey_id" => $survey->id,
                "question" => $rating["question"],
                "value" => $rating["value"]
            ]);
        }

        //$ratings = Ratings::create($ratings);

        $survey->socio_data = $socioData;
        $survey->ratings = $ratings;
        
        return response()->json($survey);
    }
}
