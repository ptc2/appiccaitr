<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use App\Helpers\Application;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\User;

class UserAccountSetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        Application::setCMS();
    }
    /**
     * Return view for set account
     * 
     * @param string $token
     */
    public function showSetForm($token)
    {
        $tokenData = DB::table('set_account_tokens')
            ->where('token', $token)->first();

        if ($tokenData && !$this->tokenExpired($tokenData)) {
            return view('auth.account.set', [
                'url' => route('user.account.request'),
                'token' => $token
            ]);
        } else {
            return view('auth.account.error-set')->with('message', trans('set_account.message_error_set'));
        }
    }
    /**
     * Set account
     * 
     * @param  Request $request
     */
    public function set(Request $request)
    {
        $validatedData = $request->validate([
            'password' => 'required|min:6|regex:/[A-Z]/',
            'password_confirmation' => 'required|in:' . $request->password
        ]);

        $tokenData = DB::table('set_account_tokens')
            ->where('token', $request->token)->first();

        if ($tokenData && !$this->tokenExpired($tokenData)) {
            $password = $request->password;
            $user = User::where('username', $tokenData->email)->first();
            if (!$user) return view('auth.account.error-reset')->with('message', trans('set_account.no_user'));
            $user->password = $password;
            $user->active = 1;
            $user->save();

            DB::table('set_account_tokens')->where('email', $user->username)->delete();

            return view('auth.account.success-set')->with('message', trans('set_account.message_success_set'));
        } else {
            return view('auth.account.error-set')->with('message', trans('set_account.message_error_set'));
        }
    }
    /**
     * Check if token reset is expired
     * 
     * @param string $token
     * @return bool
     */
    private function tokenExpired($tokenData)
    {
        return Carbon::parse($tokenData->created_at)->addSeconds(config('auth.password_timeout'))->isPast();
    }
}
