<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $module_id = null;
    protected $fillable = [
        'player_id',
        'push_token',
        'uuid',
        'language',
        'model_device',
        'os_version',
        'app_version',
        'last_activity',
        'badge_counter',
        'unsubscribe',
    ];
    public $fields = [
        'id',
        'user_id',
        'platform_id',
        'player_id',
        'push_token',
        'uuid',
        'language',
        'model_device',
        'os_version',
        'app_version',
        'last_activity',
        'badge_counter',
        'unsubscribe',
    ];
    /**
     * Grid columns for the model
     *
     * @var array
     */
    public $gridColumns = [
        'id',
        'uuid',
        'language',
        'model_device',
        'os_version',
        'app_version',
        'last_activity',
        'unsubscribe',
    ];
    public $relationsGridColumns = [
        'platform' => [
            'table' => 'platform',
            'field' => 'name',
            'fk' => [
                'field' => 'platform_id'
            ]
        ],
    ];
    /**
     * Filter columns for the model
     *
     * @var array
     */
    public $filters = [
        'uuid' => [
            'field' => 'uuid',
            'type' => 'text',
            'relation' => 'this'
        ],
        'platform' => [
            'table' => 'platform',
            'field' => 'id',
            'display_fields' => ['name'],
            'type' => 'multiple',
            'relation' => 'fk',
            'fk' => [
                'field' => 'platform_id',
            ]
        ],
    ];
    public $modelOptions = [
        'can_create' => false,
        'can_inactive' => false,
        'can_cancel' => false,
        'can_see' => false,
        'can_delete' => false,
    ];
    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'device';
    protected $dates = ['deleted_at'];
    /**
     * The device that belong to the user.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    /**
     * The device that belong to the platform.
     */
    public function platform()
    {
        return $this->belongsTo('App\Models\Platform', 'platform_id');
    }
    /**
     * Get the error onesignals record associated with the device.
     */
    public function deviceOneSignals()
    {
        return $this->hasMany('App\Models\DeviceOneSignal', 'device_id');
    }
}
