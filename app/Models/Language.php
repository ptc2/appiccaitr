<?php

namespace App\Models;

/**
 * Languages Model
 *
 */
class Language extends BaseModel
{
    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'language';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array(
        'id',
        'code',
        'key',
        'description'
    );

    protected $dates = ['deleted_at'];

    /**
     * Grid columns for the model
     *
     * @var array
     */
    public $gridColumns = [];

    public static function getAvailable()
    {
        return self::where('available', 1)->get();
    }

    public static function getMainLanguage()
    {
        return self::where('main', 1)->first();
    }
}
