<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */    
    public $module_id=8;
    protected $i18nDateAttributes= [];
    protected $i18nDateTimeAttributes= [];
    public $datesToIso8601 = [];
    protected $fillable = [
        'id',
        'text',
    ];
    public $fields = [
        'id',
        'text',
    ];
    public $fieldsLangs = [];
    public $masterDataFields = [];
    public $relationsGrid = [];
    public $relatedField = [];
    public $relationsCombo = [];
    public $notExcel = [];
    public $notPDF = [];
    /**
     * Grid columns for the model
     *
     * @var array
     */
    public $gridColumns = [
        'id',
		'label',
        'text',
    ];
    public $relationsGridColumns = [];
    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'question';
    /**
     * Filter columns for the model
     *
     * @var array
     */
    public $filters = [];
    public $modelOptions = [
        'can_create' => false,
        'can_edit' => false,
        'can_delete' => false,
        'can_inactive' => false,
        'can_see' => false,
        'can_cancel' => false,
    ];
    protected $dates = ['deleted_at'];
    /**
     * The validation rules associated with the model
     *
     * @var array
     */
    static $rules= [];
}
