<?php

namespace App\Models;

use App\Events\UserChanged;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;

class User extends BaseModel implements AuthenticatableContract, CanResetPasswordContract
{
	use HasApiTokens, Authenticatable, CanResetPassword, SoftDeletes, Notifiable;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = 3;
	public $datesToIso8601 = [
		'created_at',
		'updated_at',
		'deleted_at',
		'renovation_date'
	];
	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
	];
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'username', 
		'password',
		'active',
		'remember_token',
		'renovation_date'
	];
	public $fields = [
		'username',
		'password',
		'remember_token',
		'active',
		'renovation_date'
	];
	/**
	 * Grid columns for the model
	 *
	 * @var array
	 */
    public $gridColumns = [
    	'id',
		'username',
    ];
	public $relationsGridColumns = [
		'name' => [
	 		'table' => 'user_data',
	 		'field' => 'name',
	 		'mul' => [
	 			'field' => 'user_id'
	 		]
	 	],
	 	'email' => [
	 		'table' => 'user_data',
	 		'field' => 'email',
	 		'mul' => [
	 			'field' => 'user_id'
	 		]
	 	],
	 	'manager' => [
	 		'table' => 'user_data',
	 		'field' => 'manager',
	 		'mul' => [
	 			'field' => 'user_id'
	 		]
	 	],
	];
	/**
	 * Filter columns for the model
	 *
	 * @var array
	 */
	public $filters = [];
	public $fieldsLangs = [];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'user';
	protected $dates = ['deleted_at'];
	/**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
		'saved' => UserChanged::class,
		'deleted' => UserChanged::class,
    ];
    /**
	 * The validation rules associated with the model
	 *
	 * @var array
	 */
	static $rules= [];
	public $modelOptions = [
		'can_create' => true,
		'can_edit' => true,
		'can_inactive' => true,
		'can_cancel' => true,
		'can_see' => true,
		'can_delete' => true,
	];
	static $rulesApi = [
		'username' => 'required|unique:user',
	];

	/**
	 * Set the user's password.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setPasswordAttribute($value)
	{
		$this->attributes['password'] = Hash::make($value);
	}
	/**
	 * Get the model's renovation date.
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getRenovationDateAttribute($value)
	{
		if (request()->is('api/*')) return to_iso_8601_string($value);

		return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y');
	}
	/**
	 * Set the model's renovation date.
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function setRenovationDateAttribute($value)
	{
		$this->attributes['renovation_date'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
	}
	/**
	 * Get the user data record associated with the user.
	 */
	public function userData() {
        return $this->hasOne('App\Models\UserData', 'user_id');
    }
	/**
	 * Route notifications for the mail channel.
	 *
	 * @return string
	 */
	public function routeNotificationForMail()
	{
		return $this->username;
	}
	/**
	 * Send the password reset notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendPasswordResetNotification($token)
	{
		$this->notify(new ResetPasswordNotification($token));
	}

}
