<?php

/**
 * Users Model
 *
 * Users model to use in the Auth process.
 *
 * @see BaseModel
 * @author Solbyte
 *
 */

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;

class UserCms extends BaseModel implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, SoftDeletes, Notifiable;

    protected $dates = ['deleted_at'];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_cms';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * Set true if the model has enabled behaviour
     *
     * @var bool
     */
    static $enableBehabiour = false;
    /**
     * Set the name for scope's column
     *
     * @var string
     */
    static $enableColumn = 'active';
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_cms_profile_id',
		'related_user_id',
        'username',
        'password',
        'remember_token',
        'active',
        'name',
        'surname',
        'email',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    protected $fields = [
        'id',
        'user_cms_profile_id',
		'related_user_id',
        'username',
        'password',
        'active',
        'name',
        'surname',
        'email',
    ];
    public $fieldsLangs = [];
    /**
     * Store the columns schema associated with the model to fill the grid
     *
     * @var array
     */
    public $gridColumns = [
        'id',
        'email',
        'name',
        'surname'
    ];
    public $relationsCombo = [];
    public $modelOptions = [
        'can_edit' => true,
        'can_delete' => false,
        'can_inactive' => false,
        'can_cancel' => false,
        'can_ok' => false
    ];
    /**
     * If set to true, the model will automatically replace all plain-text passwords
     * attributes (listed in $passwordAttributes) with hash checksums
     *
     * @var bool
     */
    public $autoHashPasswordAttributes = true;
    /**
     * hydrates on new entries' validation
     *
     * @var bool
     */
    public $autoHydrateEntityFromInput = false;
    /**
     * hydrates whenever validation is called
     *
     * @var bool
     */
    public $forceEntityHydrationFromInput = false;
    /**
     * If set to true, the object will automatically remove redundant model
     * attributes (i.e.
     * confirmation fields).
     *
     * @var bool
     */
    public $autoPurgeRedundantAttributes = true;
    /**
     * List of attribute names which should be hashed using the Bcrypt hashing algorithm.
     *
     * @var array
     */
    public static $passwordAttributes = [
        'password'
    ];

    /**
     * The validation rules associated with the model
     *
     * @var array
     */
    static $rules = [
        'email' => 'required|email',
        'username' => 'required',
        'name' => 'required',
    ];
    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->attributes[$this->getRememberTokenName()];
    }
    /**
     * Set the token value for the "remember me" session.
     *
     * @param string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }
    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }
    /**
     * Returns the shared schema for a group of users
     *
     * @return string
     */
    public function getProfileSchema()
    {
        // TODO Returns the "parent's" schema for production environment
        return $_ENV['DB_DATABASE'];
    }

    /**
     * Returns the scope id key (wich sets the private behaviour environment)
     *
     * @return Integer
     */
    public function getScopeKey()
    {
        // Fastest option
        if ($this->parent_type == __CLASS__) {
            return $this->parent_id;
        } else {
            return $this->getKey();
        }
    }

    /**
     * Preprocess input data before fill-in the model
     *
     * @param array $input Input data
     * @return array       Processed data
     */
    protected function preProcessData($input)
    {
        $input['active'] = isset($input['active']) ? $input['active'] : false;
        if (empty($input['password']) && empty($input['password_confirmation'])) {
            unset($input['password']);
            unset($input['password_confirmation']);
        }
        return parent::preProcessData($input);
    }
    /**
     * Get the user data record associated with the user.
     */
    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'related_user_id');
    }
    /**
     * Get the user cms profile associated with the user.
     */
    public function userProfile()
    {
        return $this->belongsTo('App\Models\UserCmsProfile', 'user_cms_profile_id');
    }
	/**
	 * Get the user data record associated with the user.
	 */
	public function userData() {
        return $this->hasOne('App\Models\UserData', 'user_id', 'related_user_id');
    }
    /**
     * Route notifications for the mail channel.
     *
     * @return string
     */
    public function routeNotificationForMail()
    {
        return $this->email;
    }
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
	
	
}
