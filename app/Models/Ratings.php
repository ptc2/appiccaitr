<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Ratings extends BaseModel
{
	use SoftDeletes;

	public $module_id = null;
	public $datesToIso8601 = [
		'created_at',
		'updated_at',
		'deleted_at',
		'birth_date'
	];
	protected $fillable = [
		'survey_id',
		'question',
		'value',
	];

	protected $table = 'ratings';
	protected $dates = ['deleted_at'];

	static $rules = [];
	static $rulesApi = [];

	//relationships
	public function survey()
	{
		return $this->hasOne('App\Models\Survey', 'id', 'survey_id');
	}
}
