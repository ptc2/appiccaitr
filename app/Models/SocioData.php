<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class SocioData extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = null;
	public $datesToIso8601 = [
		'created_at',
		'updated_at',
		'deleted_at',
		'birth_date'
	];
	protected $fillable = [
		'active',
		'studies_id',
		'genre',
		'age',
		'birthplace',
		'other_studies',
		'kinship_id',
		'other_kinship',
		'time_going',
		'other_center',
	];

	protected $table = 'socio_data';
	protected $dates = ['deleted_at'];

	static $rules = [];
	static $rulesApi = [];

	//relationships
	public function studies(){
		return $this->hasOne('App\Models\Studies', 'id', 'studies_id');
	}
	public function kinship(){
		return $this->hasOne('App\Models\Kinship', 'id', 'kinship_id');
	}
}
