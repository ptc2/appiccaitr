<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Stringy\StaticStringy as Stringy;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Module extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array(
        'active',
        'name',
        'table_name',
        'slug',
        'icon',
        'type_icon',
        'data_order',
        'is_menu',
        'is_dashboard',
        'description'
    );
    public $fields = array(
        'active',
        'name',
        'table_name',
        'slug',
        'icon',
        'type_icon',
        'data_order',
        'is_menu',
        'is_dashboard',
        'description'
    );
    public $fieldsLangs = [];
    public $notExcel = [];
    public $notPDF = [];
    /**
     * Grid columns for the model
     *
     * @var array
     */
    public $gridColumns = [
        'id',
        'active',
        'name',
        'table_name',
        'slug',
        'icon',
        'data_order',
        'is_menu',
        'is_dashboard',
        'description'
    ];
    protected $dates = ['deleted_at'];
    public $modelOptions = [
        'can_create' => true,
        'can_edit' => true,
        'can_delete' => true,
        'can_inactive' => false,
        'can_see' => true,
        'can_cancel' => true,
    ];
    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'module';
    /**
     * The validation rules associated with the model
     *
     * @var array
     */
    static $rules = array(
        'slug' => 'required',
        'name' => 'required'
    );
    static function getModulesWithSEO()
    {
        $modules = self::where('active', '=', 1)->get();
        $ms = [];
        foreach ($modules as $key => $module) {
            $camelizeModule = Stringy::upperCamelize($module->table_name);
            $class = '\\App\\Models\\' . $camelizeModule;
            $splice = false;

            if (class_exists($class)) {
                $model = new $class;
                if (array_key_exists('seo', $model->options) && $model->options['seo']) $splice = true;
            }

            if ($splice) {
                $ms[] = $module;
            }
        }
        $modules = $ms;
        $listItems = [];
        foreach ($modules as $key => $module) {
            $camelizeModule = Stringy::upperCamelize($module->name);
            $class = '\\App\\Models\\' . $camelizeModule;
            $model = new $class;
            $table = $model->getTable();
            $result = DB::table($table . ' AS t')
                ->leftJoin($table . '_langs AS tl', 't.id', '=', 'tl.' . $module->name . '_id')
                ->where('tl.language_id', '=', Language::getMainLanguage()->id)
                ->get();
            $listItems[] = [
                'model_camelize' => $camelizeModule,
                'model' => $module->name,
                'all' => $result
            ];
        }

        return $listItems;
    }
    /**
     * The user cms profile that belong to the module.
     */
    public function profiles()
    {
        return $this->belongsToMany('App\Models\UserCmsProfile', 'module_user_cms_profile')
            ->withTimestamps()
            ->withPivot('c', 'r', 'u', 'd');
    }
}
