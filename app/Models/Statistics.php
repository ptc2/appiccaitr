<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Statistics extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = null;
	protected $fillable = [
		'id',
		'mean',
		'mode',
		'standard_deviation',
		'crobach',
		//'created_at',
		//'deleted_at',
		//'updated_at',
	];
	/*public $fields = [
		'name',
		'label',
		'description',
	];
	public $gridColumns = [
		'id',
		'name',
		'label',
		'description',
	];
	public $modelOptions = [
		'can_create' => true,
		'can_edit' => true,
		'can_delete' => true,
		'can_inactive' => false,
		'can_see' => true,
		'can_cancel' => true,
	];*/
	protected $table = 'statistics';
}
