<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Kinship extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */    
	public $module_id=6;
	protected $i18nDateAttributes= [];
    protected $i18nDateTimeAttributes= [];
    public $datesToIso8601 = [];
    protected $fillable = [
        'name',
        'label',
        'description'
	];
    public $fields = [
        'name',
        'label',
        'description'
	];
    public $fieldsLangs = [];
	public $masterDataFields = [];
    public $relationsGrid = [];
    public $relatedField = [];
	public $relationsCombo = [];
    public $notExcel = [];
	public $notPDF = [];
	/**
	 * Grid columns for the model
	 *
	 * @var array
	 */
    public $gridColumns = [
		'id',
		'name',
		'label'
	];
	public $relationsGridColumns = [];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'kinship';
	/**
	 * Filter columns for the model
	 *
	 * @var array
	 */
	public $filters = [];
	public $modelOptions = [
        'can_create' => true,
        'can_edit' => true,
        'can_delete' => true,
        'can_inactive' => false,
        'can_see' => true,
        'can_cancel' => true,
    ];
	protected $dates = ['deleted_at'];
	/**
	 * The validation rules associated with the model
	 *
	 * @var array
	 */
	static $rules= [];
}
