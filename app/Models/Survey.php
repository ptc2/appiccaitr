<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

class Survey extends BaseModel
{
	use SoftDeletes;

	public $module_id = 4;
	protected $fillable = [
		'user_id',
		'socio_data_id'
	];
	public $fields = [
        'user_id',
		'socio_data_id'
	];
	/**
	 * Grid columns for the model
	 *
	 * @var array
	 */
    public $gridColumns = [
		'id',
		'created_at'
	];
	public $relationsGridColumns = [
	 	'code' => [
	 		'table' => 'user',
	 		'field' => 'username',
			'fk' => [
				'field' => 'user_id',
            ]
	 	],
		'name' => [
	 		'table' => 'user_data',
	 		'field' => 'name',
			'fk' => [
				'field' => 'user_id',
            ]
	 	],
	];
	public $gridColumnsToHide = ['active'];
	protected $table = 'survey';
	/**
	 * Filter columns for the model
	 *
	 * @var array
	 */
	public $filters = [
		'code' => [
	 		'table' => 'user',
			'field' => 'username',
			'relation' => 'fk',
	 		'fk' => [
				'field' => 'user_id',
            ]
	 	],
        'created_at' => [
	        'field' => 'created_at',
	        'type' => 'date',
	        'relation' => 'this'
		]
	];
	public $modelOptions = [
        'can_create' => false,
        'can_edit' => false,
        'can_delete' => false,
        'can_inactive' => false,
        'can_see' => true,
        'can_cancel' => false,
    ];
	protected $dates = ['deleted_at'];
	
	public $relationsToDelete = [
		'socioData', 
		'ratings'
	];

	protected function preProcessData($input) {
		return $input;
	}

	//relationships
	public function user()
	{
		return $this->hasOne('App\Models\User', 'id', 'user_id');
	}
	public function socioData()
	{
		$userRole = Auth::user()->user_cms_profile_id;
		return (IntVal($userRole) == 1) ? $this->hasOne('App\Models\SocioData', 'id', 'socio_data_id')->withTrashed() : $this->hasOne('App\Models\SocioData', 'id', 'socio_data_id');
	}
	public function ratings()
	{
		$userRole = Auth::user()->user_cms_profile_id;
		return (IntVal($userRole) == 1) ? $this->hasMany('App\Models\Ratings', 'survey_id')->withTrashed() : $this->hasMany('App\Models\Ratings', 'survey_id');
	}
}
