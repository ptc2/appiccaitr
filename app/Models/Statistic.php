<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Statistic extends BaseModel
{
	use SoftDeletes;
	
    public $module_id = 5;
    protected $fillable = [];
    public $fields = [
		'socio_data_id',
		'survey_id',
		'questions',
		'values'
	];
    /**
     * Grid columns for the model
     *
     * @var array
     */
    public $gridColumns = [
        'id',
    ];
    public $relationsGridColumns = [
        'code' => [
	 		'table' => 'user',
	 		'field' => 'username',
			'fk' => [
				'field' => 'id'
            ]
	 	],
		'name' => [
	 		'table' => 'user_data',
	 		'field' => 'name',
			'fk' => [
				'field' => 'id'
            ]
	 	],
    ];
	public $gridColumnsToHide = ['active'];
    protected $table = 'statistic';
    /**
     * Filter columns for the model
     *
     * @var array
     */
    public $filters = [
		'code' => [
	 		'table' => 'user',
			'field' => 'username',
			'relation' => 'fk',
	 		'fk' => [
                'field' => 'id',
            ]
	 	],
	];
    public $modelOptions = [
        'can_create' => false,
        'can_edit' => false,
        'can_delete' => false,
        'can_inactive' => false,
        'can_see' => true,
        'can_cancel' => false,
    ];
    protected $dates = [];

    protected function preProcessData($input) {
		return $input;
	}

    //relationships
    public function user()
    {
		return $this->hasOne('App\Models\User', 'id');
    }
    public function socioData()
    {
        return $this->hasOne('App\Models\SocioData', 'id', 'socio_data_id');
    }
	public function surveys()
	{
		return $this->hasMany('App\Models\Survey', 'id', 'survey_id');
	}
}
