<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class DeviceOneSignal extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $module_id = null;
    protected $fillable = [
        'type',
        'message_error',
        'body',
        'function',
    ];
    public $fields = [
        'device_id',
        'type',
        'message_error',
        'body',
        'function',
    ];
    /**
     * The schema associated with the model.
     *
     * @var string
     */
    protected $table = 'device_onesignal';
    protected $dates = ['deleted_at'];
    public $relationship = [
        'device'
    ];
    /**
     * The device that belong to the platform.
     */
    public function device()
    {
        return $this->belongsTo('App\Models\Device', 'device_id');
    }
}
