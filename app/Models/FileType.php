<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class FileType extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = null;
	protected $fillable = [
		'name',
		'label',
		'description',
	];
	public $fields = [
		'name',
		'label',
		'description',
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'file_type';
	protected $dates = ['deleted_at'];
	/**
	 * Get the files user data for the type.
	 */
	public function filesUserData()
	{
		return $this->hasMany('App\Models\FileUserData', 'file_type_id');
	}
	/**
	 * Get the files event for the type.
	 */
	public function filesEvent()
	{
		return $this->hasMany('App\Models\FileEvent', 'file_type_id');
	}
}
