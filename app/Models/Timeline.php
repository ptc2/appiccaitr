<?php

namespace App\Models;

use Request;
use Auth;
use Session;
use DB;

use Illuminate\Database\Eloquent\Model;


/**
 * @brief 	Clase Timeline para Anugal. Registra actiones de usuario en BBDD 
 * 
 *        
 */
class Timeline extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'actor_ip',
        'username',
        'action',
        'actor_type',
        'actor_id',
        'object_type',
        'object_id'
    ];
    protected $table = 'timeline';
    // Sets the private behaviour
    protected $private_behaviour = false;
    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes            
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setVisible($this->fillable);
    }
    /**
     * Save the model to the database.
     *
     * @see \Illuminate\Database\Eloquent\Model
     * @return bool
     */
    public function save(array $options = [])
    {
        $data = [];
        if (!isset($this->actor_ip)) {
            $ip = Request::getClientIp();
            if ($ip)
                $data['actor_ip'] = ip2long($ip);
        }

        if (Auth::check()) {
            $user = Auth::getUser();
            $data['actor_type'] = empty($this->actor_type) ? get_class($user) : $this->actor_type;
            $data['actor_id'] = empty($this->actor_id) ? $user->id : $this->actor_id;
            $data['username'] = empty($this->username) ? $user->username : $this->username;
        } else {
            $data['username'] = 'anonymous';
        }
        $this->fill($data);
        return parent::save($options);
    }
    /** 
     * Returns the failed login attemps from this IP and the last attempt date  
     * 
     * @param string $ip
     * @return multitype:number \Carbon\Carbon>
     */
    public function getFailedLoginAttemps($ip = null)
    {
        $ip = ($ip) ? $ip : ip2long(Request::getClientIp());
        $data = array(
            'attemps' => 0,
            'lastAttemp' => \Carbon\Carbon::now()
        );

        $items = DB::table($this->getTable())
            ->where($this->getTable() . '.actor_ip', $ip)
            ->where($this->getTable() . '.action', 'session.login')
            ->orderBy('id', 'desc')
            ->take(1)
            ->select('created_at')
            ->get();

        $lastSuccessfullLogin = ($items && isset($items[0])) ? $items[0]->created_at : \Carbon\Carbon::create(1976);

        $items = DB::table($this->getTable())
            ->where($this->getTable() . '.actor_ip', $ip)
            ->where($this->getTable() . '.action', 'session.failed_login')
            ->where($this->getTable() . '.created_at', '>', $lastSuccessfullLogin)
            ->orderBy('id', 'desc')
            ->select('created_at')
            ->get();

        if ($items->isNotEmpty()) {
            $data['attemps'] = count($items);
            $data['lastAttemp'] = $items[0]->created_at;
        }

        return $data;
    }
}
