<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserData extends BaseModel
{
	use SoftDeletes;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public $module_id = null;
	public $datesToIso8601 = [
		'created_at',
		'updated_at',
		'deleted_at',
		'birth_date'
	];
	protected $fillable = [
		'user_id',
		'name',
		'email',
		'manager',
		'address',
		'province',
		'city',
		'members',
		'phone'
	];
	public $fields = [
		'user_id',
		'name',
		'email',
		'manager',
		'address',
		'province',
		'city',
		'members',
		'phone'
	];
	/**
	 * The schema associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'user_data';
	protected $dates = ['deleted_at'];
	static $rules = [
		'name' => 'required',
		'email' => 'required|unique:user_data,email'
	];
	static $rulesApi = [
		'name' => 'required',
		'email' => 'required|unique:user_data,email'
	];
	/**
	 * Get the user that owns the user data.
	 */
	public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
