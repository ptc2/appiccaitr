<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use App\Services\StatisticService;

class StatisticFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return StatisticService::class;
    }
}
