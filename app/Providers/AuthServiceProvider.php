<?php

namespace App\Providers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $modules = \App\Models\Module::where('has_policy', 1)->get();
        foreach($modules as $module)
        {   
            Gate::define('index_' . $module->name, 'App\Policies\\' .ucfirst(Str::camel($module->name)) . 'Policy@index');
            Gate::define('edit_' . $module->name, 'App\Policies\\' .ucfirst(Str::camel($module->name)) . 'Policy@edit');
            Gate::define('create_' . $module->name, 'App\Policies\\' .ucfirst(Str::camel($module->name)) . 'Policy@create');
            Gate::define('update_' . $module->name, 'App\Policies\\' .ucfirst(Str::camel($module->name)) . 'Policy@update');
            Gate::define('destroy_' . $module->name, 'App\Policies\\' .ucfirst(Str::camel($module->name)) . 'Policy@destroy');
            Gate::define('grid_' . $module->name, 'App\Policies\\' .ucfirst(Str::camel($module->name)) . 'Policy@grid');
            Gate::define('listToPdf_' . $module->name, 'App\Policies\\' .ucfirst(Str::camel($module->name)) . 'Policy@listToPdf');
            Gate::define('export_' . $module->name, 'App\Policies\\' .ucfirst(Str::camel($module->name)) . 'Policy@export');
        }
    }
}