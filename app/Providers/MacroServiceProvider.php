<?php 

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {        
        require base_path() . '/resources/macros/macros.php';
        require base_path() . '/resources/macros/filters.php';
        //require base_path() . '/resources/macros/macro2.php';
        // etc...
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}