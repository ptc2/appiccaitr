<?php

namespace App\Providers;

use App\Services\OneSignalService;
use Illuminate\Support\ServiceProvider;

class OneSignalServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OneSignalService::class, function ($app) {
            return new OneSignalService();
        });
    }
}