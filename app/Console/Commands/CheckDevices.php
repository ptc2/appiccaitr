<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use OneSignal\Config;
use OneSignal\OneSignal;
use Symfony\Component\HttpClient\Psr18Client;
use Nyholm\Psr7\Factory\Psr17Factory;

use App\Models\Device;

class CheckDevices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'onesignal:check-devices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check update devices from OneSignal';

    private $devices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $config = new Config(
            env('ONESIGNAL_APPLICATION_ID', 'dd374b68-cb1d-4f88-a14f-677768ff815b'), 
            env('ONESIGNAL_APPLICATION_AUTH_KEY', 'YzliMzU2YzYtMDljZC00ZmU1LTk4NWEtNDMzZmYzNDcxMmRm'), 
            env('ONESIGNAL_USER_AUTH_KEY', 'OTdiNGY0MWItNzhlMS00YWRlLWJmZWQtMDI3ODIxODhmNzFh')
        );
        $httpClient = new Psr18Client();
        $requestFactory = $streamFactory = new Psr17Factory();

        $api = new OneSignal($config, $httpClient, $requestFactory, $streamFactory);
        $this->devices = $api->devices();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        
        $oneSignalDevices = $this->devices->getAll();
        $totalCount = $oneSignalDevices['total_count'];
        $limit = $oneSignalDevices['limit'];

        foreach($oneSignalDevices['players'] as $player) {
            $device = Device::where('player_id', $player['id'])->orWhere('push_token', $player['identifier'])->first();
            if($device) {
                $data = [
                    'player_id' => $player['id'],
                    'badge_counter' => $player['badge_count'],
                    'unsubscribe' => $player['invalid_identifier']
                ];
                $device->update($data);
            }
        }
    }
}
