<?php

namespace App\Policies;

use App\Models\BaseModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserCmsPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    /**
     * Determine if the given user can show laborers.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function edit(BaseModel $user, $class)
    {
        return $user->id == request()->id;
    }
    /**
     * Determine if the given user can update laborers.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function update(BaseModel $user, $class)
    {
        return $user->id == request()->id;
    }
}
