<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Models\BaseModel;
use App\Models\Module;

class BasePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    public function before($user, $ability)
    {   
        
    }
    /**
     * Determine if the given user can see list.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function index(BaseModel $user, $class)
    {
        $class = new $class();
        $moduleId = $class->module_id?: $this->getModuleId($class->getTable());
        $module = Module::find($moduleId);
        
        return $module && isset($class->modelOptions['can_see']) ? $class->modelOptions['can_see'] : false;
    }
    /**
     * Determine if the given user can show.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function edit(BaseModel $user, $class)
    {
        $class = new $class();
        $moduleId = $class->module_id?: $this->getModuleId($class->getTable());
        $module = Module::where('module_id', $moduleId)->first();

        return $module && isset($class->modelOptions['can_see']) ? $class->modelOptions['can_see'] : false;
    }
    /**
     * Determine if the given user can create.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function create(BaseModel $user, $class)
    {
        $class = new $class();
        $moduleId = $class->module_id?: $this->getModuleId($class->getTable());
        $module = Module::find($moduleId);
        
        return $module && isset($class->modelOptions['can_create']) ? $class->modelOptions['can_create'] : false;
    }
        /**
     * Determine if the given user can update.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function update(BaseModel $user, $class)
    {
        $class = new $class();
        $moduleId = $class->module_id?: $this->getModuleId($class->getTable());
        $module = Module::find($moduleId);
        
        return $module && isset($class->modelOptions['can_edit']) ? $class->modelOptions['can_edit'] : false;
    }
    /**
     * Determine if the given user can delete.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function destroy(BaseModel $user, $class)
    {
        $class = new $class();
        $moduleId = $class->module_id?: $this->getModuleId($class->getTable());
        $module = Module::find($moduleId);
        
        return $module && isset($class->modelOptions['can_delete']) ? $class->modelOptions['can_delete'] : false;
    }
    /**
     * Determine if the given user can see grid.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function grid(BaseModel $user, $class)
    {
        $class = new $class();
        $moduleId = $class->module_id?: $this->getModuleId($class->getTable());
        $module = Module::find($moduleId);
        
        return $module && isset($class->modelOptions['can_see']) ? $class->modelOptions['can_see'] : false;
    }
    /**
     * Determine if the given user can export pdf.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function listToPdf(BaseModel $user, $class)
    {
        $class = new $class();
        $moduleId = $class->module_id?: $this->getModuleId($class->getTable());
        $module = Module::find($moduleId);

        return $module && isset($class->modelOptions['can_see']) ? $class->modelOptions['can_see'] : false;
    }
    /**
     * Determine if the given user can export excel.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function export(BaseModel $user, $class)
    {
        $class = new $class();
        $moduleId = $class->module_id?: $this->getModuleId($class->getTable());
        $module = Module::find($moduleId);
        
        return $module && isset($class->modelOptions['can_see']) ? $class->modelOptions['can_see'] : false;
    }
    /**
     * Return module id by table name
     * 
     * @param String $table
     * @return Int
     */
    protected function getModuleId($table)
    {   
        $module = Module::where('name', $table)->orderBy('updated_at', 'desc')->first();
        
        return $module ? $module->id : 0;
    }
}
