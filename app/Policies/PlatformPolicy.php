<?php

namespace App\Policies;

use App\Models\BaseModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class PlatformPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    /**
     * Determine if the given user can see laborers list.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function index(BaseModel $user, $class)
    {
        return parent::index($user, $class);
    }
    /**
     * Determine if the given user can show laborers.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function edit(BaseModel $user, $class)
    {
        return parent::edit($user, $class);
    }
    /**
     * Determine if the given user can create laborers.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function create(BaseModel $user, $class)
    {
        return parent::create($user, $class);
    }
        /**
     * Determine if the given user can update laborers.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function update(BaseModel $user, $class)
    {
        return parent::update($user, $class);
    }
    /**
     * Determine if the given user can delete laborers.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function destroy(BaseModel $user, $class)
    {
        return parent::destroy($user, $class);
    }
        /**
     * Determine if the given user can see grid.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function grid(BaseModel $user, $class)
    {
        return parent::grid($user, $class);
    }
    /**
     * Determine if the given user can export pdf.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function listToPdf(BaseModel $user, $class)
    {
        return parent::listToPdf($user, $class);
    }
    /**
     * Determine if the given user can export excel.
     *
     * @param  \App\BaseModel  $user
     * @return bool
     */
    public function export(BaseModel $user, $class)
    {
        return parent::export($user, $class);
    }
}
