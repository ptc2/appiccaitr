<?php

/**
 * Abstract Presenter Class
 *
 * Abstract Presenter Layer Class to improve the model's view layer
 *
 * @author fmgonzalez
 * @since 2014/6/19
 *
 */

namespace App\Presenters;

use App\Presenters\Presentable;

use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;

/**
 * Abstract Presenter Class implementation
 */
abstract class AbstractPresenter implements ArrayAccess, Arrayable, Presentable
{

    /**
     * The object to present
     *
     * @var mixed
     */
    protected $object;

    /**
     * The array class types (Typables)
     *
     * @var array
     */
    protected $types = [];

    /**
     * The array callbacks class types to fill the values for each type
     *
     * @var array
     */
    protected $typesCallbacks = [];

    /**
     * The array of data types
     *
     * @var array
     */
    protected $dataTypes = [];

    /**
     * Inject the object to be presented
     *
     * @param mixed $object
     */
    public function set($object)
    {
        $this->object = $object;
    }

    /**
     * Check to see if there is a presenter method.
     * If not pass to the object
     *
     * @param string $key
     */
    public function __get($key)
    {
        if (method_exists($this, $key)) {
            return $this->$key();
        }

        return $this->object->$key;
    }


    /**
     * Get DataTypes
     *
     * @return multitype:
     */
    public function getDataTypes()
    {
        return $this->dataTypes;
    }

    /**
     * Set DataTypes
     *
     * @param array $data
     */
    public function setDataTypes($data = array())
    {
        if (empty($data)) {
            foreach ($this->types as $type) {
                if (array_key_exists($type, $this->typesCallbacks)) {
                    try {
                        if (method_exists($this, $this->typesCallbacks[$type])) {
                            $data[$type] = call_user_func_array(array($this, $this->typesCallbacks[$type]), []);
                        } else {
                            $data[$type] = 'ERROR: Callback is missing for ' . $type;
                        }
                    } catch (Exception $e) {
                        $data[$type] = 'ERROR: Callback is missing for ' . $type;
                    }
                } else {
                    $object = new \ReflectionClass($type);
                    if ($object->implementsInterface('App\Models\InterfaceTypable')) {
                        $data[$type] = (new $type())->getValues();
                    } else {
                        $data[$type] = 'ERROR: Class ' . $type . ' doesn\'t implements Typable';
                    }
                }
            }
        }
        $this->dataTypes = $data;
    }

    /**
     * Generic presenter
     */
    public function active()
    {
        $faClass = $this->object->{__FUNCTION__} ? 'fa-check text-success' : 'fa-times text-danger';
        return '<div class="text-center"><i class="fa ' . $faClass . '"></i></div>';
    }
    public function image1()
    {
        if ($this->object->{__FUNCTION__} != '' && file_exists('uploads/' . $this->object->{__FUNCTION__}))
            return '<img src="' . env('APP_URL') . '/uploads/' . $this->object->{__FUNCTION__} . '" width="100" height="75" />';
        else
            return '';
    }

    /**
     * Returns a typical name from a Type class
     *
     * @param string $class
     * @param string $value
     * @return string
     */
    /*public function type($class, $value){
        $result = null;
        if ( in_array($class, $this->types) && class_exists($class) ) {
            $object = new \ReflectionClass($class);
            if ( $object->implementsInterface('App\Models\InterfaceTypable') ){
                if (! array_key_exists($class, $this->dataTypes)) {
                    $data = $object->getValues();
                    $this->dataTypes[$class] = $data;
                }
                $result = is_array($this->dataTypes[$class])
                                ? ( array_key_exists($value, $this->dataTypes[$class]) ? $this->dataTypes[$class][$value] : $value )
                                : $this->dataTypes[$class];
            } else {
                $result = 'ERROR: Class ' . $class . ' doesn\'t implements Typable';
            }
        }else {
            $result = 'ERROR: Class ' . $class . ' doesn\'t exists or unavailable';
        }
        return $result;
    }*/

    /**
     * Returns a typical percentaje
     *
     * @param string $value
     * @return string
     */
    public function percentage($value)
    {
        return $this->object->{$value} . ' %';
    }

    /**
     * Returns a typical distance
     *
     * @param string $value
     * @return string
     */
    public function distance($value)
    {
        return $this->object->{$value} . trans('format.distance');
    }

    /**
     * Get the presenter as a plain array.
     *
     * @return array
     */
    public function toArray()
    {
        $items = $this->object->toArray();
        $result = array();
        foreach ($items as $key => $value) {
            $result[$key] = $this->$key;
        }
        return $result;
    }

    /**
     * ==============================================================
     * Implementation for Presentable
     * ==============================================================
     */

    /**
     * Return an instance of a Model wrapped in a presenter object
     *
     * @param Model $model
     * @param Presentable $presenter
     * @return Model
     */
    public function model(Model $model, Presentable $presenter)
    {
        $object = clone $presenter;

        $object->set($model);

        return $object;
    }

    /**
     * Return an instance of a Collection with each value
     * wrapped in a presenter object
     *
     * @param Collection $collection
     * @param Presentable $presenter
     * @return Collection
     */
    public function collection(Collection $collection, Presentable $presenter)
    {
        $dataTypes = $presenter->getDataTypes();
        if (empty($dataTypes)) {
            $presenter->setDataTypes();
        }
        foreach ($collection as $key => $value) {
            $collection->put($key, $this->model($value, $presenter));
        }

        return $collection;
    }

    /**
     * Return an instance of a Paginator with each value
     * wrapped in a presenter object
     *
     * @param Paginator $paginator
     * @param Presentable $presenter
     * @return Paginator
     */
    public function paginator(Paginator $paginator, Presentable $presenter)
    {
        $items = array();

        foreach ($paginator->getItems() as $item) {
            $items[] = $this->model($item, $presenter);
        }

        $paginator->setItems($items);

        return $paginator;
    }


    /**
     * ==============================================================
     * Implementation for ArrayAccess
     * ==============================================================
     */

    /**
     * Check to see if the offset exists
     * on the current object
     *
     * @param string $key
     * @return boolean
     */
    public function offsetExists($key)
    {
        return isset($this->object[$key]);
    }

    /**
     * Retrieve the key from the object
     * as if it were an array
     *
     * @param string $key
     * @return boolean
     */
    public function offsetGet($key)
    {
        return $this->object[$key];
    }

    /**
     * Set a property on the object
     * as if it were any array
     *
     * @param string $key
     * @param mixed $value
     */
    public function offsetSet($key, $value)
    {
        $this->object[$key] = $value;
    }

    /**
     * Unset a key on the object
     * as if it were an array
     *
     * @param string $key
     */
    public function offsetUnset($key)
    {
        unset($this->object[$key]);
    }

    protected function booleanFormat($value)
    {
        if ($value == '1') {
            return trans('download.yes');
        } else {
            return trans('download.no');
        }
    }

    protected function dateFormat($value)
    {
        if (!empty($value) && preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $value) && !request()->is('api/*')) {
            $date = Carbon::createFromFormat('Y-m-d', $value);
            return $date->format('d/m/Y');
        }

        return $value;
    }
}
