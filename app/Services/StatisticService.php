<?php

namespace App\Services;

use OneSignal;
use Illuminate\Http\Request;

use App\Models\Statistic;
use App\Models\Question;
use App\Models\SocioData;

use Auth;

class StatisticService
{
    public function getStatisticForAGivenUser($id = NULL)
    {
        $results = [];
        if(!is_null($id)) {
            $userRole = Auth::user()->user_cms_profile_id;
            
            $items = (IntVal($userRole) == 1) ? Statistic::where('id', $id)->withTrashed()->get() : Statistic::where('id', $id)->get();

            $results = $this->buildStatisticItemsToShow($items);
        }

        return $results;
    }
    public function buildStatisticItemsToShow($items)
    {
        $results = [];
        $questions = [];
        foreach($items as $item) {
            $arrayQuestions = explode(',', $item->questions);
            $arrayValues = explode(',', $item->values);
            
            foreach($arrayQuestions as $index => $value) {                    
                $questions[$value][] = $arrayValues[$index];
            }
        }

        foreach($questions as $index => $values) {
            // average
            $average = $this->getAverageValue($values);
            // standard_deviation
            $standard_deviation = $this->getStandardDeviationValue($values, $average);
            // min
            $min = $this->getMinValue($values);
            // max
            $max = $this->getMaxValue($values);
            // mode         
            $mode = $this->getModeValue($values);
            
            $question = Question::find($index);
            
            $results[$index] = [
                'label' => $question->text,
                'average' => $average,
                'standard_deviation' => $standard_deviation,
                'min' => $min,
                'max' => $max,
                'mode' => $mode
            ];
        }
        
        return $results;
    }
    public function getTotalsAndPercentagesInfo($surveys, $members = 0)
    {
        $userRole = Auth::user()->user_cms_profile_id;
        $totals = [];
        $percentages = [];
        
        $totals['totalSurveys'] = 0;
            
        $totals['totalMen'] = 0;
        $totals['totalWomen'] = 0;
        
        $totals['totalMother'] = 0;
        $totals['totalFather'] = 0;
        $totals['totalAunt'] = 0;
        $totals['totalUncle'] = 0;
        $totals['totalGrandma'] = 0;
        $totals['totalGrandfather'] = 0;
        $totals['totalGuardian'] = 0;
        $totals['totalTutor'] = 0;
        $totals['totalNanny'] = 0;
        $totals['totalCaregiver'] = 0;
        $totals['totalOther'] = 0;
        
        $totals['totalUnder20Years'] = 0;
        $totals['totalBetween20And29Years'] = 0;
        $totals['totalBetween30And39Years'] = 0;
        $totals['totalBetween40And49Years'] = 0;
        $totals['totalBetween50And59Years'] = 0;
        $totals['totalOverEqual60Years'] = 0;
        
        $totals['totalUneducatedStudies'] = 0;
        $totals['totalPrimaryStudies'] = 0;
        $totals['totalHighSchoolEducationStudies'] = 0;
        $totals['totalHigherEducation'] = 0;
        $totals['totalPostgraduateStudies'] = 0;
        $totals['totalOtherStudies'] = 0;
        
        $totals['totalMenUneducatedStudies'] = 0;
        $totals['totalMenPrimaryStudies'] = 0;
        $totals['totalMenHighSchoolEducationStudies'] = 0;
        $totals['totalMenHigherEducation'] = 0;
        $totals['totalMenPostgraduateStudies'] = 0;
        $totals['totalMenOtherStudies'] = 0;
        
        $totals['totalWomenUneducatedStudies'] = 0;
        $totals['totalWomenPrimaryStudies'] = 0;
        $totals['totalWomenHighSchoolEducationStudies'] = 0;
        $totals['totalWomenHigherEducation'] = 0;
        $totals['totalWomenPostgraduateStudies'] = 0;
        $totals['totalWomenOtherStudies'] = 0;
        
        $totals['totalMenStudies'] = 0;
        $totals['totalWomenStudies'] = 0;

        $totals['totalBetween1And12MonthsGoing'] = 0;
        $totals['totalBetween13And24MonthsGoing'] = 0;
        $totals['totalBetween25And36MonthsGoing'] = 0;
        $totals['totalBetween37And48MonthsGoing'] = 0;
        $totals['totalBetween49And60MonthsGoing'] = 0;
        $totals['totalBetween61And72MonthsGoing'] = 0;
        
        $percentages['percentageSurveyAnswered'] = 0;
        
        $percentages['percentageSurveyMenAnswered'] = 0;
        $percentages['percentageSurveyWomenAnswered'] = 0;
        
        $percentages['percentageSurveyMotherAnswered'] = 0;
        $percentages['percentageSurveyFatherAnswered'] = 0;
        $percentages['percentageSurveyAuntAnswered'] = 0;
        $percentages['percentageSurveyUncleAnswered'] = 0;
        $percentages['percentageSurveyGrandmaAnswered'] = 0;
        $percentages['percentageSurveyGrandfatherAnswered'] = 0;
        $percentages['percentageSurveyGuardianAnswered'] = 0;
        $percentages['percentageSurveyTutorAnswered'] = 0;
        $percentages['percentageSurveyNannyAnswered'] = 0;
        $percentages['percentageSurveyCaregiverAnswered'] = 0;
        $percentages['percentageSurveyOtherAnswered'] = 0;
        
        $percentages['percentageSurveyUnder20YearsAnswered'] = 0;
        $percentages['percentageSurveyBetween20And29YearsAnswered'] = 0;
        $percentages['percentageSurveyBetween30And39YearsAnswered'] = 0;
        $percentages['percentageSurveyBetween40And49YearsAnswered'] = 0;
        $percentages['percentageSurveyBetween50And59YearsAnswered'] = 0;
        $percentages['percentageSurveyOverEqual60YearsAnswered'] = 0;
        
        $percentages['percentageSurveyUneducatedStudiesAnswered'] = 0;
        $percentages['percentageSurveyPrimaryStudiesAnswered'] = 0;
        $percentages['percentageSurveyHighSchoolEducationStudiesAnswered'] = 0;
        $percentages['percentageSurveyHigherEducationAnswered'] = 0;
        $percentages['percentageSurveyPostgraduateStudiesAnswered'] = 0;
        $percentages['percentageSurveyOtherStudiesAnswered'] = 0;
        
        $percentages['percentageSurveyMenUneducatedStudiesAnswered'] = 0;
        $percentages['percentageSurveyMenPrimaryStudiesAnswered'] = 0;
        $percentages['percentageSurveyMenHighSchoolEducationStudiesAnswered'] = 0;
        $percentages['percentageSurveyMenHigherEducationAnswered'] = 0;
        $percentages['percentageSurveyMenPostgraduateStudiesAnswered'] = 0;
        $percentages['percentageSurveyMenOtherStudiesAnswered'] = 0;
        
        $percentages['percentageSurveyWomenUneducatedStudiesAnswered'] = 0;
        $percentages['percentageSurveyWomenPrimaryStudiesAnswered'] = 0;
        $percentages['percentageSurveyWomenHighSchoolEducationStudiesAnswered'] = 0;
        $percentages['percentageSurveyWomenHigherEducationAnswered'] = 0;
        $percentages['percentageSurveyWomenPostgraduateStudiesAnswered'] = 0;
        $percentages['percentageSurveyWomenOtherStudiesAnswered'] = 0;
        
        $percentages['percentageSurveyBetween1And12MonthsGoing'] = 0;
        $percentages['percentageSurveyBetween13And24MonthsGoing'] = 0;
        $percentages['percentageSurveyBetween25And36MonthsGoing'] = 0;
        $percentages['percentageSurveyBetween37And48MonthsGoing'] = 0;
        $percentages['percentageSurveyBetween49And60MonthsGoing'] = 0;
        $percentages['percentageSurveyBetween61And72MonthsGoing'] = 0;
            
        if(count($surveys) > 0) {
            $surveysIds = array_values(array_column($surveys, 'survey_id'));
            $socioIds = array_values(array_column($surveys, 'socio_data_id'));
            
            $totals['totalSurveys'] = count($surveysIds);
            
            $totals['totalMen'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('genre', 1)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('genre', 1)->count();
            $totals['totalWomen'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('genre', 2)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('genre', 2)->count();
            
            $totals['totalMother'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 1)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 1)->count();
            $totals['totalFather'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 2)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 2)->count();
            $totals['totalAunt'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 3)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 3)->count();
            $totals['totalUncle'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 4)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 4)->count();
            $totals['totalGrandma'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 5)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 5)->count();
            $totals['totalGrandfather'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 6)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 6)->count();
            $totals['totalGuardian'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 7)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 7)->count();
            $totals['totalTutor'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 8)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 8)->count();
            $totals['totalNanny'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 9)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 9)->count();
            $totals['totalCaregiver'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 10)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 10)->count();
            $totals['totalOther'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('kinship_id', 11)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('kinship_id', 11)->count();
            
            $totals['totalUnder20Years'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('age', '<', 20)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('age', '<', 20)->count();     
            $totals['totalBetween20And29Years'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('age', [20, 29])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('age', [20, 29])->count();
            $totals['totalBetween30And39Years'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('age', [30, 39])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('age', [30, 39])->count();
            $totals['totalBetween40And49Years'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('age', [40, 49])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('age', [40, 49])->count();
            $totals['totalBetween50And59Years'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('age', [50, 59])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('age', [50, 59])->count();
            $totals['totalOverEqual60Years'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('age', '>=', 60)->count() : SocioData::whereIn('id', $socioIds)->where('age', '>=', 60)->count();
            
            $totals['totalUneducatedStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 1)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 1)->count();
            $totals['totalPrimaryStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 2)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 2)->count();
            $totals['totalHighSchoolEducationStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 3)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 3)->count();
            $totals['totalHigherEducation'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 4)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 4)->count();
            $totals['totalPostgraduateStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 5)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 5)->count();
            $totals['totalOtherStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 6)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 6)->count();
            
            $totals['totalMenUneducatedStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 1)->where('genre', 1)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 1)->where('genre', 1)->count();
            $totals['totalMenPrimaryStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 2)->where('genre', 1)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 2)->where('genre', 1)->count();
            $totals['totalMenHighSchoolEducationStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 3)->where('genre', 1)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 3)->where('genre', 1)->count();
            $totals['totalMenHigherEducation'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 4)->where('genre', 1)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 4)->where('genre', 1)->count();
            $totals['totalMenPostgraduateStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 5)->where('genre', 1)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 5)->where('genre', 1)->count();
            $totals['totalMenOtherStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 6)->where('genre', 1)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 6)->where('genre', 1)->count();
            
            $totals['totalWomenUneducatedStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 1)->where('genre', 2)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 1)->where('genre', 2)->count();
            $totals['totalWomenPrimaryStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 2)->where('genre', 2)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 2)->where('genre', 2)->count();
            $totals['totalWomenHighSchoolEducationStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 3)->where('genre', 2)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 3)->where('genre', 2)->count();
            $totals['totalWomenHigherEducation'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 4)->where('genre', 2)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 4)->where('genre', 2)->count();
            $totals['totalWomenPostgraduateStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 5)->where('genre', 2)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 5)->where('genre', 2)->count();
            $totals['totalWomenOtherStudies'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->where('studies_id', 6)->where('genre', 2)->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->where('studies_id', 6)->where('genre', 2)->count();
            
            $totals['totalMenStudies'] = $totals['totalMenUneducatedStudies']+$totals['totalMenPrimaryStudies']+$totals['totalMenHighSchoolEducationStudies']+$totals['totalMenHigherEducation']+$totals['totalMenPostgraduateStudies']+$totals['totalMenOtherStudies'];
            $totals['totalWomenStudies'] = $totals['totalWomenUneducatedStudies']+$totals['totalWomenPrimaryStudies']+$totals['totalWomenHighSchoolEducationStudies']+$totals['totalWomenHigherEducation']+$totals['totalWomenPostgraduateStudies']+$totals['totalWomenOtherStudies'];

            $totals['totalBetween1And12MonthsGoing'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [1, 12])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [1, 12])->count();
            $totals['totalBetween13And24MonthsGoing'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [13, 24])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [13, 24])->count();
            $totals['totalBetween25And36MonthsGoing'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [25, 36])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [25, 36])->count();
            $totals['totalBetween37And48MonthsGoing'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [37, 48])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [37, 48])->count();
            $totals['totalBetween49And60MonthsGoing'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [49, 60])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [49, 60])->count();
            $totals['totalBetween61And72MonthsGoing'] = (IntVal($userRole) == 1) ? SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [61, 72])->withTrashed()->count() : SocioData::whereIn('id', $socioIds)->whereBetween('time_going', [61, 72])->count();
            
            $percentages['percentageSurveyAnswered'] = (float) number_format((($totals['totalSurveys']*100) / $members), 2);
            
            if($totals['totalSurveys'] > 0) {
                $percentages['percentageSurveyMenAnswered'] = (float) number_format((($totals['totalMen']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyWomenAnswered'] = (float) number_format((($totals['totalWomen']*100) / $totals['totalSurveys']), 2);
                
                $percentages['percentageSurveyMotherAnswered'] = (float) number_format((($totals['totalMother']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyFatherAnswered'] = (float) number_format((($totals['totalFather']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyAuntAnswered'] = (float) number_format((($totals['totalAunt']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyUncleAnswered'] = (float) number_format((($totals['totalUncle']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyGrandmaAnswered'] = (float) number_format((($totals['totalGrandma']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyGrandfatherAnswered'] = (float) number_format((($totals['totalGrandfather']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyGuardianAnswered'] = (float) number_format((($totals['totalGuardian']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyTutorAnswered'] = (float) number_format((($totals['totalTutor']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyNannyAnswered'] = (float) number_format((($totals['totalNanny']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyCaregiverAnswered'] = (float) number_format((($totals['totalCaregiver']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyOtherAnswered'] = (float) number_format((($totals['totalOther']*100) / $totals['totalSurveys']), 2);
                
                $percentages['percentageSurveyUnder20YearsAnswered'] = (float) number_format((($totals['totalUnder20Years']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyBetween20And29YearsAnswered'] = (float) number_format((($totals['totalBetween20And29Years']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyBetween30And39YearsAnswered'] = (float) number_format((($totals['totalBetween30And39Years']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyBetween40And49YearsAnswered'] = (float) number_format((($totals['totalBetween40And49Years']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyBetween50And59YearsAnswered'] = (float) number_format((($totals['totalBetween50And59Years']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyOverEqual60YearsAnswered'] = (float) number_format((($totals['totalOverEqual60Years']*100) / $totals['totalSurveys']), 2);
                
                $percentages['percentageSurveyUneducatedStudiesAnswered'] = (float) number_format((($totals['totalUneducatedStudies']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyPrimaryStudiesAnswered'] = (float) number_format((($totals['totalPrimaryStudies']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyHighSchoolEducationStudiesAnswered'] = (float) number_format((($totals['totalHighSchoolEducationStudies']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyHigherEducationAnswered'] = (float) number_format((($totals['totalHigherEducation']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyPostgraduateStudiesAnswered'] = (float) number_format((($totals['totalPostgraduateStudies']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyOtherStudiesAnswered'] = (float) number_format((($totals['totalOtherStudies']*100) / $totals['totalSurveys']), 2);
                
                $percentages['percentageSurveyBetween1And12MonthsGoing'] = (float) number_format((($totals['totalBetween1And12MonthsGoing']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyBetween13And24MonthsGoing'] = (float) number_format((($totals['totalBetween13And24MonthsGoing']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyBetween25And36MonthsGoing'] = (float) number_format((($totals['totalBetween25And36MonthsGoing']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyBetween37And48MonthsGoing'] = (float) number_format((($totals['totalBetween37And48MonthsGoing']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyBetween49And60MonthsGoing'] = (float) number_format((($totals['totalBetween49And60MonthsGoing']*100) / $totals['totalSurveys']), 2);
                $percentages['percentageSurveyBetween61And72MonthsGoing'] = (float) number_format((($totals['totalBetween61And72MonthsGoing']*100) / $totals['totalSurveys']), 2);
            }

            if($totals['totalMenStudies'] > 0) {
                $percentages['percentageSurveyMenUneducatedStudiesAnswered'] = (float) number_format((($totals['totalMenUneducatedStudies']*100) / $totals['totalMenStudies']), 2);
                $percentages['percentageSurveyMenPrimaryStudiesAnswered'] = (float) number_format((($totals['totalMenPrimaryStudies']*100) / $totals['totalMenStudies']), 2);
                $percentages['percentageSurveyMenHighSchoolEducationStudiesAnswered'] = (float) number_format((($totals['totalMenHighSchoolEducationStudies']*100) / $totals['totalMenStudies']), 2);
                $percentages['percentageSurveyMenHigherEducationAnswered'] = (float) number_format((($totals['totalMenHigherEducation']*100) / $totals['totalMenStudies']), 2);
                $percentages['percentageSurveyMenPostgraduateStudiesAnswered'] = (float) number_format((($totals['totalMenPostgraduateStudies']*100) / $totals['totalMenStudies']), 2);
                $percentages['percentageSurveyMenOtherStudiesAnswered'] = (float) number_format((($totals['totalMenOtherStudies']*100) / $totals['totalMenStudies']), 2);               
            }
            
            if($totals['totalWomenStudies'] > 0) {          
                $percentages['percentageSurveyWomenUneducatedStudiesAnswered'] = (float) number_format((($totals['totalWomenUneducatedStudies']*100) / $totals['totalWomenStudies']), 2);
                $percentages['percentageSurveyWomenPrimaryStudiesAnswered'] = (float) number_format((($totals['totalWomenPrimaryStudies']*100) / $totals['totalWomenStudies']), 2);
                $percentages['percentageSurveyWomenHighSchoolEducationStudiesAnswered'] = (float) number_format((($totals['totalWomenHighSchoolEducationStudies']*100) / $totals['totalWomenStudies']), 2);
                $percentages['percentageSurveyWomenHigherEducationAnswered'] = (float) number_format((($totals['totalWomenHigherEducation']*100) / $totals['totalWomenStudies']), 2);
                $percentages['percentageSurveyWomenPostgraduateStudiesAnswered'] = (float) number_format((($totals['totalWomenPostgraduateStudies']*100) / $totals['totalWomenStudies']), 2);
                $percentages['percentageSurveyWomenOtherStudiesAnswered'] = (float) number_format((($totals['totalWomenOtherStudies']*100) / $totals['totalWomenStudies']), 2);
            }
        }
        
        return ['totals' => $totals, 'percentages' => $percentages];
    }
    private function getStandardDeviationValue($values, $average)
    {
        $result = 0;
        
        foreach($values as $value) {
            $result += pow(($value - $average), 2); 
        }
        $result = $result / count($values); 
        
        return (float) number_format(sqrt($result), 2);
    }
    private function getAverageValue($values)
    {
        return (float) number_format((array_sum($values) / count($values)), 2);
    }
    private function getMinValue($values)
    {
        return (int) min($values);
    }   
    private function getMaxValue($values)
    {
        return (int) max($values);
    }
    private function getModeValue($values)
    {
        $countValues = array_count_values($values);
        
        if(count($countValues) == 1) {
            return (int) array_key_first($countValues);
        } else {
            $maxs = array_keys($countValues, max($countValues));
            
            return (int) $maxs[0];
        }
    }
}
