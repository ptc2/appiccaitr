<?php

namespace App\Services;

use App;
use Auth;
use Route;
use Request;
use URL;

use Stringy\StaticStringy as Stringy;
use Illuminate\Support\Collection;
use Carbon\Carbon;

use App\Models\Timeline;

class TimelineService
{
	protected $timelineModel = null;

    /**
     * Obtnemos el usuario logueado
     */
    public function __construct(){
    	$this->timelineModel = App::make(Timeline::class);
    }

    public function add($data) {

        $ip = Request::getClientIp();
        $data['actor_ip'] = !isset($data['actor_ip']) && ($ip) ? ip2long($ip) : null;
        
        if (Auth::check()) {
            $user = Auth::getUser();
            $data['actor_type'] = empty($data['actor_type']) ? get_class($user) : $data['actor_type'];
            $data['actor_id'] 	= empty($data['actor_id']) ? $user->id : $data['actor_id'];
            $data['username'] 	= empty($data['username']) ? $user->username : $data['username'];
        } else {
            $data['username'] 	= 'anonymous';
        }

        $data['url'] = URL::current();
        $data['origin'] = (strpos($data['url'], '/cms/') === false) ? 'web' : 'cms';

        return $this->timelineModel->fill($data)->save();
    }

    public function getFailedLoginAttemps($ip = null)
    {
        $ip = ($ip) ? $ip : ip2long(Request::getClientIp());
        $data = array(
            'attemps' => 0,
            'lastAttemp' => \Carbon\Carbon::now()
        );
        
        $items = DB::table($this->getTable())
            ->where($this->getTable() . '.actor_ip', $ip)
            ->where($this->getTable() . '.action', 'session.login')
            ->orderBy('id', 'desc')
            ->take(1)
            ->select('created_at')
            ->get();
            
        $lastSuccessfullLogin = ($items && isset($items[0]))?$items[0]->created_at:\Carbon\Carbon::create(1976);
        
        $items = DB::table($this->getTable())
            ->where($this->getTable() . '.actor_ip', $ip)
            ->where($this->getTable() . '.action', 'session.failed_login')
            ->where($this->getTable() . '.created_at', '>', $lastSuccessfullLogin)
            ->orderBy('id', 'desc')
            ->select('created_at')
            ->get();

        if ($items->isNotEmpty()){
            $data['attemps'] = count($items);
            $data['lastAttemp'] = $items[0]->created_at;
        }
        
        return $data;
    }
    
}
