<?php

namespace App\Services;

use OneSignal;
use Illuminate\Http\Request;

use App\Models\Device;
use App\Models\Platform;
use App\Models\User;

class DeviceService
{
    /**
     * Store a new device
     * @param Request $request
     * @return Response $object
     */
    public function storeDevice($data)
    {
        $platformId = $data['platform_id'];
        unset($data['platform_id']);

        $device = Device::create($data);
        $this->associatePlatform($platformId, $device);
        $this->associateDisassociateUser($device);

        OneSignal::createDevice($device);

        return $device;
    }
    /**
     * Update a device
     * @param Request $request
     * @param Int $id
     * @return Response $object
     */
    public function updateDevice($data, $uuid)
    {
        $platformId = $data['platform_id'];
        unset($data['platform_id']);

        $device = Device::updateOrCreate(
            ['uuid' => $uuid],
            $data
        );
        $this->associatePlatform($platformId, $device);

        if ($device->player_id) {
            OneSignal::updateDevice($device);
        } else {
            OneSignal::createDevice($device);
        }

        $this->associateDisassociateUser($device);

        return $device;
    }
    /**
     * Associate a new platform
     * @param Int $platformId
     * @param Device $device
     */
    private function associatePlatform($platformId, Device $device)
    {
        $platform = $device->platform;

        if (!isset($platform) || $platform->id !== $platformId) {
            $newPlatform = Platform::find($platformId);

            if ($newPlatform) {
                $device->platform()->dissociate();
                $device->platform()->associate($newPlatform);
                $device->save();
            }
        }
    }
    /**
     * Associate new user or dissociate user
     */
    private function associateDisassociateUser($device)
    {
        if (request()->has('user_id')) {
            $this->associateUser(request()->user_id, $device);
        } else {
            $device->user()->dissociate();
            $device->save();
        }
    }
    /**
     * Associate and dissociate new user
     */
    private function associateUser($userId, Device $device)
    {
        $user = $device->user;

        if (!isset($user) || $user->id !== $userId) {
            $newUser = User::find($userId);

            if ($newUser) {
                $device->user()->dissociate();
                $device->user()->associate($newUser);
                $device->save();
            }
        }
    }
}
