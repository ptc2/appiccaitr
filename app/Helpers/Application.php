<?php

namespace App\Helpers;

use View;
use Config;

class Application
{


    private static $app = '';

    public static function isCMS()
    {
        return Application::$app == 'cms' ? true : false;
    }
    public static function isWeb()
    {
        return Application::$app == 'web' ? true : false;
    }
    public static function setWeb()
    {
        View::addLocation(realpath(base_path('resources/views/web')));
        Application::$app = 'web';
    }
    public static function setCMS()
    {
        Config::set('app.url_base', Config::get('app.url_base') . '/' . Config::get('app.cms_prefix'));
        View::addLocation(realpath(base_path('resources/views/cms')));
        Application::$app = 'cms';
    }
}
