<?php

if (! function_exists('float_format')) {
    function float_format($number, $unit = null)
    {
        // number to float
        $numberFloat = $number;

        if (is_string($numberFloat)) {
            // a veces viene un numero con su unidad (por ejemplo 10 €)
            if (preg_match('/\s+/', $numberFloat)) {
                $numberFloat = explode(' ', $numberFloat)[0];
            }

            // 0,000.00 €
            if (preg_match('/^(\-)?[0-9]+,[0-9]+\.[0-9]+$/', $numberFloat)) {
                $numberFloat = str_replace(',', '', $numberFloat);
            } elseif (preg_match('/^(\-)?([0-9]+\.)?[0-9]+,[0-9]+$/', $numberFloat)) {
                $numberFloat = str_replace('.', '', $numberFloat);
                $numberFloat = str_replace(',', '.', $numberFloat);
            }

            // si es vacio o null, se convierte a 0
            $numberFloat = (float) $numberFloat;
        }

        //redondeo al alza
        $numberFloat = round($numberFloat, 2, PHP_ROUND_HALF_UP);

        // format config
        $decimal   = trans('format.decimal');
        $thousands = trans('format.thousands');

        // formato correcto
        $numberFloat = number_format($numberFloat, 2, $decimal, $thousands);

        if (!is_null($unit)) {
            return $numberFloat . ' ' . $unit;
        }

        return $numberFloat;
    }
}

if (! function_exists('float_round')) {
    function float_round($number)
    {
        return round($number, 2, PHP_ROUND_HALF_UP);
    }
}
if (! function_exists('fwRoute')) {
    function fwRoute($alias)
    {
        return Route::getRoutes()->hasNamedRoute($alias)?route($alias):'';
    }
}
if (! function_exists('res')) {
    
    function res($file)
    {
        
        return Config::get('app.url_base').$file;
    }
}
if (! function_exists('uploads')) {
    
    function uploads($file)
    {
        
        return Config::get('app.url_base').'/res/uploads/'.$file;
    }
}
if (! function_exists('weblink')) {
    
    function weblink($url)
    {
        if(strpos($url, 'http:')==FALSE || strpos($url, 'https:')==FALSE)
            return Config::get('app.url_base').'/'.$url;
        else
            return $url;
    }
}
if (! function_exists('weburl')) {
    
    function weburl($url)
    {
        
        return Config::get('app.url_base').'/'.$url;
    }
}

/* Importados desde Solbyte FW*/

/**
     * @brief Crea una cadena de texto amigable
     * @param <String> $text  texto a convertir
     */
if (! function_exists('friendly')){
    function friendly($text)
    {
        $delete=array("!","¡","?","¿","'","\"","$","(",")",".",":",";","_","/","\\","\$","%","@","#",",", "«", "»","™");
        $search=array(" ","á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ","ü","à","è","ì","ò","ù","À","È","Ì","Ò","Ù");
        $change=array("-","a","e","i","o","u","a","e","i","o","u","n","n","u","a","e","i","o","u","A","E","I","O","U");
        $end=strtolower(str_replace($search,$change,str_replace($delete,"",$text)));
        $end=str_replace("–","-",$end);
        $end=str_replace("–","-",$end);
        return (strlen($end)>50) ? substr($end,0,strrpos(substr($end,0,50),"-")):$end;        
    }
}
    /**
     * @brief Crea una cadena de texto amigable para nombre de ficheros
     * @param <String> $text  texto a convertir
     */
if (! function_exists('friendlyFileName')){
    function friendlyFileName($text)
    {
        $text=strip_tags($text);
        $delete=array("!","¡","?","¿","'","\"","$","(",")","",":",";","_","/","\\","\$","%","@","#",",", "«", "»","&","<",">");
        $search=array(" ","á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ","ü","à","è","ì","ò","ù","À","È","Ì","Ò","Ù");
        $change=array("-","a","e","i","o","u","a","e","i","o","u","n","n","u","a","e","i","o","u","A","E","I","O","U");
        $end=strtolower(str_replace($search,$change,str_replace($delete,"",$text)));
        $end=str_replace("–","-",$end);
        $end=str_replace("–","-",$end);
        return (strlen($end)>250) ? substr($end,0,strrpos(substr($end,0,250),"-")):$end;        
    }
}    

/**
 * @brief Crea un nombre de fichero válido
 * @param <String> $text  texto a convertir
 */
if (! function_exists('makeName')){
    function makeName($name, $table, $extension)
    {
    	if(!$name)$name='image';
		$friendly = friendlyFileName($name);
		$cont='';
		$fileName=$friendly.'.'.$extension;
		while(file_exists(public_path("/uploads/$table/").$fileName)){
			$cont=$cont?++$cont:1;
            $fileName=$friendly.'-'.$cont.'.'.$extension;
		}
		return $fileName;
    }
}    

if (! function_exists('escape')){
    function escape($val,$conn)
    {
        if(is_numeric($val))
            return $val;
        else
            return mysqli_real_escape_string($conn,$val);
    }
}
    /**
     * @brief desescapa una variable
     * @param   <String> $text cadena a descapar
     * @return  <String> cadena desescapada
     */
if (! function_exists('unescape')){
    function unescape($val)
    {
        return htmlspecialchars(stripslashes($val));
    }
}
    /**
     * @brief Devuelve el texto del mes pasándole un entero
     * @param   <Int> $month entero número de mes (0 es Enero) 
     * @param   <String> $lang idioma para devolver el mes 
     * @return  <String> cadena con el nombre del mes
     */
if (! function_exists('month')){
    function month($month,$lang){
        switch ($lang) {
            case 'en': $months = array('January','February','March','April','May','June','July','August','September','October','November','December');break;            
            case 'fr': $months = array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre');break;
            case 'ru': $months = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');break;
            default: $months = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');break;
        }
        return $months[$month];
    }
}
    /**
     * @brief Devuelve el número de la letra
     * @param   <String> $letter Letra a convertir en número
     * @return  <Int> Número de la letra A->1
     */
if (! function_exists('letterToNumber')){
    function letterToNumber($letter) 
    {
        $letters= array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        return array_search(strtoupper($letter), $letters)+1;
    }
}
    /**
     * @brief Devuelve la letra a la que corresponde un número
     * @param   <Int> $number Número a convertir en letra
     * @return  <String> Letra a la que corresponde 1->A
     */ 
if (! function_exists('numberToLetter')){
    function numberToLetter($number) 
    {
        $letters= array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1');
        return $letters[$number-1];
    }
}
    /*                  
     * *** Funciones de fecha *** *
     */
    /**
     * @brief Convierte fecha de SQL a ES
     * @param <String> $date  Fecha
     */
if (! function_exists('sqlToDate')){
    function sqlToDate($date) 
    {
        $aTmp = explode('-', $date);
                return $aTmp[2].'/'.$aTmp[1].'/'.$aTmp[0];
    }
}    
    /**
     * @brief Convierte fecha de ES a SQL
     * @param <String> $date  Fecha
     */
if (! function_exists('dateToSql')){
    function dateToSql($date) 
    {
        $aTmp = explode('/', $date);
                return $aTmp[2].'-'.$aTmp[1].'-'.$aTmp[0];
    }
}
    /**
     * @brief Convierte una fecha dd/mm/aaaa a aaaa-mm-dd
     * @param   <String> $date Fecha a convertir
     * @return  <String> Fecha convertida
     */
if (! function_exists('dateEspToMysql')){
    function dateEspToMysql($date) 
    {
        if($date=="") return "0000-00-00";
        $newDate=explode( "/", $date);
        return $newDate[2]."-".$newDate[1]."-".$newDate[0];
    }
}
    /**
     * @brief Convierte una fecha dd/mm/aaaa hh:mm:ss a aaaa-mm-dd hh:mm:ss
     * @param   <String> $date Fecha a convertir
     * @return  <String> Fecha convertida
     */ 
if (! function_exists('dateTimeEspToMysql')){
    function dateTimeEspToMysql($date) 
    {
        if($date=="") return "0000-00-00 00:00:00";
        $dateTime=explode( " ", $date);
        $newDate=explode( "/", $dateTime[0]);
        return $newDate[2]."-".$newDate[1]."-".$newDate[0]." ".$dateTime[1];
    }
}
    /**
     * @brief Convierte una fecha aaaa-mm-dd a dd/mm/aaaa
     * @param   <String> $date Fecha a convertir
     * @return  <String> Fecha convertida
     */
if (! function_exists('dateMysqlToEsp')){
    function dateMysqlToEsp($date) 
    {
        if($date=="0000-00-00 00:00:00"|| is_null($date) || $date=="") return "";
        $newDate=explode( "-", $date);
        return $newDate[2]."/".$newDate[1]."/".$newDate[0];
    }
}
    /**
     * @brief Convierte una fecha aaaa-mm-dd hh:mm:ss a dd/mm/aaaa hh:mm:ss
     * @param   <String> $date Fecha a convertir
     * @return  <String> Fecha convertida
     */
if (! function_exists('dateTimeMysqlToEsp')){
    function dateTimeMysqlToEsp($date) 
    {
        if($date=="0000-00-00 00:00:00" || is_null($date) || $date=="") return "";
        $dateTime=explode( " ", $date);
        $newDate=explode( "-", $dateTime[0]);
        return $newDate[2]."/".$newDate[1]."/".$newDate[0]." ".$dateTime[1];
    }
}
    /**
     * @brief Convierte una fecha aaaa-mm-dd a dd/mm
     * @param   <String> $date Fecha a convertir
     * @return  <String> Fecha convertida
     */ 
if (! function_exists('simpleDate')){
    function simpleDate($date) 
    {
        if($date=="0000-00-00" || $date=="0000-00-00 00:00:00" || is_null($date) || $date=="") return "";
        $newDate=explode( "-", $date);
        return $newDate[2]."/".$newDate[1];
    }
}
    /**
     * @brief Convierte una fecha aaaa-mm-dd a aaaaa
     * @param   <String> $date Fecha a convertir
     * @return  <String> Fecha convertida
     */
if (! function_exists('dateToYear')){
    function dateToYear($date) 
    {
        if($date=="0000-00-00"|| is_null($date) || $date=="") return "";
        $newDate=explode( "-", $date);
        return $newDate[0];
    }
}
    /**
     * @brief Convierte una fecha aaaa-mm-dd a mm
     * @param   <String> $date Fecha a convertir
     * @return  <String> Fecha convertida
     */
if (! function_exists('dateToMonth')){
    function dateToMonth($date) 
    {
        if($date=="0000-00-00"|| is_null($date) || $date=="") return "";
        $newDate=explode( "-", $date);
        return $newDate[1];
    }
}
    /**
     * @brief Convierte una fecha aaaa-mm-dd a dd
     * @param   <String> $date Fecha a convertir
     * @return  <String> Fecha convertida
     */
if (! function_exists('dateToDay')){
    function dateToDay($date) 
    {
        if($date=="0000-00-00"|| is_null($date) || $date=="") return "";
        $newDate=explode( "-", $date);
        return $newDate[2];
    }
}
    /**
     * @brief devuelve una cadena a la cual se ha aplicado una limitación de caracteres. Solo devuelve palabras enteras.
     * @param <int> $string  cadena de caracteres entera
     * @param <int> $limit   número de caracteres a limitar
     * @return <string> cadena limitada
     */
if (! function_exists('limitHtmlText')){
    function limitHtmlText ($text,$limit) {

        if(strlen($text) < $limit) {
            $text = strip_tags($text,'<strong>');
        }else {
            $text2=substr($text,0,$limit);
            if(strpos($text2, '<a')!==FALSE){
                $limit+=35;
                $text = strip_tags($text,'<a>');
                $text=substr($text,0,$limit);
                $cutFromHere = strrpos($text," ");
                $text=substr($text,0,$cutFromHere).' ...';
            }
            else{
                $cutFromHere = strrpos($text2," ");
                $text2=substr($text2,0,$cutFromHere).' ...';
                return $text2;
            }
        } 
        return $text;
    }
}
    /**
     * @brief Obtiene las URLS amigables desde la BD de la sección
     */
if (! function_exists('getURLs')){
    function getURLs()
    {
        $objModel= new SectionsModel;
        $urls=$objModel->getURLs();
        return $urls;
    }
}
    /**
     * @brief Obtiene las URLS amigables desde la BD de la sección
     */
if (! function_exists('getRRSS')){
    function getRRSS()
    {
        $objModel= new SectionsModel;
        $rrss=$objModel->getRRSS();
        return $rrss;
    }
}
    /**
     * @brief Obtiene los scripts
     */
if (! function_exists('getScripts')){
    function getScripts()
    {
        $objModel= new SectionsModel;
        $scripts=$objModel->getScripts();
        return $scripts;
    }
}
if (! function_exists('langCode')){
    function langCode($str) 
    {
        $idiomas['en']='en_GB';
        $idiomas['fr']='fr_FR';
        $idiomas['de']='de_DE';
        $idiomas['pt']='pt_PT';
        $idiomas['it']='it_IT';
        $idiomas['ru']='ru_RU';
        $idiomas['ch']='zh_TW';
        $idiomas['nl']='nl_NL';
        $idiomas['ar']='ar';
        $idiomas['ro']='ro_RO';
        $idiomas['ca']='ca_ES';
        $idiomas['po']='pl_PL';
        $idiomas['hu']='hu_HU';
        $idiomas['tr']='tr_TR';
        return $letters[$str];
    }
}
if (! function_exists('is_home')){
    function is_home()
    {
        $langs=array();
        foreach ($GLOBALS['langs'] as $l) {
            if($l==DEFAULT_LOCALE)
                $langs[]='/';
            else
                $langs[]='/'.$l;
        }
        if(in_array($_SERVER['REQUEST_URI'],$langs))
            return true;
        else
            return false;
    }
}

if (! function_exists('clear_script_code')) {
    function clear_script_code($code)
    {
        return str_replace(
            array('{ ','{','<script>', '<script type="text/javascript">','</script>'), 
            array('{ ','{ ','','',''), 
            $code
        );
    }
}
if (! function_exists('get_concat')) {
    function get_concat($columns)
    {   $concat = 'CONCAT(';
        $concats = [];
        foreach($columns as $column) {
            $coalesce = "COALESCE(REPLACE(" . $column . ",' ',''),'')";
            $concats[] = $coalesce;
        }
        $concat .= implode(",", $concats) . ') as data';

        return $concat;
    }
}

if(!function_exists('to_iso_8601_string')) {
    function to_iso_8601_string($value) {
        try {
            if($value)return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value)->toIso8601String();
        }catch(\Exception $e) {}

        return $value;
    }
}

if(!function_exists('addressInArea')) {
    
    function addressInArea($point, $area) {
        $intersections = 0; 
        $area_count = count($area);

        for ($i=1; $i < $area_count; $i++) {
            $vertex1 = $area[$i-1]; 
            $vertex2 = $area[$i];

            // Check if point is equal to vertex
            if($point == $vertex1 || $point == $vertex2) {
                return true;
            }

            // Check if point is in horizontal segment
            if ($vertex1['lng'] == $vertex2['lng'] && 
                $vertex1['lng'] == $point['lng'] && 
                $point['lat'] > min($vertex1['lat'], $vertex2['lat']) && 
                $point['lat'] < max($vertex1['lat'], $vertex2['lat'])) { 

                return true;
            }

            if ($point['lng'] > min($vertex1['lng'], $vertex2['lng']) && 
                $point['lng'] <= max($vertex1['lng'], $vertex2['lng']) && 
                $point['lat'] <= max($vertex1['lat'], $vertex2['lat']) && 
                $vertex1['lng'] != $vertex2['lng']) { 

                $xinters = ($point['lng'] - $vertex1['lng']) * ($vertex2['lat'] - $vertex1['lat']) / ($vertex2['lng'] - $vertex1['lng']) + $vertex1['lat']; 
                // Check if point is in another horizontal segment
                if ($xinters == $point['lat']) {
                    return true;
                }
                if ($vertex1['lat'] == $vertex2['lat'] || $point['lat'] <= $xinters) {
                    $intersections++; 
                }
            } 
        } 
        // If the intersetions number is odd, the point is inside
        if ($intersections % 2 != 0) {
            return true;
        } else {
            return false;
        }
    }
}
if(!function_exists('get_transport')) {
    function get_transport($step) {
        $travelMode = $step->travel_mode;
        switch($travelMode) {
            case 'DRIVING':
                $conveyance = App\Models\Conveyance::find(7);
            break;
            case 'WALKING':
                $conveyance = App\Models\Conveyance::find(9);
            break;
            case 'BICYCLING':
                $conveyance = App\Models\Conveyance::find(10);
            break;
            case 'TRANSIT':
                $vehicleType = $step->transit_details->line->vehicle->type;
                switch($vehicleType) {
                    case 'CABLE_CAR':
                    case 'FUNICULAR':
                    case 'GONDOLA_LIFT':
                    case 'RAIL':
                    case 'METRO_RAIL':
                    case 'SUBWAY':
                    case 'TRAM':
                    case 'MONORAIL':
                    $conveyance = App\Models\Conveyance::find(3);
                    break;
                    case 'FERRY':
                    case 'HEAVY_RAIL':
                    case 'COMMUTER_TRAIN':
                    case 'HIGH_SPEED_TRAIN':
                    case 'LONG_DISTANCE_TRAIN':
                    $conveyance = App\Models\Conveyance::find(4);
                    break;
                    case 'TROLLEYBUS':
                    case 'INTERCITY_BUS':
                    $conveyance = App\Models\Conveyance::find(2);
                    break;
                    case 'BUS':
                    $conveyance = App\Models\Conveyance::find(1);
                    break;
                    case 'SHARE_TAXI':
                    $conveyance = App\Models\Conveyance::find(5);
                    break;
                    case 'OTHER':
                    $conveyance = App\Models\Conveyance::find(11);
                    break;
                }
            break;
        }

        return $conveyance;
    }
}
if(!function_exists('clear_distance')) {
    function clear_distance($distance) {
        return floatval(trim(str_replace('km', '', $distance)));
    }
}
if(!function_exists('aes_decrypt')) {
    function aes_decrypt($value) {
        $payload = json_decode(base64_decode($payload), true);
        $iv = base64_decode($payload['iv']);
        $decrypted = \openssl_decrypt(
            $payload['value'], config('app.cipher'), base64_decode(env('API_KEY')), 0, $iv
        );

        return $decrypted;
    }
}